# Questions from *Kurose* Chapter 6

## R4: Suppose two nodes start to transmit at the same time a packet of length *L* over a broadcast channel of rate *R*. Denote the propagation delay between the two nodes as *d*<sub>*prop*</sub>. Will there be a collision if *d*<sub>*prop*</sub> < *L*/*R*? Why or why not?

For this question, it depends on what is categorized as a 'collision'. If we take an example with two nodes, N1 and N2, then *d*<sub>*prop*</sub> being less than *L*/*R* essentially means that if both nodes start transmitting at the same time, the the data from the packet sent from N1 will reach N2 before N2 has fully transmitted its packet (and vice versa). So, there will be somewhat of a collision in the sense that a node will be attempting to transmit bits onto the medium while also trying to receive data from the medium. However, modern interfaces support simultaneous reading and writing from a physical medium, so in reality, there would be no collision issue.

## R5: In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?

Slotted ALOHA has the following characteristics:
* When only one node has data to send, that node has a throughput of *R* bps.
* The protocol is decentralized; that is, there is no master node that represents a single point of failure for the network.
* The protocol is simple, so that it is inexpensive to implement.

Token passing has the following characteristics:
* When only one node has data to send, that node has a throughput of *R* bps.
* When *M* nodes have data to send, each of these nodes has a throughput of *R*/*M* bps. This need not necessarily imply that each of the *M* nodes always has an instantaneous rate of *R*/*M*, but rather that each node should have an average transmission rate of *R*/*M* over some suitably defined interval of time.
* The protocol is simple, so that it is inexpensive to implement.

## R6: In CSMA/CD, after the fifth collision, what is the probability that a node chooses *K* = 4? The result *K* = 4 corresponds to a delay of how many seconds on a 10 Mbps Ethernet?

The node has a 1 in 32 chance of choosing *K* = 4. A *K* = 4 corresponds to a delay of 204.8 microseconds on a 10 Mbps Ethernet.

## P1: Suppose the information content of a packet is the bit pattern `1110 0110 1001 0101` and an even parity scheme is being used. What would the value of the field containing the parity bits be for the case of a two-dimensional parity scheme? Your answer should be such that a minimum-length checksum field is used.

<pre>
1 1 1 0 <mark>1</mark>  
0 1 1 0 <mark>0</mark>  
1 0 0 1 <mark>0</mark>  
0 1 0 1 <mark>0</mark>  
<mark>0 1 0 0</mark>  
</pre>

Assuming the protocol for the two-dimensional parity scheme specified that the parity bits would be the row bits followed by the column bits, then the value of the field would be `1 0 0 0 0 1 0 0`.

## P3: Suppose the information portion of a packet (*D* in Figure 6.3) contains 10 bytes consisting of the 8-bit unsigned binary ASCII representation of the string "Internet". Compute the Internet checksum for this data.

The internet checksum calculates the addition of the data as if it were just a string of 16-bit integers. Because the ASCII characters are represented by 8 bits, each integer will just be two characters put together, so the addition can be visualized as `In + te + rn + et`. Turning each letter into its binary counterpart, we have:

<pre>
  0100100101101110  
  0111010001100101  
  0111001001101110  
+ 0110010101110100  
  <b>1001010110110101</b>
</pre>

Then, the checksum is actually the 1's complement of the addition result, so the final answer is `0110101001001010`.

## P6: Consider the 5-bit generator, G = `10011`. Find R based on the following D values:

#### a. D = `1000100101`.

<pre>
<b>10001</b>001010000  
<b>10011</b>  
|||||  
vvvvv  
000<b>10001</b>010000  
   <b>10011</b>  
   |||||  
   vvvvv  
   000<b>10010</b>000  
      <b>10011</b>  
      |||||  
      vvvvv  
      0000<b>1000</b>  
          ||||  
          vvvv  
      R = <b>1000</b>
</pre>

#### b. D = `0101101010`.

<pre>
0<b>10110</b>10100000  
 <b>10011</b>  
 |||||  
 vvvvv  
 00<b>10110</b>100000  
   <b>10011</b>  
   |||||  
   vvvvv  
   00<b>10110</b>0000  
     <b>10011</b>  
     |||||  
     vvvvv  
     00<b>10100</b>00  
       <b>10011</b>  
       |||||  
       vvvvv  
       00<b>11100</b>  
         <b>10011</b>
         |||||  
         vvvvv  
         0<b>1111</b>  
          ||||  
          vvvv  
      R = <b>1111</b>
</pre>

#### c. D = `0110100011`.

<pre>
0<b>11010</b>00110000  
 <b>10011</b>  
 |||||  
 vvvvv  
 0<b>10010</b>0110000  
  <b>10011</b>  
  |||||  
  vvvvv  
  0000<b>10110</b>000  
      <b>10011</b>  
      |||||  
      vvvvv  
      00<b>10100</b>0  
        <b>10011</b>  
        |||||  
        vvvvv  
        00<b>1110</b>  
          ||||  
          vvvv  
      R = <b>1110</b>
</pre>

## P11: Suppose four active nodes - nodes A, B, C, and D - are competing for access to a channel using slotted ALOHA. Assume each node has an infinite number of packets to send. Each node attempts to transmit in each slot with probability *p*. The first slot is numbered slot 1, the second slot is numbered slot 2, and so on.

#### a. What is the probability that node A succeeds for the first time in slot 4?

$$((1-p) + (p * 3p))^3 * p * (1-p)^3$$

#### b. What is the probability that some node (either A, B, C, or D) succeeds in slot 5?

$$4((1-p)^3 * p)$$

#### c. What is the probability that the first success occurs in slot 4?

$$(p^2 + (1-p)^4)^3 * 4p$$

#### d. What is the efficiency of this four-node system?

$$4p(1-p)^3$$

## P13: Consider a broadcast channel with *N* nodes and a transmission rate of *R* bps. Suppose the broadcast channel uses polling (with an additional polling node) for multiple access. Suppose the amount of time from when a node completes transmission until the subsequent node is permitted to transmit (that is, the polling delay) is *d*<sub>*poll*</sub>. Suppose that within a polling round, a given node is allowed to transmit at most *Q* bits. What is the maximum throughput of the broadcast channel?

$$\frac{Q}{\frac{R}{Q} + d}$$