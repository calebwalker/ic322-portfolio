# Lab: Protocol Pioneer

## Introduction

This lab involved playing LCDR Downs' beta-version game and testing its functionality. The game is intended to teach others about network protocols without explicitly referring to them.

## Collaboration/Citations

The game is located [here](https://gitlab.com/jldowns-usna/protocol-pioneer/-/tree/main?ref_type=heads). I did not use any additional resources, nor did I get help from anyone on this lab.

## Process

I followed the lab instructions, and completed Chapter 1, Act 2 of the game. This was all in accordance with the instructions outlined in the [Protocol Pioneer Lab](https://courses.cs.usna.edu/IC322/#/assignments/week07/protocol-pioneer/).

## Questions

### Q1: Include your client and server strategy code.

```python
def server_strategy(self):

    self.set_display(f"Queued:{len(self.from_layer_3())}")
    while(self.message_queue):
            m = self.message_queue.pop()
            if parse_message(m.text).get("Destination") == self.id:
                self.to_layer_3(m.text)

def client_strategy(self):
    
    i = self.connected_interfaces()[0]

    canSend = False
    tickMod = self.current_tick() % 40
    if self.id == "1" and tickMod < 15:
        canSend = True
    if self.id == "2" and tickMod >= 20 and tickMod < 35:
        canSend = True

    if not self.interface_sending(i) and len(self.from_layer_3()) > 0 and canSend:
        msg_text = self.from_layer_3().pop()
        self.send_message(msg_text, i)
```

### Q2: Explain your strategies in English.

Essentially, the clients are the only things that send, and the servers just receive the messages (without responding). This is becuase server responses are way longer than client requests, and so to maximize successes (which are **either** a server receiving a request or a client receiving a response), we only send the shortest messages through the channel. Then, for each client, there's basically a 40 second time interval to make sure both clients can send messages and that the messages aren't overlapping:

* 0-14 seconds: Client 1 sends.
* 15-19 seconds: Pause (to let any messages propagate through).
* 20-34 seconds: Client 2 sends.
* 35-39 seconds: Pause (to let any messages propagate through).


### Q3: What was your maximum steady-state success rate (after 300 or so ticks)?

0.19, with some variation/fluctuation.

### Q4: Evaluate your strategy. Is it good? Why or why not?

No, the strategy is certainly not good. In a real scenario, a success is a request **and** a response. This network would not even function, because servers never respond.

### Q5: Are there any strategies you'd like to implement, but you don't know how?

Initially, 

### Q6: Any other comments?