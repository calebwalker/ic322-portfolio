# Week 12 Learning Objectives

## I can explain the ALOHA and CSMA/CD protocols and how they solve the multiple-access problem.

**ALOHA** is a Random Access Protocol, which means that nodes on the shared medium have a random aspect to the amount of time they wait before re-transmitting a frame. It is easiest to explain how a protocol operates by using an example. Suppose three nodes are all connected by a shared medium, and that when more than one transmission arrives at a given node, the transmissions interfere and cannot be interpolated. With the ALOHA protocol, when one of the nodes has something to send, it immediately sends an entire frame of data. If, during the transmission, no other transmissions are detected on the medium, then the node knows that its message made it to all other nodes without any interference.

However, considering a scenario where two nodes are transmitting at the same time, then a problem arises. ALOHA resolves this issue by defining re-transmission to be based on a probability. Consider one of the multiple nodes that tries to transmit a frame while other nodes also transmit. It will receive a transmission in the midst of trying to transmit its own frame onto the medium, and thus will know that its transmission did *not* successfully reach all other nodes on the medium. It must now re-transmit the frame - but this is where the random access comes in.

Instead of immediately re-transmitting, the node conditions a retransmission on a given probability *p*. Essentially, the node flips a theoretical 'coin' with a probability *p* of landing heads and a probability (1-*p*) of landing tails. If the 'coin' lands heads, the node retransmits the frame. If the 'coin' lands tails, the node waits for a *frame time* (the length of time it takes to transmit a frame onto the medium). Then, it flips the coin again. For those with an understanding of probability, this is akin to a Geometrically-distributed random variable. Every *frame time*, the node either re-transmits the frame, or waits and tries again at the next time interval.

With this random aspect of the protocol, collisions have a smaller and smaller chance of happening after the initial transmission interference. At some point, a node will randomly choose to transmit while the other nodes choose to not transmit. This will allow for a frame to be sent through the medium successfully. As this generalizes over time, the total throughput will approach 0.184*R*, with *R* being the maximum transmission rate of the medium.

**Carrier Sense Multiple Access (CSMA) and CSMA with Collision Detection (CSMA/CD)** protocols essentially implement similar re-transmission rules as the ALOHA protocol, but have stricter rules regarding intial transmissions. First, CSMA dictates that a node on a shared medium must first listen to the medium for a short bit to make sure no other node is already transmitting before starting its own transmission. That rule defines CSMA, but CSMA/CD protocols also implement another rule: if a node detects a transmission on the medium while it is already transmitting, then it immediately ceases its current transmission.

Even with these additional rules, collisions do still occur. This is because of *propagation delay* - when a node transmits a frame, it does not immediately propagate through the medium. Thus, even if a node senses silence on the medium, there could be another node already transmitting, if the bits of that transmission haven't fully propagated thoughout the medium yet. The longer a propagation delay is for a given medium, the higher a chance that a collision occurs.

As mentioned above, CSMA and CSMA/CD protocols implement similar re-transmission rules to ALOHA. However, there are some key differences in how the re-transmission is randomly computed. In CSMA/CD, upon detecting another transmission while transmitting a frame, a node knows that the frame it was trying to transmit was involved in a collision. Thus, it must re-transmit, and must do so in a random fashion in order to avoid a cycle of collisions. Instead of just having a probability of re-transmission every *frame time*, the node uses an algorithm known as *binary exponential backoff*.

The binary exponential backoff algorithm effectively allows the node to wait a short time interval when only 1 or 2 other nodes are transmitting, but also ensures the node waits for a longer time interval when there are more nodes transmitting at the same time. The premise of the algorithm is such: a node that has experienced *n* consecutive collisions will wait for an interval of *K*, where *K* is a random number chosen from {0, 1, 2, ... 2<sup>*n*</sup>-1}. When many nodes are trying to transmit, *n* will usually be larger, so the node will wait for a longer period of time on average.

## I can compare and contrast various error correction and detection schemes, including parity bits, 2D parity, the Internet Checksum, and CRC.

**Parity** is a checking system that can detect an odd number of bit errors in an arbitrarily long string of bits. Parity bit checking is useful because the overhead is minimal (just one extra bit appended to a string of any number of bits). However, it provides no correction and is unable to detect errors in a substantial amount of cases (if there are an even number of bit errors).

Parity checking works by setting a single bit at the end of a message to ensure the number of 1 bits remain even in an even parity scheme (or odd in an odd parity scheme - the process is identical). Then, the receiver counts the number of 1s in the received message (including the parity bit), and if the number of 1s is odd in (using a even parity scheme) or even (using an odd parity scheme), then the receiver knows there has been a bit error. For example, if the message to be sent was `0 1 1 1 0 0 1 0`, then the sender (assuming an even parity scheme) would choose a `0` to append as the parity bit, in order to keep the number of 1 bits even. Then, the receiver would count all of the 1s and come up with 4 bits. 4 is even, so the receiver assumes there are no errors in the message. However, if a `0` was flipped to a `1`, or vice versa, the receiver would end up with an odd number after counting the bits, and know for certain that an error occurred.

Important to note with the parity bit is the fact that it does not provide any error *detection* - the receiver can know if a bit was flipped, but it cannot know which bit with only a single parity bit to go off of. Also, continuing with the previous example, suppose that both the first and second bit were flipped. The resulting message would be `1 0 1 1 0 0 1 0`, and the receiver also receives a parity bit of `0`. This would look completely fine to the receiver, even though the received message *does* have bit errors. Such is a showcase of how a parity bit scheme can only detect an odd number of bit errors.

For a really great animated example of parity checking, look at [this video](https://www.youtube.com/watch?v=X8jsijhllIA&t=350s), starting at the linked timestamp (5:50). The video also goes on and explains a more sophisticated error detection scheme in a really easy-to-follow way.

**2D parity** is a method that implements parity bits in a specific way to allow for error *detection* in addition to a more robust error *detection*. 2D parity requires significantly more overhead (instead of just one bit, the amount of parity bits needed scales with the length of the message - the longer the message, the more bits required). However, it allows for a message to be corrected (assuming a single bit error), which helps tremendously with delay (a message needing to be re-sent takes much more time than just correcting it on the spot).

2D parity takes the message, and turns the string of bits into a matrix of bits (arbitrarily). Then, for each row *and* column of the matrix, the sender computes a parity bit for the bits in that row/column. Then, the receiver checks the parity bits for all rows/columns. If there is a single bit error, the receiver will observe two incorrect parity bits - one for the row containing the incorrect bit, and one for the column containing the incorrect bit. With this, the receiver has enough information to know the precise location of the incorrect bit (and thus can correct it on the spot).

For example, suppose the message to send was `1 1 0 0 0 1 0 0 1 1 1 1 1 1 0 1`. With 2D parity, we would turn the message into a 4x4 matrix, and compute parity bits (in bold) for the rows and columns (assuming even parity scheme):

<pre>
1 1 0 0 <mark>0</mark>  
0 1 0 0 <mark>1</mark>  
1 1 1 1 <mark>0</mark>  
1 1 0 1 <mark>1</mark>  
<mark>1 0 1 0</mark>  
</pre>

Then, suppose the message arrived at the receiver with the 6th bit flipped from a 1 to a 0: `1 1 0 0 0 0 0 0 1 1 1 1 1 1 0 1`. The receiver would reconstruct the following matrix:

<pre>
1 1 0 0 0 
0 <b>0</b> 0 0 <b>1</b> 
1 1 1 1 0  
1 1 0 1 1  
1 <b>0</b> 1 0  
</pre>

And, with the problematic parity bits above bolded (as well as the flipped bit), it is obvious how the receiver could pinpoint the exact bit that has an error (and correct it).

**Internet Checksum** is a different error-detection method. It does the exact same job as a parity bit, but requires slightly more overhead. The tradeoff, however, is that the computation/calculation to determine whether an bit error is present can be done much, much faster. This is important for networks, as error detection should be an extremely short process compared to getting information from one place to another. For an explanation of why it is faster, as well as an example, look at P3 (the last chapter question) from my [Week 4 Portfolio Folder](https://gitlab.com/calebwalker/ic322-portfolio/-/blob/master/week04/vegetables.md).

**Cyclic Redundancy Check (CRC)** is a more robust error-detection method that involves a series of computations based on modulo-2 arithmetic that can detect errors based on just a few bits. It uses a property of this arithmetic to generate a key in the same fashion that the key is used - this property will become more obvious below, with the worked example.

The advantages of using CRC include being able to have a fairly resilient error-detection mechanism that requires minimal overhead. It has similar properties to a parity bit - as it turns out, *a parity bit is just a CRC scheme with a remainder of 1 bit!* Also, the method that CRC uses is easily implementable in hardware, so it is a popular choice for error-detection. One key disadvantage is that CRC (in the most basic form) offers no error *correction*. Additionally, while the method is easily implementable in hardware, that does not necessarily mean that it is fast.

For CRC, there are a few important data points to internalize before getting into the algorithm. The main method involves long division using bits. In the CRC case, the dividend of the operation is the message (as one long string of bits). The divisor is a previously agreed upon set of *L* bits that both the sender and the receiver know in advance (there are standardized divisors for certain CRC schemes). The divisor is also known as a 'generator', because it directly determines the remainder (which is the CRC function output). Additionally, if the generator is *L* bits long, the remainder will always end up being *L-1* bits long.

One important aspect of CRC is that the generator's most important bit must be a `1`. The following information in this paragraph deals with why, which is not necessary to understand how CRC works, so feel free to skip past. This requirement is due to the pure math foundation that supports the functionality of CRC. What the algorithm does is essentially represent the message as a polynomial with only `0` or `1` as the coefficients to the respective polynomial degrees. If we want to preserve a generator that has a highest degree of *n*, then the *n+1* bit must be a `1`, otherwise, the polynomial is simplified down to a lesser degree.

Another important step to keep in mind is that the sender appends *L-1* 0's to the end of the message before running the algorithm. Since the sender is *generating* a check value, it must figure out what the remainder is if the last three bits are all `0`. Then, due to the nature of arithmetic in a finite modulo-2 field, when the receiver performs the same operation with the calculated check value at the end, it should revert the remainder back to all `0`'s, indicating an error-free message.

Now, for a worked example, suppose the message we want to use for CRC is the following: `1101001110110101`. Also suppose that the generator we will use is the following 4 bits: `1011`. The generator being 4 bits long means that our remainder, or check value, will be 3 bits long. In our long division, we discard the quotient, since we only care about the remainder, so the following steps will only be the subtraction aspect. First, we add three `0`'s to the end of the message (the length of our remainder), so our dividend is now `1101001110110101 000`. Then, we start the long division, which involves placing the divisor at the first `1`, and then doing subtraction. Binary subtraction in a modulo-2 field ends up being the exact same operation as XOR, so the 'subtraction' in the following steps looks exactly like an XOR operation.

<pre>
<b>1101</b>001110110101000  
<b>1011</b>
||||  
vvvv  
0<b>1100</b>01110110101000  
 <b>1011</b>  
 ||||  
 vvvv  
 0<b>1110</b>1110110101000  
  <b>1011</b>  
  ||||  
  vvvv  
  0<b>1011</b>110110101000  
   <b>1011</b>  
   ||||  
   vvvv  
   0000<b>1101</b>10101000  
       <b>1011</b>  
       ||||  
       vvvv  
       0<b>1101</b>0101000  
        <b>1011</b>  
        ||||  
        vvvv  
        0<b>1100</b>101000  
         <b>1011</b>  
         ||||  
         vvvv  
         0<b>1111</b>01000  
          <b>1011</b>  
          ||||  
          vvvv  
          0<b>1000</b>1000  
           <b>1011</b>  
           ||||  
           vvvv  
           00<b>1110</b>00  
             <b>1011</b>  
             ||||  
             vvvv  
             0<b>1010</b>0  
              <b>1011</b>  
              ||||  
              vvvv  
              00<b>010</b>  
                |||  
                vvv  
                <b>010</b>
</pre>

And we see that our remainder is 010, which is what the sender appends to the end of the message. Then, when the receiver gets the message, it will look like this: `1101001110110101 010`. The receiver then performs the exact same algorithm, except with the `010` appended to the end (instead of `000`). This will (assuming no bit errors) result in a remainder of `000`, which means that the message is error-free! If the message *did* have one or more errors, the remainder would *likely* end up being non-zero, which would correctly identify a bit error in the message.

## I can describe the Ethernet protocol, including how it implements each of the Layer 2 services and how different versions of Ethernet differ.

**Ethernet** is a protocol that defines how a wired LAN operates, and has taken over almost the entire market for MAC protocols dealing with wired LANs. It is a very robust protocol that was conceived in the 1970s, made commercially available in the 1980s, and has thus far still remained one of the most common protocols used in computer networks.

Ethernet also shares many commonalities with the IP protocol, just among individual links rather than entire network paths (made up of multiple links). Ethernet uses MAC addresses instead of IP addresses, and instead of routers forwarding packets at layer 3, *hubs/switches* forward *frames* at layer 2. Ethernet also provides no guarantee that data will get across the link. Ethernet uses CRC, which only provides error-detection. Additionally, Ethernet does not have a system of acknowledgments, so a frame that has bit errors detected is simply dropped, and it is the job of a higher layer to handle re-transmission.

Ethernet initially utilized hubs, which were central units that, upon receiving a transmission on one interface, immediately re-transmitted a boosted transmission on all other interfaces. The layout of a LAN, then, was just all of the nodes connected to a central hub. However, this method was still susceptible to collisions (if the hub received more than one transmission simultaneously). This, of course, meant that multiple access control was necessary, hence why Ethernet is a CSMA/CD protocol. However, in the 2000s, Ethernet underwent a major change - instead of using hubs, *switches* were introduced. Unlike hubs, switches were more sophisticated, using a store-and-forward technique to eliminate collisions. Thus, a multiple access control protocol is rarely even needed.

There are different versions of Ethernet, examples of which are given in the textbook: `10BASE-T`, `10BASE-2`, `100BASE-T`, `1000BASE-LX`, `10GBASE-T`, and `40GBASE-T`. The first part of the version refers to the speed of the standard: `10` -> 10 Megabits/second, `1000` -> 1 Gigabit/second, and `40G` -> 40 Gigabits/second. Almost all Ethernet versions include `BASE` in the name - this refers to the medium that carries the version of Ethernet, and specifically refers to 'baseband' Ethernet in which the medium will only carry Ethernet traffic. Finally, the part at the end represents the type of physical medium; `T` -> twisted-pair copper wires, `F` -> fiber optic cables, and `_X` -> Fast Ethernet. Essentially, there need to be different versions of Ethernet for all different speeds and mediums, so each version uses slightly different implementations of the same Ethernet principles.