# Week 12: Link Layer

*MIDN Caleb Walker | Fall 2023*

* [Learning Objectives](learning-objectives.md)
* [Protocol Pioneer](protocol-pioneer.md)
* [Feedback for Caleb's Portfolio](https://gitlab.usna.edu/ckoutrakos/ic322-portfolio/-/issues/28)
* [Vegetables](vegetables.md)