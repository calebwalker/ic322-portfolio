# Week 1 Learning Objectives

## I can explain the role that the network core plays vs. the network edge.

The network edge is a good place to start, since that is where all traffic physically starts. The network edge is all of the 'things' that access the internet. Any computer, phone, server, or any other electronic device that has internet access is part of the edge. The network edge holds all of the information that is on the internet, and is responsible for all requests for this information. There are more connections closer to the edge, because each individual house/device/area needs a connection, but the connections are relatively slow, since it only needs to transmit information for one house/device/area.

The network core is the extensive and complex system that connects the edge together. It is made up of different tiers of ISPs and different levels of connections. The closer you get to the core, the more data is being transferred, because instead of just connecting one 'thing', these high-level ISPs connect millions of 'things'.

## I can compare different physical technologies used in access networks, including dial-up, DSL, cable, fiber, and wireless.

Dial-up internet uses the existing telephone infrastructure entirely. A person would use a modem to 'call' another modem, and then data would be transferred just like a normal phone call, except transmitting digital code rather than someone's voice. Dial-up's design meant that only one thing could be using the phone line at any given time - you could not make a call while accessing a webpage.

DSL uses the existing telephone infrastructure to some extent. It differs from dial-up because instead of actually using the calling system, a telco's local central office was installed with a DSL access multiplexer, and it would distinguish between phone calls and internet access. Also, for incoming information, every home had a splitter that did the same job. Data was not transferred the same - upload and download streams were transmitted at higher frequencies than phone calls, so DSL did not interfere with calling someone.

Cable internet uses the existing television infrastructure to provide access. It works almost identically to DSL, with the television company's central office having a cable modem termination system, which does the same thing as the DSL access multiplexer. And in every home, cable internet requires use of a cable modem, which is somewhat similar to a DSL splitter. One key difference is that cable is a shared broadcast medium, meaning a bunch of homes will be connected to one fiber node, and all homes connected to that node get sent and received information from all other homes.

Fiber internet is a direct approach to internet access. Optical fibers are connected directly to homes, and there are a few ways of achieving this currently. Companies can have a fiber connection for each home, or can have bigger fibers that split into smaller fibers for each home. Regardless of the method, this access type provides much higher speeds than DSL or cable.

Wireless internet is slightly different than the other types, because it is built on top of an already existing internet access technology. A local area network is able to be accessed wirelessly through access points, and those access points connect back to the main router, typically through ethernet. Then, the information is handled accordingly, through whatever main internet access technology is being used.

3G, LTE 4G, and 5G cellular internet access is similar to Wireless internet, except the company provides large access points which have a much bigger range than a wireless access point. It is a scaled-up version of wireless that is meant for many users at great ranges.

## I can use queueing, transmission, and propagation delays to calculate total packet delay, I can describe the underlying causes for these delays, and I can propose ways to improve packet delay on a network.

Queueing delay is the delay that comes from a router's inability to send out more packets than it is getting in. When the rate in is more than the rate out, a queue forms. This queue takes time to get through, and that time is represented as queueing delay.

Transmission delay is the time it takes for a router to transcribe the information of the packet onto the medium through which the packet will be sent. This is only the time it takes for the router to put the bits on the wire, and is NOT the time it takes for the information to travel the wire - transmission delay has no correlation with the length that the packet will travel along the medium.

Propagation delay is the time it takes information to be sent along a medium. This DOES correlate with the physical distance that the information needs to travel.

When considering how the delays affect a router sending multiple packets, there are some nuances to keep in mind. For example, if a router has finished transmitting a packet, it will immediately start transmitting the next packet, even if the data from the previous packet is still on the medium. Also, it is important to make sure the packets are queueing like they would in the real world, and that a router is not transmitting more than one packet at a time.

## I can describe the differences between packet-switched networks and circuit-switched networks.

A circuit-switched network is a simpler, older network system that was used for telephone connections. Essentially, when a machine on the network wants to communicate with another machine, the network reserves a path/line between the two machines. This reservation lasts for the duration of the communication, and no other communications can occur on that given line. This creates a very reliable but highly inefficient system - as the network grows, the amount of infrastructure needed to keep delays small grows extremely rapidly. It was not a viable system for the internet.

The internet is a packet-switched network. This means that there are no reserved communication lines, and everything can be used all the time. When a message is to be sent, it is broken up into a standard size, which is called a 'packet'. The bigger the message, the more packets needed to transmit the message. Then, each of these packets are labeled with a 'to' (and 'from') address and are all sent out. When a packet leaves its origin, it will make a stop at a router to figure out where to go next. The router's job is to send the packet in the general direction of the 'to' address, and then the packet will reach another router, and the process continues as the routers gradually refine the destination until the packet actually gets there. This is much better for larger networks, and since the internet is not normally used for keeping a constant flow of data, but rather bursts of data. The downside of a packet-switched network is that since there are no reservations of resources, packets can get backed up if links are all currently transmitting other packets.

## I can describe how to create multiple channels in a single medium using FDM and TDM.

Frequency Division Multiplexing (FDM) is one method of splitting a medium/link into multiple connections. With FDM, the link must be capable of transmitting a wide range of frequencies. Then, this range is split up into a number of channels, each of which is designated to have a portion of the overall range of frequencies. With this, multiple connections can take place on the same link, since each connection is only paying attention to its designated channel frequency.

Time Division Multiplexing (TDM) is the other method of splitting one medium/link into multiple connections. With TDM, each channel is instead given a certain time slot. Then, the connection is only allowed to transmit during that time slot, and the link rotates time slots rapidly, so that multiple communications can happen at the 'same time'. Channel 1 will transmit for a second, and then Channel 2 will transmit for the next second, and so on until it gets back to Channel 1.

## I can describe the hierarchy of ISPs and how ISPs at different of similar levels interact.

The hierarchy of ISPs starts at the lowest level, the Access ISPs. These ISPs are small, and simply provide homes or small networks access to the internet. These Access ISPs then connect further up the hierarchy in order to get data wherever it needs to go.

The next level in the hierarchy is Regional ISPs. These ISPs connect multiple Access ISPs together and can vary in scope. They will be able to connect physically close locations together, but typically are not able to directly get data across very large distances.

The final level is the Tier 1 ISPs. These are the largest ISPs, and have global scope. They connect Regional ISPs together (they can also connect directly with Access ISPs) and therefore must have a large infrastructure to handle the large amount of data going all over the world. The Tier 1 ISPs link to each other to make the final connection points for any device with internet access wanting to communicate with another device far away.

However, this hierarchy is not the only way that ISPs connect to each other. By avoiding Tier 1 ISPs, lower level ISPs can cut down on costs, and there are a number of ways that this can be done. The simplest way would be *peering*, which is when two lower level ISPs share traffic directly at no cost to either ISP. This allows any traffic going just between the two ISPs to occur at no cost - however, that amount of data might be relatively small, since not much traffic will be originating from one lower level ISP **and** going to another lower level ISP. Peering can work for a couple of arrangements, but on a larger scale, *IXPs* are used. These are connection points of ISPs - essentially peering on a larger scale, with minor cost of upkeep. IXPs connect many lower level ISPs together, so that any traffic staying within the interconnected ISPs can be transmitted at much lower costs.

## I can explain how encapsulation is used to implement the layered model of the Internet.

The internet relies on a layered system, which is helpful because at each level, different protocols ensure different functionalities. The highest level is the application layer, dealing with HTTP and SMTP protocols, and the levels get lower until it reaches the physical layer, which is literally bits across a wire.

Each layer has one job, and different protocols on how to perform that job. The protocols add information to a packet, and the packet is 'wrapped' in information at every layer until it becomes a frame at the link layer. When sending a packet, the packet goes through this process regularly - it is encoded with an application layer protocol, then that message is given to the transport layer, which adds its own protocol, and so on until it becomes a frame. Then, when the frame reaches its destination, it undergoes the process in reverse. The link-layer protocol is processed, and the remaining datagram is passed to the network layer, and so on.