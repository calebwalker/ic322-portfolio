# Questions from *Kurose* Chapter 1

## R1: What is the difference between a host and an end system? List several different types of end systems.

A host and an end system are basically the same thing. The textbook explains that for the context of this class, the terms can be used effectively interchangeably.

A host or end system is anything that is connected to the internet that information stops at or generates from. This means that the classification is very broad - computers, web servers, printers, remote doorbells, and mobile phones are all examples of hosts/end systems.

Computer jargon often includes a specification of a host, such as a server-client distinction, where a server is a host that mostly provides information/files, and a client is a host that mostly receives information/files.

## R4: List four access technologies. Classify each one as home access, enterprise access, or wide-area wireless access.

* Mobile Networks/Cellular (3G, 4G LTE, 5G): Wide-area wireless access
* Cable: Home access
* Wi-Fi: Enterprise (and home) access
* FTTH (direct fiber): home access

## R11: Suppose there is exactly one packet switch between a sending host and a receiving host. The transmission rates between the sending host and the switch and between the switch and the receiving host are R<sub>1</sub> and R<sub>2</sub>, respectively. Assuming that the switch uses store-and-forward packet switching, what is the total end-to-end delay to send a packet of length L? (Ignore queuing, propagation delay, and processing delay).

The textbook defines transmission delay as the total length of the packet (in bits) divided by the transmission rate.

In this example, there are two times the packet needs to be transmitted, so the total delay will be the sum of each individual delay. As an equation:

T<sub>d</sub> = L/R<sub>1</sub> + L/R<sub>2</sub>

## R12: What advantage does a circuit-switched network have over a packet-switched network? What advantages does TDM have over FDM in a circuit-switched network?

A circuit-switched network is much better for constant streams of data to be transmitted. It is more reliable, and is able to reserve a specific channel for the data to constantly flow.

TDM has the advantage of being easier to implement. To implement FDM, the medium through which data is being sent must be able to support a wide range of frequencies, and the receivers also must be able to listen to a wide range of frequencies. It also requires a bit more computation, probably requiring some sort of demodulation or Fast Fourier Transform algorithm to be able to distinguish the correct frequency. TDM is simpler in the fact that it just uses one frequency, and the receivers just need to only listen during a specific time.

## R13: Suppose users share a 2 Mbps link. Also suppose each user transmits continuously at 1 Mbps when transmitting, but each user transmits only 20% of the time.

### a. When circuit switching is used, how many users can be supported?

For circuit switching, one could use TDM to split the 2 Mbps channel into two 1 Mbps channels, and then 2 users could be supported.

### b. For the remainder of this problem, suppose packet switching is used. Why will there be essentially no queuing delay before the link if two or fewer users transmit at the same time? Why will there be a queuing delay if three users transmit at the same time?

If two or fewer users transmit, then the rate at which information is going through the network (which is always 2 Mbps or less) will never be more than the capacity of the network. This means that the links will be able to fully transmit any incoming data before more data arrives. However, when a third user starts transmitting, there will be more data coming in than the links can transmit, so there will be some data that must wait as previous data is still getting transmitted. This delay is queuing delay.

### c. Find the probability that a given user is transmitting.

The probability is given in the question description (0.2 or 20%).

### d. Suppose now there are three users. Find the probability that at any given time, all three users are transmitting simultaneously. Find the fraction of time during which the queue grows.

The probability that all users are transmitting is 0.2<sup>3</sup>, which is 0.008 (or 0.8%).

This means that the queue will grow only 1/125 of the time.

## R14: Why will two ISPs at the same level of the hierarchy often peer with each other? How does an IXP earn money?

Regional and Access ISPs only provide smaller levels of internet. It is up to them to find ways to connect users to the rest of the internet. This is mostly done by paying a tier-1 ISP. However, if the bulk of their data is only going to another smaller ISP's network, then it is not fiscally efficient to pay the tier-1 ISP for such a short connection. Thus, many smaller ISPs will directly link up networks, and then the connections require no money to maintain.

IXPs are just this peering scheme on a bigger level: they connect lots of smaller ISPs together in a central hub. They make money because they do typically charge the smaller ISPs for the service. The smaller ISPs are willing to pay because it is typically much less money than paying a tier-1 ISP.

## R18: How long does it take a packet of length 1,000 bytes to propagate over a link of distance 2,500 km, propagation speed 2.5 * 10<sup>8</sup> m/s, and transmission rate of 2 Mbps? More generally, how long does it take a packet of length L to propagate over a link of distance *d*, propagation speed *s*, and transmission rate *R* bps? Does this delay depend on packet length? Does this delay depend on transmission rate?

The total delay is a sum of all individual delays (queuing delay, transmission delay, and propagation delay).

In this example, there is only one packet, so queuing delay is irrelevant.

Transmission delay is given by the textbook as L/R, where L is the length of the packet and R is the transmission rate.

Propagation delay is given by the textbook as d/s, where d is the distance the information has to travel, and s is the propagation speed.

Thus, in general, the total delay for one packet is d/s + L/R.

## R19: Suppose Host A wants to send a large file to Host B. The path from Host A to Host B has three links, of rates R<sub>1</sub> = 500 kbps, R<sub>2</sub> = 2 Mbps, and R<sub>3</sub> = 1 Mbps.

### a. Assuming no other traffic in the network, what is the throughput for the file transfer?

Throughput with no traffic and only transmission rates given will be whatever the lowest transmission rate is. The throughput is bottlenecked by the slowest link, which in this case is R<sub>1</sub>, with a transmission rate of 500 kbps.

### b. Suppose the file is 4 million bytes. Dividing the file size by the throughput, roughly how long will it take to transfer the file to Host B?

We can calculate the total time it would take to transfer the entire file through the bottlenecked link, and neglect the time it takes the last bit of information to travel through the second and third links (because the file is so large).

In this case, the time to transfer would be L/R, where L is the size of the file.

(4 * 10<sup>6</sup>)/(500 * 10<sup>3</sup>) = 8 seconds

### c. Repeat (a) and (b), but now with R<sub>2</sub> reduced to 100 kbps.

The bottlenecked link is now R<sub>2</sub>, but the calculation is essentially the same.

(4 * 10<sup>6</sup>)/(100 * 10<sup>3</sup>) = 40 seconds