# Wireshark Introduction Lab

## Introduction

In this lab, I explored Wireshark and learned about the features and functions of a packet sniffer.

## Collaboration

I did not use any additional resources, nor did I get help from anyone on this lab.

## Process

I completed the "Getting Started" Wireshark lab on the [textbook's website](http://gaia.cs.umass.edu/kurose_ross/wireshark.php) (version 8.1). I didn't deviate from the instructions.

## Answers to lab questions

### Q1: Which of the following protocols are shown as appearing in your trace file?

In my trace file, I see a lot of TCP protocols, as well as TLSv1.2 and some TLSv1.3. There are also the occasional DNS protocol, and some SSH protocols as well.

### Q2: How long did it take from when the HTTP GET message was sent until the HTTP OK reply was received?

It took 0.023172 seconds.

### Q3: What is the Internet address of the gaia.cs.umass.edu? What is the Internet address of your computer?

The gaia.cs.umass.edu server is located at 128.119.245.12. My computer is 10.25.153.87.

### Q4: What type of Web browser issued the HTTP request?

Looking at the User Agent field of the HTTP section of the packet, it reads:

```
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36\r\n
```

It would appear that even though I used a Chrome browser to access the webpage, some other underlying software was also used (Safari and Mozilla).

### Q5: Expand the information on the Transmission Control Protocol for this packet in the Wireshark "Details of selected packet" window so you can see the fields in the TCP segment carrying the HTTP message. What is the destination port number (the number following "Dest Port:" for the TCP segment containing the HTTP request) to which this HTTP request is being sent?

Port 80. I already know that this is the default port for HTTP traffic, so this checks out.

### Q6: Print the two HTTP messages (GET and OK) referred to in question 2 above.

The GET message:
```
No. Time Source Destination Protocol Length Info
346 4.339072 10.25.153.87 128.119.245.12 HTTP 527 GET /wireshark-labs/INTRO-wireshark-file1.html HTTP/1.1
Frame 346: 527 bytes on wire (4216 bits), 527 bytes captured (4216 bits) on interface \Device\NPF_{0BC3CEEE-9CC8-4A4B-A1CD-71F8A5EBCCD0}, id 0
Section number: 1
Interface id: 0 (\Device\NPF_{0BC3CEEE-9CC8-4A4B-A1CD-71F8A5EBCCD0})
Interface name: \Device\NPF_{0BC3CEEE-9CC8-4A4B-A1CD-71F8A5EBCCD0}
Interface description: Wi-Fi
Encapsulation type: Ethernet (1)
Arrival Time: Aug 28, 2023 22:35:10.848197000 Eastern Daylight Time
[Time shift for this packet: 0.000000000 seconds]
Epoch Time: 1693276510.848197000 seconds
[Time delta from previous captured frame: 0.003718000 seconds]
[Time delta from previous displayed frame: 0.003718000 seconds]
[Time since reference or first frame: 4.339072000 seconds]
Frame Number: 346
Frame Length: 527 bytes (4216 bits)
Capture Length: 527 bytes (4216 bits)
[Frame is marked: False]
[Frame is ignored: False]
[Protocols in frame: eth:ethertype:ip:tcp:http]
[Coloring Rule Name: HTTP]
[Coloring Rule String: http || tcp.port == 80 || http2]
Ethernet II, Src: IntelCor_54:b2:df (f0:77:c3:54:b2:df), Dst: ArubaaHe_11:71:80 (b8:d4:e7:11:71:80)
Destination: ArubaaHe_11:71:80 (b8:d4:e7:11:71:80)
Address: ArubaaHe_11:71:80 (b8:d4:e7:11:71:80)
.... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
.... ...0 .... .... .... .... = IG bit: Individual address (unicast)
Source: IntelCor_54:b2:df (f0:77:c3:54:b2:df)
Address: IntelCor_54:b2:df (f0:77:c3:54:b2:df)
.... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
.... ...0 .... .... .... .... = IG bit: Individual address (unicast)
Type: IPv4 (0x0800)
Internet Protocol Version 4, Src: 10.25.153.87, Dst: 128.119.245.12
0100 .... = Version: 4
.... 0101 = Header Length: 20 bytes (5)
Differentiated Services Field: 0x00 (DSCP: CS0, ECN: Not-ECT)
0000 00.. = Differentiated Services Codepoint: Default (0)
.... ..00 = Explicit Congestion Notification: Not ECN-Capable Transport (0)
Total Length: 513
Identification: 0x7be3 (31715)
010. .... = Flags: 0x2, Don't fragment
0... .... = Reserved bit: Not set
.1.. .... = Don't fragment: Set
..0. .... = More fragments: Not set
...0 0000 0000 0000 = Fragment Offset: 0
Time to Live: 128
Protocol: TCP (6)
Header Checksum: 0x0000 [validation disabled]
[Header checksum status: Unverified]
Source Address: 10.25.153.87
Destination Address: 128.119.245.12
Transmission Control Protocol, Src Port: 49239, Dst Port: 80, Seq: 1, Ack: 1, Len: 473
Source Port: 49239
Destination Port: 80
[Stream index: 14]
[Conversation completeness: Incomplete, DATA (15)]
[TCP Segment Len: 473]
Sequence Number: 1 (relative sequence number)
Sequence Number (raw): 1328432575
[Next Sequence Number: 474 (relative sequence number)]
Acknowledgment Number: 1 (relative ack number)
Acknowledgment number (raw): 2154575286
0101 .... = Header Length: 20 bytes (5)
Flags: 0x018 (PSH, ACK)
000. .... .... = Reserved: Not set
...0 .... .... = Accurate ECN: Not set
.... 0... .... = Congestion Window Reduced: Not set
.... .0.. .... = ECN-Echo: Not set
.... ..0. .... = Urgent: Not set
.... ...1 .... = Acknowledgment: Set
.... .... 1... = Push: Set
.... .... .0.. = Reset: Not set
.... .... ..0. = Syn: Not set
.... .... ...0 = Fin: Not set
[TCP Flags: ·······AP···]
Window: 512
[Calculated window size: 131072]
[Window size scaling factor: 256]
Checksum: 0x1ae8 [unverified]
[Checksum Status: Unverified]
Urgent Pointer: 0
[Timestamps]
[Time since first frame in this TCP stream: 0.251665000 seconds]
[Time since previous frame in this TCP stream: 0.233123000 seconds]
C:\Users\m256642\A ppData\Local\Temp\wireshark _Wi-FiZ8DA A 2.pcapng 383 total pack ets, 383 shown
[SEQ/ACK analysis]
[iRTT: 0.018542000 seconds]
[Bytes in flight: 473]
[Bytes sent since last PSH flag: 473]
TCP payload (473 bytes)
Hypertext Transfer Protocol
GET /wireshark-labs/INTRO-wireshark-file1.html HTTP/1.1\r\n
[Expert Info (Chat/Sequence): GET /wireshark-labs/INTRO-wireshark-file1.html HTTP/1.1\r\n]
[GET /wireshark-labs/INTRO-wireshark-file1.html HTTP/1.1\r\n]
[Severity level: Chat]
[Group: Sequence]
Request Method: GET
Request URI: /wireshark-labs/INTRO-wireshark-file1.html
Request Version: HTTP/1.1
Host: gaia.cs.umass.edu\r\n
Connection: keep-alive\r\n
Upgrade-Insecure-Requests: 1\r\n
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36\r\n
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signedexchange;
v=b3;q=0.7\r\n
Accept-Encoding: gzip, deflate\r\n
Accept-Language: en-US,en;q=0.9\r\n
\r\n
[Full request URI: http://gaia.cs.umass.edu/wireshark-labs/INTRO-wireshark-file1.html]
[HTTP request 1/1]
[Response in frame: 349]
```

The OK message:
```
No. Time Source Destination Protocol Length Info
349 4.362244 128.119.245.12 10.25.153.87 HTTP 492 HTTP/1.1 200 OK (text/html)
Frame 349: 492 bytes on wire (3936 bits), 492 bytes captured (3936 bits) on interface \Device\NPF_{0BC3CEEE-9CC8-4A4B-A1CD-71F8A5EBCCD0}, id 0
Section number: 1
Interface id: 0 (\Device\NPF_{0BC3CEEE-9CC8-4A4B-A1CD-71F8A5EBCCD0})
Interface name: \Device\NPF_{0BC3CEEE-9CC8-4A4B-A1CD-71F8A5EBCCD0}
Interface description: Wi-Fi
Encapsulation type: Ethernet (1)
Arrival Time: Aug 28, 2023 22:35:10.871369000 Eastern Daylight Time
[Time shift for this packet: 0.000000000 seconds]
Epoch Time: 1693276510.871369000 seconds
[Time delta from previous captured frame: 0.002431000 seconds]
[Time delta from previous displayed frame: 0.002431000 seconds]
[Time since reference or first frame: 4.362244000 seconds]
Frame Number: 349
Frame Length: 492 bytes (3936 bits)
Capture Length: 492 bytes (3936 bits)
[Frame is marked: False]
[Frame is ignored: False]
[Protocols in frame: eth:ethertype:ip:tcp:http:data-text-lines]
[Coloring Rule Name: HTTP]
[Coloring Rule String: http || tcp.port == 80 || http2]
Ethernet II, Src: ArubaaHe_11:71:80 (b8:d4:e7:11:71:80), Dst: IntelCor_54:b2:df (f0:77:c3:54:b2:df)
Destination: IntelCor_54:b2:df (f0:77:c3:54:b2:df)
Address: IntelCor_54:b2:df (f0:77:c3:54:b2:df)
.... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
.... ...0 .... .... .... .... = IG bit: Individual address (unicast)
Source: ArubaaHe_11:71:80 (b8:d4:e7:11:71:80)
Address: ArubaaHe_11:71:80 (b8:d4:e7:11:71:80)
.... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
.... ...0 .... .... .... .... = IG bit: Individual address (unicast)
Type: IPv4 (0x0800)
Internet Protocol Version 4, Src: 128.119.245.12, Dst: 10.25.153.87
0100 .... = Version: 4
.... 0101 = Header Length: 20 bytes (5)
Differentiated Services Field: 0x00 (DSCP: CS0, ECN: Not-ECT)
0000 00.. = Differentiated Services Codepoint: Default (0)
.... ..00 = Explicit Congestion Notification: Not ECN-Capable Transport (0)
Total Length: 478
Identification: 0x65a7 (26023)
010. .... = Flags: 0x2, Don't fragment
0... .... = Reserved bit: Not set
.1.. .... = Don't fragment: Set
..0. .... = More fragments: Not set
...0 0000 0000 0000 = Fragment Offset: 0
Time to Live: 42
Protocol: TCP (6)
Header Checksum: 0xd07e [validation disabled]
[Header checksum status: Unverified]
Source Address: 128.119.245.12
Destination Address: 10.25.153.87
Transmission Control Protocol, Src Port: 80, Dst Port: 49239, Seq: 1, Ack: 474, Len: 438
Source Port: 80
Destination Port: 49239
[Stream index: 14]
[Conversation completeness: Incomplete, DATA (15)]
[TCP Segment Len: 438]
Sequence Number: 1 (relative sequence number)
Sequence Number (raw): 2154575286
[Next Sequence Number: 439 (relative sequence number)]
Acknowledgment Number: 474 (relative ack number)
Acknowledgment number (raw): 1328433048
0101 .... = Header Length: 20 bytes (5)
Flags: 0x018 (PSH, ACK)
000. .... .... = Reserved: Not set
...0 .... .... = Accurate ECN: Not set
.... 0... .... = Congestion Window Reduced: Not set
.... .0.. .... = ECN-Echo: Not set
.... ..0. .... = Urgent: Not set
.... ...1 .... = Acknowledgment: Set
.... .... 1... = Push: Set
.... .... .0.. = Reset: Not set
.... .... ..0. = Syn: Not set
.... .... ...0 = Fin: Not set
[TCP Flags: ·······AP···]
Window: 237
[Calculated window size: 30336]
[Window size scaling factor: 128]
Checksum: 0x099d [unverified]
[Checksum Status: Unverified]
Urgent Pointer: 0
[Timestamps]
[Time since first frame in this TCP stream: 0.274837000 seconds]
[Time since previous frame in this TCP stream: 0.002431000 seconds]
[SEQ/ACK analysis]
[iRTT: 0.018542000 seconds]
[Bytes in flight: 438]
C:\Users\m256642\A ppData\Local\Temp\wireshark _Wi-FiZ8DA A 2.pcapng 383 total pack ets, 383 shown
[Bytes sent since last PSH flag: 438]
TCP payload (438 bytes)
Hypertext Transfer Protocol
HTTP/1.1 200 OK\r\n
[Expert Info (Chat/Sequence): HTTP/1.1 200 OK\r\n]
[HTTP/1.1 200 OK\r\n]
[Severity level: Chat]
[Group: Sequence]
Response Version: HTTP/1.1
Status Code: 200
[Status Code Description: OK]
Response Phrase: OK
Date: Tue, 29 Aug 2023 02:35:10 GMT\r\n
Server: Apache/2.4.6 (CentOS) OpenSSL/1.0.2k-fips PHP/7.4.33 mod_perl/2.0.11 Perl/v5.16.3\r\n
Last-Modified: Mon, 28 Aug 2023 05:59:01 GMT\r\n
ETag: "51-603f5641c64cc"\r\n
Accept-Ranges: bytes\r\n
Content-Length: 81\r\n
[Content length: 81]
Keep-Alive: timeout=5, max=100\r\n
Connection: Keep-Alive\r\n
Content-Type: text/html; charset=UTF-8\r\n
\r\n
[HTTP response 1/1]
[Time since request: 0.023172000 seconds]
[Request in frame: 346]
[Request URI: http://gaia.cs.umass.edu/wireshark-labs/INTRO-wireshark-file1.html]
File Data: 81 bytes
Line-based text data: text/html (3 lines)
<html>\n
Congratulations! You've downloaded the first Wireshark lab file!\n
</html>\n
```