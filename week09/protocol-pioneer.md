# Lab: Protocol Pioneer

## Introduction

This lab involved playing LCDR Downs' beta-version game and testing its functionality. The game is intended to teach others about network protocols without explicitly referring to them.

## Collaboration/Citations

The game is located [here](https://gitlab.com/jldowns-usna/protocol-pioneer/-/tree/main?ref_type=heads). I did not use any additional resources, nor did I get help from anyone on this lab.

## Process

I followed the lab instructions, and completed Chapter 5 of the game. This was all in accordance with the instructions outlined in the [Protocol Pioneer Lab](https://courses.cs.usna.edu/IC322/#/assignments/week07/protocol-pioneer/).

## Questions

### Q1: How did you solve the Chapters? Please copy and paste your winning strategy(s), and also explain it in English.

For the fifth chapter, there were multiple strategies that were involved, the main one being the drone strategy. My drone strategy followed a simple Distance Vector implementation. There were some nuances pertaining to this specific environment, such as the ability to only send one message per tick. When using a DV implementation, the idea is to broadcast an updated dv to all neighbors. However, to do this, I had to iterate through all neighbors one tick at a time, and so I kept track of which interface with the `interfaceIndex` variable. Importantly, the drones also had to know the difference between a updated DV packet and a message packet, which was done with a "Type" field of the packet.

```python
def drone_strategy(self):

    # variable to keep track of what interface we need to send a message to (we can only send one message per tick
    if "interfaceIndex" not in self.state:
        self.state["interfaceIndex"] = 0
    
    # the distance vector (only has a dummy 'self' entry for now)
    if "dv" not in self.state:
        self.state["dv"] = {self.id:{"Interface":'X', "Distance":0}}

    # keep track of what tick it is
    if "ticks" not in self.state:
        self.state["ticks"] = 1
    else:
        self.state["ticks"] += 1

    # check to see if we need to send out our updated dv
    if self.state["interfaceIndex"] < len(self.connected_interfaces()):
        msg = create_msg(self.id, "null", self.state["ticks"], "DV", self.state["dv"])
        self.send_message(msg, self.connected_interfaces()[self.state["interfaceIndex"]])
        self.state["interfaceIndex"] += 1
    
    while(self.message_queue):
        m = self.message_queue.pop()
        msg = parse_msg(m.text)

        # update dv if we receive updated dv from neighbor
        if(msg["Type"] == "DV"):
            if update_dv(msg, self.state["dv"], self.state["ticks"], m.interface):
                self.state["interfaceIndex"] = 0

        # if packet is a message, forward it (if we have destination in dv)
        elif(msg["Type"] == "Message"):
            if msg["Destination"] in self.state["dv"]:
                self.send_message(m.text, self.state["dv"][msg["Destination"]]["Interface"])
```

The `update_dv` method simply implements the DV algorithm. The 'weight' of a given path is actually calculated using ticks, instead of just a constant time, since packets don't take a constant amount of time to traverse a link. One important aspect of the method is how it returns whether or not the local DV was changed - this return value is used to determine whether or not the calling drone needs to broadcast its own new DV.

```python
# update dv using received message, and return whether or not the dv was changed
def update_dv(m, dv, currTick, interface):

    # calculate distance to source
    weight = currTick-m["TickSent"]

    # flag to indicate if dv has changed
    flag = False

    # if we didn't even know about the source node to begin with, add it to our dv
    if m["Source"] not in dv:
        dv[m["Source"]] = {"Interface":interface, "Distance":weight}
        flag = True

    # otherwise, look at the dv it sent and compare
    else:
        rcvd_dv = m["Payload"]
        for element in rcvd_dv:
            totalDistance = rcvd_dv[element]["Distance"]+weight

            # for every item in received dv, if (1) we don't have it, or (2) it is a better path to a node, then add to/change our dv
            if element not in dv or totalDistance < dv[element]["Distance"]:
                dv[element] = {"Interface":interface, "Distance":totalDistance}
                flag = True

    return flag
```

My player strategy was fairly simple. It basically just involved waiting for the 'access' drone to send a DV with Sawblade as one of the destinations with a calculated/measurable distance. Whenever it got this message, that meant that a path was now fully available to Sawblade. So, the player creates a new key and sends it to Sawblade.

```python
def player_strategy(self):
    
    # keep track of what tick it is
    if "ticks" not in self.state:
        self.state["ticks"] = 1
    else:
        self.state["ticks"] += 1

    while(self.message_queue):
        m = self.message_queue.pop()
        msg_rcvd = parse_msg(m.text)

        # if we receive a dv from 'access' drone with Sawblade, then we send the message knowing it will get there
        if "Sawblade" in msg_rcvd["Payload"]:
            msg_send = create_msg("null", "Sawblade", self.state["ticks"], "Message", self.generate_key()[0])
            self.send_message(msg_send, self.connected_interfaces()[0])
```

My Sawblade strategy was also fairly simple. First, at the very first tick, Sawblade would send out her own dummy DV to let the 'access' drone know about her presence on the network. Her presence is then distributed to all drones on the network. Then, Sawblade waits until a packet with "Type: Message" arrived. This packet has to be the one the player sent, so Sawblade should operate the radio with the given key.

```python
def sawblade_strategy(self):

    # keep track of what tick it is
    if "ticks" not in self.state:
        self.state["ticks"] = 1
        
        # send initial dummy dv to let 'access' drone know of our existence
        msg = create_msg("Sawblade", "null", self.state["ticks"], "DV", {"Sawblade":{"Interface":'X', "Distance":0}})
        self.send_message(msg, self.connected_interfaces()[0])
    else:
        self.state["ticks"] += 1

    # if we receive a "message" type packet, the payload must be the key, and operate the radio
    while(self.message_queue):
        m = self.message_queue.pop()
        msg = parse_msg(m.text)
        if(msg["Type"] == "Message"):
            self.operate_radio(msg["Payload"])
```

### Q2: Include a section on Beta Testing Notes.

This chapter was a really large leap in complexity. Not only was there three separate strategies, but now the drones were randomized, and it was a lot to get everything working correctly. It might be relevant in this chapter to include a note about json, and how it works, because I was dreading having to create my own object-to-string and string-to-object methods. This chapter did introduce essentially full control over the message protocol, which was cool, and challenged me to build everything essentially from scratch. Overall, a hard chapter, but very useful in terms of learning about networks.