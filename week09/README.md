# Week 9: The Network Layer: Control Plane

*MIDN Caleb Walker | Fall 2023*

* [Learning Objectives](learning-objectives.md)
* [Protocol Pioneer](protocol-pioneer.md)
* [Feedback for Austria's Portfolio](https://gitlab.usna.edu/laustria/ic322-portfolio-la/-/issues/15)
* [Vegetables](vegetables.md)