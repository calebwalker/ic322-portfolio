# Questions from *Kurose* Chapter 5

## R5:  What is the "count-to-infinity" problem in distance vector routing?

From [Learning Objectives](learning-objectives.md), 2nd learning goal:

One problem that arises in DV algorithms is known as the **count-to-infinity** problem. This problem involves a somewhat specific scenario, but introduces a huge flaw. The scenario is as such: suppose there are three nodes *a*, *b*, and *c*. The connections from *a* to *b* and *b* to *c* are both 1, but the connection from *a* to *c* is large (1000). If this is the case, *a* thinks it can get to *c* in 2 steps (through b). If the connection from *b* to *c* jumps up to 1000, then *b* will search for a faster connection to *c*. It realizes that *a* can get to *c* in 2, so *b* thinks "Hey, I can get to *c* in 3 if I just go through *a*!" However, this is not the case, since that cost assumed *b* could get to *c* in 1. Nonetheless, *b* sends *a* an updated table (with the cost from *b* to *c* set to 3 now) and *a* thinks "Oh, ok, now it takes 4 for me to send something to *c*" and *a* updates its distance vector. It then sends it to *b*, and *b* recalculates and sends back to *a*. This will continue until the cost finally reaches 1000, at which point *a* will finally update the table correctly to use the direct connection to *c* as the shortest path. *Pictured below is a visual representation of the graph, for reference:*

![Visual Representation of Count-to-Infinity Graph](../images/week09/DVProblem.jpg)

## R8: True or False: When an OSPF router sends its link state information, it is sent only to those nodes directly attached neighbors. Explain.

False - an OSPF router broadcasts its link state information to all nodes in the AS system. This is because OSPF is a link-state protocol, which means the algorithm needs the entire network topology before it can calculate shortest paths. Thus, every node in the system broadcasts its link state information to all other nodes, so that each node has the same view of the entire network.

## R9: What is meant by an *area* in an OSPF autonomous system? Why was the concept of an area introcudced?

An "area" in an OSPF autonomous system is defined similar to a subnet - it is a set of routers within the network that run their own OSPF protocol. Each area has one or more routers called 'border routers' that send and receive packets outside the area, and exactly one area is defined as the 'backbone area', which facilitates inter-area communication. The reason for areas being introduced is that it allows for the amount of broadcasts to be cut down, since routers only broadcast to other routers in their respective area. Essentially, a hierarchy is created to distribute computation/routing.

## P3: Consider the following network. With the indicated link costs, use Dijkstra's shortest-path algorithm to compute the shortest path from *x* to all network nodes. Show how the algorithm works by computing a table similar to Table 5.1.

![P3 network](../images/week09/P3.png)

| Step |    N'   | D(z), p(z) | D(y), p(y) | D(t), p(t) | D(v), p(v) | D(u), p(u) | D(w), p(w) |
| :--: | :-----: | :--------: | :--------: | :--------: | :--------: | :--------: | :--------: |
|  0   |    x    |    8, x    |    6, x    |     ??     |  **3, x**  |     ??     |    6, x    |
|  1   |   xv    |    8, x    |  **6, x**  |    7, v    |            |    6, v    |    6, x    |
|  2   |   xvy   |    8, x    |            |    7, v    |            |  **6, v**  |    6, x    |
|  3   |  xvyu   |    8, x    |            |    7, v    |            |            |  **6, x**  |
|  4   |  xvyuw  |    8, x    |            |  **7, v**  |            |            |            |
|  5   | xvyuwt  |  **8, x**  |            |            |            |            |            |
|  6   | xvyuwtz |            |            |            |            |            |            |

**NOTE:** ?? = infinity

## P4 (only node *v*, *y*, and *w*): Consider the network shown in Problem P3. Using Dijkstra's algorithm, and showing your work using a table similar to Table 5.1, do the following:

### Compute the shortest path from *v* to all network nodes.

| Step |    N'   | D(z), p(z) | D(y), p(y) | D(t), p(t) | D(x), p(x) | D(u), p(u) | D(w), p(w) |
| :--: | :-----: | :--------: | :--------: | :--------: | :--------: | :--------: | :--------: |
|  0   |    v    |     ??     |    8, v    |    4, v    |  **3, v**  |    3, v    |    4, v    |
|  1   |   vx    |   11, x    |    8, v    |    4, v    |            |  **3, v**  |    4, v    |
|  2   |   vxu   |   11, x    |    8, v    |  **4, v**  |            |            |    4, v    |
|  3   |  vxut   |   11, x    |    8, v    |            |            |            |  **4, v**  |
|  4   |  vxutw  |   11, x    |  **8, v**  |            |            |            |            |
|  5   | vxutwy  | **11, x**  |            |            |            |            |            |
|  6   | vxutwyz |            |            |            |            |            |            |

### Compute the shortest path from *y* to all network nodes.

| Step |    N'   | D(z), p(z) | D(x), p(x) | D(t), p(t) | D(v), p(v) | D(u), p(u) | D(w), p(w) |
| :--: | :-----: | :--------: | :--------: | :--------: | :--------: | :--------: | :--------: |
|  0   |    y    |   12, y    |  **6, y**  |    7, y    |    8, y    |     ??     |     ??     |
|  1   |   yx    |   12, y    |            |  **7, y**  |    8, y    |     ??     |   12, x    |
|  2   |   yxt   |   12, y    |            |            |  **8, y**  |    9, t    |   12, x    |
|  3   |  yxtv   |   12, y    |            |            |            |  **9, t**  |   12, x    |
|  4   |  yxtvu  | **12, y**  |            |            |            |            |   12, x    |
|  5   | yxtvuz  |            |            |            |            |            | **12, x**  |
|  6   | yxtvuzw |            |            |            |            |            |            |

### Compute the shortest path from *w* to all network nodes.

| Step |    N'   | D(z), p(z) | D(y), p(y) | D(t), p(t) | D(v), p(v) | D(u), p(u) | D(x), p(x) |
| :--: | :-----: | :--------: | :--------: | :--------: | :--------: | :--------: | :--------: |
|  0   |    w    |     ??     |     ??     |     ??     |    4, w    |  **3, w**  |    6, w    |
|  1   |   wu    |     ??     |     ??     |    5, u    |  **4, w**  |            |    6, w    |
|  2   |   wuv   |     ??     |   12, v    |  **5, u**  |            |            |    6, w    |
|  3   |  wuvt   |     ??     |   12, v    |            |            |            |  **6, w**  |
|  4   |  wuvtx  |   14, x    | **12, v**  |            |            |            |            |
|  5   | wuvtxy  | **14, x**  |            |            |            |            |            |
|  6   | wuvtxyz |            |            |            |            |            |            |

## P5: Consider the network shown below, and assume that each node initially knows the costs to each of its neighbors. Consider the distance-vector algorithm and show the distance table entries at node *z*.

![P5 network](../images/week09/P5.png)

**NOTE:** ? = infinity

### Round 0

|     |  z  |  x  |  v  |
| :-: | :-: | :-: | :-: |
|  z  |  0  |  2  |  6  |
|  x  |  ?  |  ?  |  ?  |
|  v  |  ?  |  ?  |  ?  |

### Round 1

|     |  z  |  x  |  v  |  u  |  y  |
| :-: | :-: | :-: | :-: | :-: | :-: |
|  z  |  0  |  2  |  5  |  7  |  5  |
|  x  |  2  |  0  |  3  |  ?  |  3  |
|  v  |  6  |  3  |  0  |  1  |  ?  |

### Round 2

|     |  z  |  x  |  v  |  u  |  y  |
| :-: | :-: | :-: | :-: | :-: | :-: |
|  z  |  0  |  2  |  5  |  6  |  5  |
|  x  |  2  |  0  |  3  |  4  |  3  |
|  v  |  6  |  3  |  0  |  1  |  3  |

## P11: Consider Figure 5.7. Suppose there is another router *w*, connected to router *y* and *z*. The costs of all links are given as follows: *c*(*x*, *y*) = 4, *c*(*x*, *z*) = 50, *c*(*y*, *w*) = 1, *c*(*z*, *w*) = 1, *c*(*y*, *z*) = 3. Suppose that poisoned reverse is used in the distance-vector routing algorithm.

![P11 network](../images/week09/P11.jpg)

### a. When the distance vector routing is stabilized, router *w*, *y*, and *z* inform their distances to *x* to each other. What distance values do they tell each other?

* Router *w* receives Dy(x) = 4 and Dz(x) = infinity
* Router *y* receives Dz(x) = 6 and Dw(x) = infinity
* Router *z* receives Dy(x) = 4 and Dw(x) = 5

### b. Now suppose that the link cost between *x* and *y* increases to 60. Will there be a count-to-infinity problem even if poisoned reverse is used? Why or why not? If there is a count-to-infinity problem, then how many iterations are needed for the distance-vector routing to reach a stable state again? Justify your answer.

There will be a count-to-infinity problem even if poisoned reverse is used. The problem is that *y* doesn't know that *z* is using *w* to get to *x*. When *c*(*x*, *y*) jumps to 60, *y* will look for a new way to get to *x*. Poisoned reverse works correctly for *w*, but not for *z*, so *y* thinks that Dz(x) = 6, when in reality, Dz(6) = 50 (through the direct connection between *z* and *x*).

The question of how many iterations is a complex question. On the first round of updates, the following will occur:

1. Router *y* realizes the link to *x* has jumped up to 60, and searches for a new path. It knows that Dz(x) = 6, and *c*(*y*, *z*) = 3, and thus calculates that Dy(x) = 9. It then lets *w* know that Dy(x) = 9 (it makes *z* think Dy(x) = infinity, due to poisoned reverse being implemented).
2. Router *w* realizes now that Dy(x) = 9, and recalculates Dw(x) to be 10. It then lets *z* know that Dw(x) = 10 (it makes *y* think that Dw(x) = infinity, due to poisoned reverse).
3. Router *z* realizes now that Dw(x) = 10, and recalculates Dz(x) to be 11. It then lets *y* know that Dz(x) = 11 (it makes *w* think that Dz(x) = infinity, due to poisoned reverse).

During the first round, all shortest-path calculations jumped up by 5. We can then induce that for every round, all shortest-path calculations will increase by 5. This means that only on round 9 does *z* finally realize that the shortest path to *x* is via the direct connection (of cost 50).

### c. How do you modify *c*(*y*, *z*) such that there is no count-to-infinity problem at all if *c*(*y*, *x*) changes from 4 to 60?

If *c*(*y*, *z*) was set to 1, then there wouldn't be a count-to-infinity problem. Both *w* and *z* would have their shortest path to *x* through *y*. In this case, when the link between *x* and *y* jumps up to 60, it would have Dw(x) = infinity and Dz(x) = infinity, since both *w* and *z* would use poisoned reverse. Thus, *y* would have to stick with the link it was already using, and broadcast that Dy(x) = 60. Both *z* and *w* will receive this at roughly the same time, and since *w* thinks Dz(x) = 5 and *z* thinks Dw(x) = 5, they will both use each other as a new shortest path to *x*. In the process, they will both use poisoned reverse on each other, so now *w* thinks Dz(x) = infinity, and *z* thinks Dw(x) = infinity. At this point, *z* realizes that the shortest path to *x* is through the direct connection between them, and chooses that path, and then the network will stabilize itself correctly.