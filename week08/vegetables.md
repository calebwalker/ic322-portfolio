# Questions from *Kurose* Chapter 4

## R23: Visit a host that uses DHCP to obtain its IP address, network mask, default router, and IP addresss of its local DNS server. List these values.

* IP address: 10.25.153.87
* Network Mask: 255.255.192.0
* Default Router: 10.25.128.1
* Local DNS Server: 10.1.74.10

## R26: Suppose you purchase a wireless router and connect it to your cable modem. Also suppose that your ISP dynamically assigns your connected device (that is, your wireless router) one IP address. Also suppose that you have five PCs at home that use 802.11 to wirelessly connect to your wireless router. How are IP addresses assigned to the five PCs? Does the wireless router use NAT? Why or why not?

Because the ISP only allocated one IP address to the wireless router, an NAT must be used. Without an NAT, there would be no way to connect more than one device to the wireless router, because only one IP address was assigned to the router. With an NAT, the wireless router modifies incoming and outgoing datagrams to allow for an internal subnet with a mask of 10.0.0.0/24 (there are other reserved addresses for private networks, but I will be referring to the 10.0.0.0/8 one). With this, each PC can have its own IP address, and the router still only has one outward-facing IP address (the one assigned by the ISP).

## R29: What is a private network address? Should a datagram with a private network address ever be present in the larger public internet? Explain.

A private network address is the address range(s) used to denote private addresses. There are three ranges reserved for private networks:
* 10.0.0.0/8
* 172.16.0.0/12
* 192.168.0.0/16

Private networks are internal subnets that are not visible to the internet, and only exist behind NAT-enabled routers. Due to the nature of how NAT works, a private network address should never be present in the larger public internet. As soon as a datagram leaves a private network, the NAT-enabled router modifies the datagram to have a source IP address of the outward-facing interface of the router.

## P15: Consider the topology shown in Figure 4.20. Denote the three subnets with hosts (starting clockwise at 12:00) as Networks A, B, and C. Denote the subnets without hosts as Networks D, E, and F.

![Figure 4.20 Labelled Topology](../images/week08/P15.jpg)

### a. Assign network addresses to each of these six subnets, with the following constraints: All addresses must be allocated from 214.97.254.0/23; Subnet A should have enough addresses to support 250 addresses; Subnet B should have enough addresses to support 120 interfaces; Subnet C should have enough addresses to support 120 interfaces. Of course, subnets D, E, and F should each be able to support two interfaces. For each subnet, the assignment should take the form a.b.c.d/x or a.b.c.d/x - e.f.g.h/y.

For all subnets, we subtract 2 addresses from the total number of addresses, because 0.0.0.0 and 255.255.255.255 are reserved addresses.

* Subnet A: 214.97.255.0/24 - 250 addresses (4 addresses reserved for Subnet D)
* Subnet B: 214.97.254.0/25 - 122 addresses (4 addresses reserved for Subnet E)
* Subnet C: 214.97.254.128/25 - 122 addresses (4 addresses reserved for Subnet F)
* Subnet D: 214.97.255.0/30 - 2 addresses
* Subnet E: 214.97.254.0/30 - 2 addresses
* Subnet F: 214.97.254.128/30 - 2 addresses

### b. Using your answer to part (a), provide the forwarding tables (using longest prefix matching) for each of the three routers.

**R1**
|               Prefix              | Subnet |
| :-------------------------------: | :----: |
|     11010110 01100001 11111111    |   A    |
| 11010110 01100001 11111111 000000 |   D    |
| 11010110 01100001 11111110 100000 |   F    |

**R2**
|               Prefix              | Subnet |
| :-------------------------------: | :----: |
|    11010110 01100001 11111110 0   |   C    |
| 11010110 01100001 11111110 100000 |   F    |
| 11010110 01100001 11111110 000000 |   E    |

**R3**
|               Prefix              | Subnet |
| :-------------------------------: | :----: |
|    11010110 01100001 11111110 0   |   B    |
| 11010110 01100001 11111110 000000 |   E    |
| 11010110 01100001 11111111 000000 |   D    |

## P16: Use the [whois service](http://www.arin.net/whois) at the American Registry for Internet Numbers to determine the IP address blocks for three universities. Can the whois services be used to determine with certainty the geographical location of a specific IP address? Use [www.maxmind.com](www.maxmind.com) to determine the locations of the Web servers at each of these universities.

* MIT: 192.55.0.0 - 129.55.255.255
* University of Missouri: 151.101.0.0 - 151.101.255.255
* Clemson: 130.127.0.0 - 130.127.204.30

## P18: Consider the network setup in Figure 4.25. Suppose that the isp instead assigns the router the address 24.34.112.235 and that the network address of the home network is 192.168.1/24.

![Figure 4.25](../images/week08/P18.png)

### a. Assign addresses to all interfaces in the home network.

* Host 1: 192.168.1.1
* Host 1: 192.168.1.2
* Host 1: 192.168.1.3
* Internal Router Interface: 192.168.1.4

### b. Suppose each host has two ongoing TCP connections, all to port 80 at host 128.119.40.86. Provide the six corresponding entries in the NAT translation table.

|       WAN Side      |      LAN Side     |
| :-----------------: | :---------------: |
| 24.34.112.235, 5001 | 192.168.1.1, 3345 |
| 24.34.112.235, 5002 | 192.168.1.1, 3876 |
| 24.34.112.235, 5003 | 192.168.1.2, 3434 |
| 24.34.112.235, 5004 | 192.168.1.2, 3888 |
| 24.34.112.235, 5005 | 192.168.1.3, 3764 |
| 24.34.112.235, 5006 | 192.168.1.3, 3211 |

## P19: Suppose you are interested in detecting the number of hosts behind a NAT. You observe that the IP layer stamps an identification number sequentially on each IP packet. The identification number of the first IP packet generated by a host is a random number, and the identification numbers of the subsequent IP packets are sequentially assigned. Assume all IP packets generated by hosts behind the NAT are sent to the ouside world.

### a. Based on this observation, and assuming you can sniff all packets sent by the NAT to the outside, can you outline a simple technique that detects the number of unique hosts behind a NAT? Justify your answer.

Yes, you could figure out how many hosts are behind an NAT. You would have to use the identification numbers, because the source IP address is modified by the NAT-enabled router. The way to do this would be as follows:

1. For the first packet sent, record the identification number in the first index of some dynamic array.
2. For each subsequent packet, compare the identification number against all current values in the array. If the number is exactly one greater than any of the values, change that value to be the new identification number. Otherwise, append the new identification number onto the array.
3. After an arbitrary amount of time has passed, the length of the array is the number of hosts behind the NAT.

This method essentially keeps a running count of 'streams' of IP packets from individual hosts, and so the total number of these 'streams' will be equal to the number of hosts.

### b. If the identification numbers are not sequentially assigned but randomly assigned, would your technique work? Justify your answer.

With no way to determine 'streams', the technique could not function, and there would be no way to determine how many hosts are behind an NAT. The only thing you *could* determine would be how many *processes* are running behind the NAT. Each process is (effectively) given a unique port number by the NAT-enabled router when transferring to the external interface, so by counting the number of unique port numbers, one could determine the number of unique processes.