# Week 8: The Network Layer: More Data Plane

*MIDN Caleb Walker | Fall 2023*

* [Learning Objectives](learning-objectives.md)
* [Protocol Pioneer](protocol-pioneer.md)
* [Feedback for Nicholas' Portfolio](https://gitlab.usna.edu/m242628/ic322-portfolio/-/issues/15)
* [Vegetables](vegetables.md)