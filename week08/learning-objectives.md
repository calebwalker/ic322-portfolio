# Week 8 Learning Objectives

## I can explain the problem that NAT solves, and how it solves that problem.

The problem that NAT solves is the growing amount of small office and home office networks. With normal addressing, these networks would be defined as subnets; being a subnet, our office network would need a range of IP addresses, which would have to be provided by the ISP. Issues arise when these networks grow and shrink in number of connected hosts, thus needing more or less IP addresses. The ISP cannot handle such a dynamic system, and no homeowner should have to fully understand IP addressing and routing. Thus, NAT was presented as the solution to this problem.

NAT is supported by NAT-enabled routers, which perform all the necessary functions to utilize NAT. These routers essentially cover up the subnet and present to the internet as *one interface*, when in reality there are many. All packets that leave the router have the same source IP, and all packets arriving at the router have the same destination IP. The question, then, is how does the router distinguish which packets should go to which hosts on the internal subnet? This is done by modifying/analyzing port numbers in addition to IP addresses.

An NAT router keeps track of all source ports it receives from the internal subnet in addition to the IP addresses. For every source port/IP combination, the router assigns a unique external port number. When a packet from the subnet wants to reach a host on the internet, the router changes the IP address to the common one, and changes the source port to the unique external port number. Similarly, when a packet arrives, the router converts the destination port/IP to the combo that corresponds with the original external port number. This way, the router can maintain a single IP address for all internet traffic, and essentially multiplexes/demultiplexes packets by converting back and forth between the port/IP combos and their respective unique external port numbers.

## I can explain important differences between IPv4 and IPv6.

![IPv4 vs IPv6](https://www.networkacademy.io/sites/default/files/inline-images/comparing-ipv4-and-ipv6-headers.png)

Image from [NetworkAcademy.io](https://www.networkacademy.io/ccna/ipv6/ipv4-vs-ipv6)

The main and key difference between IPv4 and IPv6 is the range of possible addresses, with IPv4 only containing ~4.3 billion addresses and IPv6 containing far more than that (~340 undecillion). This was one of the main driving factors behind creating and adopting a new network layer protocol - IPv4 was quickly running out of addresses for the rapidly growing internet.

However, there are other important changes that make IPv6 favorable. Most of these changes arose from problems identified with IPv4 that could be fixed/streamlined with better implementation, and thus a lot of changes are actually removals. This is highlighted in the diagram above - there are far more fields **not kept** in IPv6 that fields that are **new** in IPv6.


An overview of the other changes made in IPv6:
* **Removing Fragmentation**: IPv6 no longer allows for fragmentation of datagrams - this functionality was deemed overly complicated (which caused more delay) and redundant (the transport layer already breaks the message up into chunks). If IPv6 receives a segment that is too large, it simply returns an ICMP "Packet Too Big" message.
* **Removing Checksum**: This decision was made for precisely the same reason as removing fragmentation: the functionality is overly complicated and redundant, as both the layer above and the layer below also perform checksumming.
* **Removing Options**: This removal makes sure that the IP header is a fixed length. The work-around involves using the `Next Header` field to point to an options field.

Most of the other changes just involved shifting and re-naming certain fields:
* `Type of Service` -> `Traffic Class`
* `Total Length` -> `Payload Length`
* `TTL` -> `Hop Limit`
* `Protocol` -> `Next Header`

The only other change is the `Flow Label` field, a new field in IPv6. This field is somewhat undetermined and nonspecific right now, but it is intended to differentiate between *flows*. Perhaps certain types of transmissions, or priorities of messages will be addressed in the `Flow Label` field, but the intent was mostly a foresight to differentiate packets with different *flows*.

## I can explain how IPv6 datagrams can be sent over networks that only support IPv4.

The current method for sending IPv6 datagrams over a link that contains one or more IPv4 routers is **tunneling**. Suppose, in the simplest example, that there are two IPv6 routers connected through an IPv4 router. The sending IPv6 router cannot send an IPv6 datagram to the IPv4 router, because while IPv6 routers are backwards-compatible, IPv4 routers are not forwards-compatible. The workaround is as such: first, the sending IPv6 router takes the entire IPv6 datagram, and encapsulates it in an IPv4 datagram. This IPv4 datagram is addressed to the receiving IPv6 router, and is then sent to the IPv4 router in the middle. This middle router receives an IPv4 datagram, and sends it along to the addressed IPv6 router. The IPv4 router is blissfully unaware that the payload of that IPv4 datagram contains a different IPv6 datagram rather than a transport layer segment. When the IPv6 router receives the IPv4 datagram, it checks the `Protocol Number` field to determine that it contains an IPv6 datagram, and unwraps the encapsulated information. It then sends the IPv6 datagram along. This method can be expanded to traverse sets of multiple IPv4 routers intervening between IPv6 routers, and is the method used on the internet currently.