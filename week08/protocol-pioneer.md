# Lab: Protocol Pioneer

## Introduction

This lab involved playing LCDR Downs' beta-version game and testing its functionality. The game is intended to teach others about network protocols without explicitly referring to them.

## Collaboration/Citations

The game is located [here](https://gitlab.com/jldowns-usna/protocol-pioneer/-/tree/main?ref_type=heads). I did not use any additional resources, nor did I get help from anyone on this lab.

## Process

I followed the lab instructions, and completed Chapter 3 and 4 of the game. This was all in accordance with the instructions outlined in the [Protocol Pioneer Lab](https://courses.cs.usna.edu/IC322/#/assignments/week07/protocol-pioneer/).

## Questions

### Q1: How did you solve the Chapters? Please copy and paste your winning strategy(s), and also explain it in English.

For the third chapter, my solution involved one main 'if-else' that differentiates whether the drone should forward the message or act on it. If the drone number in the message matches the current drone number, then the drone processes the "command" in the message and executes the corresponding function with the given value. Otherwise, it forwards the message. I used both the nature of the graph and logic optimization to encode the drone 'routing' logic in a simple 'if-else'. Because the graph is so small, I didn't need to make any routing tables or anything.

```python
msg_arr = m.text.split("\n")
drone = int(msg_arr[0][-1])
id = itn(self.id)
if(id == drone):
    command = msg_arr[1][8:]
    value = int(msg_arr[2][6:])
    if(command == "start_emit"):
        self.start_emit(value)
    elif(command == "focus"):
        self.focus(value)
    elif(command == "return_results"):
        self.return_results(value)
else:
    dir = "N"
    if id == 1 and drone != 3:
        dir = "E"
    elif id == 2 and drone == 5:
        dir = "S"
    self.send_message(m.text, dir)
```

---

For the fourth chapter, the graph got larger and more complex. Also, there were scanners in addition to drones, so two sets of logic had to be defined.

In my drone logic, I adopted an approach more similar to a routing table. I had a list for each direction (interface), and defined the lists differently for each drone based on its ID. Then, I just checked the message destination, compared it to each list, and forwarded the message to an interface if the destination was in the given list.

```python
msg_arr = m.text.split("\n")
        source = msg_arr[0][7:]
        dest = msg_arr[1][5:]
        id = int(self.id[2])
        north = []
        south = []
        east = []
        west = []
        if id <= 3:
            east = ["2.1", "2.2"]
        if id >= 5:
            north = ["2.1"]
            south = ["2.2"]
            west = ["1.1", "1.2", "1.3"]
        if id == 1:
            south = ["1.2", "1.3"]
            west = ["1.1"]
        elif id == 2:
            north = ["1.1"]
            south = ["1.3"]
            west = ["1.2"]
        elif id == 3:
            north = ["1.1", "1.2"]
            south = ["1.3"]
        elif id == 4:
            north = ["1.1", "1.2", "2.1", "2.2"]
            west = ["1.3"]
        dir = ""
        if dest in north:
            dir = "N"
        elif dest in south:
            dir = "S"
        elif dest in east:
            dir = "E"
        elif dest in west:
            dir = "W"
        self.send_message(m.text, dir)
```

In my scanner logic, I did the following in order:
1. Extracted the source, destination, command, and value
2. Executed the given command with the given value as an argument
3. Constructed a new message with the source and destination flipped, the command 'Result', and the computed value
4. Sent the message back the way it came

Because each scanner only had one interface, there wasn't much logic needed for computing which direction to send the message.

```python
msg_arr = m.text.split("\n")
        source = msg_arr[0][7:]
        dest = msg_arr[1][5:]
        command = msg_arr[2][8:]
        value = int(msg_arr[3][6:])
        if command == "Boot":
            value = self.boot(value)
        elif command == "Aim":
            value = self.aim(value)
        elif command == "Scan":
            value = self.scan(value)
        dir = "S" if source[2] == "1" else "N"
        msg = "Source:" + dest + "\nDest:" + source +"\nCommand:Result\nValue:" + str(value)
        self.send_message(msg, dir)
```

### Q2: Include a section on Beta Testing Notes.

These chapters definitely got me to think more, but since the first two chapters introduced me well to the general layout of the chapters, my struggles were less about implementing a concept and more about coming up with a concept that worked. These chapters were a fun challenge, and I don't really have any criticism to give.