# Week 13: More Link Layer

*MIDN Caleb Walker | Fall 2023*

* [Learning Objectives](learning-objectives.md)
* [Wireshark Lab](wireshark-lab.md)
* [Feedback for Owen's Portfolio](https://gitlab.com/owenpitch4d/ic322-portfolio/-/issues/24)
* [Vegetables](vegetables.md)
* [VintMania!](vint-mania.md)