# Questions from *Kurose* Chapters 6 and 7

## Chapter 6, R10: Suppose nodes A, B, and C each attach to the same broadcast LAN (through their adapters). If A sends thousands of IP datagrams to B with each encapsulating frame addressed to the MAC address of B, will C's adapter process these frames? If so, will C's adapter pass the IP datagrams in these frames to the network layer in C? How would your answers change if A sends frames with the MAC broadcast address?

Because it is a broadcast LAN, any frames transmitted onto the network will reach all nodes connected to the network, regardless of the MAC addresses in the source/destination fields of the frames. So, in this scenario, node C's adapter *will* process these frames, as it does not know whether or not the frames are addressed to C until it processes them. However, when it does process them, it will see that the frames are addressed to node B, and discard them. Thus, the adapter *will not* pass the IP datagrams in the frames up to the network layer.

## Chapter 6, R11: Why is an ARP query sent within a broadcast frame? Why is an ARP response sent within a frame with a specific destination MAC address?

When a query is sent, the querying host is *trying to figure out a MAC address*. Thus, it has no idea which MAC address to send the frame to, so for a guarantee, it *broadcasts* it. In this sense, the query will be sent to all hosts on the network, and only one will respond (because only one will match the queried IP address). This query broadcast includes the querying host's MAC address, which is important. When a response is sent, instead of being broadcast, the frame can be addressed to the querying host. This address is pulled from the broadcast query source MAC address.

## Chapter 6, P14: Consider three LANs interconnected by two routers, as shown in Figure 6.33.

![Figure 6.33](../images/week13/P14.jpg)

### a. Assign IP addresses to all of the interfaces. For subnet 1 use addresses of the form 192.168.1.xxx; for Subnet 2 use the addresses of the form 192.168.2.xxx; and for Subnet 3 use addresses of the form 192.168.3.xxx.

![Figure 6.33, part a](../images/week13/P14a.jpg)

### b. Assign MAC addresses to all of the adapters.

![Figure 6.33, part b](../images/week13/P14b.jpg)

### c. Consider sending an IP datagram from Host E to Host B. Suppose all of the ARP tables are up to date. Enumerate all the steps, as done for the single-router example in Section 6.4.1.

1. Host E decides that the IP datagram must be forwarded to the gateway router (based on the fact that Host B's IP address is not within subnet 3).
2. Host E knows the IP address of the gateway router (`192.168.3.1`) because it was given by DHCP. It indexes into its ARP table, finds the MAC address for the IP address (`00:00:00:00:00:04`), creates a frame containing the IP datagram with a source MAC address of `00:00:00:00:00:16` and a destination MAC address of `00:00:00:00:00:04`. Then, it sends the frame onto the subnet 3.
3. The switch for subnet 3 forwards the frame to the router.
4. The router receives the frame, decapsulates it, and figures out that the datagram inside is supposed to go to `192.168.1.3`. Assuming a routing algorithm has set its routing table correctly, the router uses longest prefix matching to determine the correct outbound interface, and uses its ARP table to determine the correct destination MAC address for the intended next router (`00:00:00:00:00:02`).
5. The router creates a new frame that encapsulates the same IP datagram with a source MAC address of `00:00:00:00:00:03` and a destination MAC address of `00:00:00:00:00:02`. Then, it sends the frame onto subnet 2.
6. The switch for subnet 2 forwards the frame to the other router.
7. The other router receives the frame, and decapsulates it to figure out that the datagram inside is supposed to go to `192.168.1.3`. Again assuming the routing table is correct, the router uses longest prefix matching to determine the correct outbound interface, and uses its ARP table to determine the correct destination MAC address for the intended next hop (Host B, which is `00:00:00:00:00:13`).
8. The router creates a new frame that encapsulates the same IP datagram with a source MAC address of `00:00:00:00:00:01` and a destination MAC address of `00:00:00:00:00:13`. Then, it sends the frame onto subnet 1.
9. The switch for subnet 1 forwards the frame to Host B.
10. Host B receives the frame, and decapsulates it to extract the IP datagram inside.

### d. Repeat (c), now assuming that the ARP table in the sending host is empty (and the other tables are up to date).

1. Same as above.
2. Host E knows the IP address of the gateway router because it was given by DHCP. However, it must use ARP to figure out what MAC address the IP address corresponds with. Host E sends a broadcast ARP frame asking for the MAC address of `192.168.3.1`. Importantly, this frame will also have a source MAC address of `00:00:00:00:00:16` - this allows the response to be addressed back to Host E.
3. The switch for subnet 3 will broadcast this frame to all hosts. Host F will discard this frame, since its IP address is `192.168.3.3`, not `192.168.3.1`. However, the router will find that the queried IP address is the same as its IP address (`192.168.3.1`). The router will create a response frame with a source MAC address of `00:00:00:00:00:04` and a destination MAC address of `00:00:00:00:00:16`, and then send this response to Host E.
4. The switch for subnet 3 will forward the response to Host E.
5. Host E will receive the response, and update its ARP table to include a new entry that looks something like this: `IP Address` -> `192.168.3.1`, `MAC Address` -> `00:00:00:00:00:04`, `TTL` -> `00:20:00`.
6. The steps continue as in the previous part, starting at (2).

## Chapter 6, P15: Consider Figure 6.33. Now we replace the router between subnets 1 and 2 with a switch S1, and label the router between subnets 2 and 3 as R1.

![Figure 6.33 Modified](../images/week13/P15.jpg)

### a. Consider sending an IP datagram from Host E to Host F. Will Host E ask router R1 to help forward the datagram? Why? In the Ethernet  frame containing the IP datagram, what are the source and destination IP and MAC addresses?

In the modified network topology example, subnet 3 remains unchanged. Therefore, an IP datagram from Host E to Host F will not go through R1 at all, because Host E and Host F are on the same subnet, so the packet never has to leave the subnet. The Ethernet frame (and the IP datagram within) will have source MAC and IP addresses of Host E, and destination MAC and IP addresses of Host F.

### b. Suppose E would like to send an IP datagram to B, and assume that E's ARP cache does not contain B's MAC address. Will E perform an ARP query to find B's MAC address? Why? In the Ethernet frame (containing the IP datagram destined for B) that is delivered to router R1, what are the source and destination IP and MAC addresses?

Host E will *not* perform an ARP query for Host B's MAC address, since the hosts are not on the same subnet. Host E would determine that Host B is not part of subnet 3 (based on the network mask and Host B's IP address), and would therefore determine that the datagram should go to R1, the gateway router. The Ethernet frame would therefore have a source MAC address of Host E, and a destination MAC address of R1. However, the IP datagram inside only deals with end-to-end routing, so it would have a source IP address of Host E and a destination IP address of Host B.

### c. Suppose Host A would like to send an IP datagram to Host B, and neither A's ARP cache contains B's MAC address nor does B's ARP cache contain A's MAC address. Further suppose that the switch S1's forwarding table contains entries for Host B and router R1 only. Thus, A will broadcast an ARP request message. What actions will switch S1 perform once it receives the ARP request message? Will router R1 also receive this ARP request message? If so, will R1 forward the message to Subnet 3? Once Host B receives this ARP request message, it will send back to Host A an ARP response message. But will it send an ARP query message to ask for A's MAC address? Why? What will switch S1 do once it receives an ARP response message from Host B?

Upon receiving the ARP request message, switch S1 will broadcast the message to all interfaces (excpet the one it received the message from). S1 will also update its forwarding table to now include an entry for Host A (the broadcast has Host A's source MAC address, so it can create this new entry from that).

R1 will receive this message, because when a ARP request message is broadcast, it reaches all hosts on the subnet. However, upon decapsulating the broadcast frame, R1 will determine that the IP address in the ARP request does not match its own, and so it will discard the frame. It will not forward the message to Subnet 3.

Host B won't send an ARP query message to ascertain Host A's MAC address. This is because Host B doesn't need Host A's IP address to send the ARP response message - it just uses the source MAC address of the request for the destination MAC address of the response. Host B does not need to know the IP address of the host that requested the information, so it does not reciprocate a request message.

When S1 receives the response message, the frame will have Host A's MAC address as the destination. S1 will have an entry for Host A (from the start of this process), so it will forward the response message to Host A.

## Chapter 7, R3: What are the differences between the following types of wireless channel impairments: path loss, multipath propagation, interference from other sources?

* **Path Loss:** this impairment is the natural attenuation of an electromagnetic signal as it travels through space. A signal will always disperse according to the inverse-square law; [this Wikipedia article](https://en.wikipedia.org/wiki/Inverse-square_law) has a nice diagram and description. Also, depending on the environment, the signal may be blocked by particles in the air or solid objects (like walls).
* **Multipath Propagation:** this phenomenon is caused by a signal bouncing off the ground, walls, or other objects, that each create a different path to the destination. The problem is that these paths have slightly different lengths, which means they can reach the destination slightly out-of-phase. This creates a blurring of the received signal, which is usually indecipherable.
* **Interference from other sources:** this is probably the easiest to understand. If other sources are nearby and transmitting at the same frequencies, their signals will interfere with each other and create a garbled transmission.

## Chapter 7, R4: As a mobile node gets farther and farther away from a base station, what are two actions that a base station could take to ensure that the loss probability of a transmitted frame does not increase?

* One action the base station could take is to increase its transmission power. This will allow the signal to be 'heard' from farther away, because the signal-to-noise ratio that the receiver will perceive will be larger. Because the signal-to-noise ratio and the bit-error-rate are inversely related, having the higher signal-to-noise ratio will make the bit-error-rate lower; in turn, the loss probability of a transmitted frame is decreased.
* Another action the base station could take is to use a modulation technique with a lower bit transmission rate. The textbook does not give the reason why this property arises, but does state explicitly that "*a modulation technique with a higher bit transmission rate will have a higher BER (bit-error-rate)*". So, we just understand that the modulation's transmission rate to be positively related to the bit-error-rate.

## Chapter 7, P6: In step 4 of the CSMA/CA protocol, a station that successfully transmits a frame begins the CSMA/CA protocol for a second frame at step 2, rather than at step 1. What rationale might the designers of CSMA/CA have had in mind by having such a station not transmit the second frame immediately (if the channel is sensed idle)?

A station beginning the CSMA/CA protocol at step 2 means that the station enters a backoff state (using binary exponential backoff) instead of immediately transmitting another frame. This choice is likely designed to allow for two stations to be able to transmit frames intermittently - if a station transmitted its next frame directly after the previous one, then it would necessarily transmit all pending frames in a row. Instead, forcing the station to go into backoff after transmitting ensures that any other stations with pending frames have a chance to start transmitting a frame.

## Chapter 7, P7: Suppose an 802.11b station is configured to always reserve the channel with the RTS/CTS sequence. Suppose this station suddenly wants to transmit 1,500 bytes of data, and all other stations are idle at this time. As a function of SIFS and DIFS, and ignoring propagation delay and assuming no bit errors, calculate the time required to transmit the frame and receive the acknowledgment.

With the following variables defined:
* *R* = transmission rate of station (bytes per second)
* *S* = SIFS
* *D* = DIFS

The time required to transmit the frame and receive the acknowledgment is $3S + D + 1500R$. Because the 1500 bytes can be transmitted all in a 802.11 frame, the sequence is just:

1. Send RTS
2. Respond with CTS
3. Send data
4. ACK data

A DIFS is placed before step `1`, and a SIFS is placed between each remaining step (between `1` and `2`, `2` and `3`, `3` and `4`).