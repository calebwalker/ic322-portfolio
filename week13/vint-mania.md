# Vint Cerf Lecture Questions

## 1. Vint Cerf is known as "one of the fathers of the Interent". Why is he known as this?

Vint Cerf was one of the original creators of the first Transmission Control Protocol, which ended up being one of the core foundations of the entire internet that is so prevalent today. For this reason, he is known as "one of the fathers of the Internet", simply because he pioneered one of the most integral parts of modern-day computer networking early on.

## 2. Find 3 surprising Vint Cerf facts.

* Vint Cerf is almost always seen wearing a three-piece suit; considering the computer science dressing norm, this is a surprising rarity.
* He has worked with NASA on creating an Interplanetary Internet, which theoretically will be used to have communications between planets using lasers/radio signals.
* The initial "*Eureka*" moment for TCP/IP was a rough sketch of a packet-switched network on an envelope that Vint Cerf drew during a break from a computing conference.

## 3. Develop 2 interesting and insightful questions you'd like to ask him. For each question, describe why it is interesting and insightful.

#### With regard to an Interplanetary Internet, error detection/correction seems to be a much larger issue, given the extremely long propagation delay and unreliability of transmissions in space. What types of new protocols are being worked on to resolve this issue?

I think that this question is really insightful because it can take our current understanding of computer networks and apply it to a problem that involves the frontier of a niche computer networking field. Having read up on it, there is already a protocol for it, and it uses Reed-Solomon error-correcting codes, or, ***CRC!***.

#### What do you envision future computer networking problems to be - rather, what do you think the internet will look like 30, 40, 50 years from now?

This is a lower-hanging fruit that I think still has great value. The future of computer networking is the computer networking we will be dealing with, so I think having insight from one of the most forward-thinking individuals in the field could be really useful and interesting.