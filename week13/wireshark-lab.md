# Wireshark Ethernet and ARP Lab

## Introduction

This lab gave a real-life example of how the Ethernet protocol handled frames sent over a local network, and how ARP matched IP addresses with Ethernet addresses. I got to see how an HTTP transfer looked at the Ethernet level, with frames carrying chunks of the HTTP response. I also got to see how ARP tables were set up, and what ARP messages looked like on a local network.

## Collaboration

I did not use any additional resources, nor did I get help from anyone on this lab.

## Process

I completed the "Ethernet and ARP" Wireshark lab on the [textbook's website](http://gaia.cs.umass.edu/kurose_ross/wireshark.php) (version 8.1). I didn't deviate from the instructions.

## Answers to lab questions

### Q1: What is the 48-bit Ethernet address of your computer?

My computer's 48-bit Ethernet address is `6c:0b:84:a9:b4:c8`.

### Q2: What is the 48-bit destination address in the Ethernet frame? Is this the Ethernet address of gaia.cs.umass.edu? What device has this as its Ethernet address?

The destination address for the frame is `88:3a:30:a1:90:40`. This is not the address of gaia.cs.umass.edu, because Ethernet only deals with single link hops. Since it takes more than one hop to get to gaia.cs.umass.edu, the Ethernet address is not known to me with just looking at what Wireshark captured. In fact, the device that has the destination address would be the gateway router of my computer. The gateway router is the first link that any internet traffic goes to, so the Ethernet will always designate the destination address of a frame from my computer to be the gateway router.

### Q3: What is the hexadecimal value for the two-byte Frame type field in the Ethernet frame carrying the HTTP GET request? What upper layer protocol does this correspond to?

The hexadecimal value for the two-byte Frame type field is `0x0800`, which corresponds to the IPv4 protocol.

### Q4: How many bytes from the very start of the Ethernet frame does the ASCII "G" in "GET" appear in the Ethernet frame? Do not count any preamble bits in your count, i.e., assume that the Ethernet frame begins with the Ethernet frame's destination address.

The "G" is the 55th byte (or byte 54 with zero-index counting) of the entire frame.

### Q5: What is the value of the Ethernet source address? Is this the address of your computer, or of gaia.cs.umass.edu? What device has this as its Ethernet address?

The source address for this frame is `88:3a:30:a1:90:40`. This is neither the address of my computer nor the address of gaia.cs.umass.edu. This is still the address of my gateway router.

### Q6: What is the destination address in the Ethernet frame? Is this the Ethernet address of your computer?

The destination address in the frame is `6c:0b:84:a9:b4:c8`. This is the Ethernet address of my computer, because it was the source address for the "GET" request frame(s).

### Q7: Give the hexadecimal value for the two-byte Frame type field. What upper layer protocol does this correspond to?

The hexadecimal value for the two-byte Frame type field is `0x0800`, which corresponds to the IPv4 protocol.

### Q8: How many bytes from the very start of the Ethernet frame does the ASCII “O” in “OK” appear in the Ethernet frame? Do not count any preamble bits in your count, i.e., assume that the Ethernet frame begins with the Ethernet frame's destination address.

The "O" is the 68th byte (or byte 67 with zero-index counting) of the entire frame.

### Q9: How many Ethernet frames (each containing an IP datagram, each containing a TCP segment) carry data that is part of the complete HTTP “OK 200 ...” reply message?

5 frames carry the data, because there were 5 TCP segments involved in the entire exchange.

### Q10: How many entries are stored in your ARP cache?

There is only one entry stored in my ARP cache:

```
hopper-1a-as1-v405.gw.usna.edu (10.60.37.1) at 88:3a:30:a0:9e:40 [ether] on eno1
```

### Q11: What is contained in each displayed entry of the ARP cache?

A hostname, the IP address of the host, the MAC address of the host, and the layer-2 interface that should be used to send data to the host.

### Q12: What is the hexadecimal value of the source address in the Ethernet frame containing the ARP request message sent out by your computer?

For this question, and all remaining questions, I used the given wireshark packet capture found [here](http://gaia.cs.umass.edu/wireshark-labs/wireshark-traces-8.1.zip). This is because I don't have the necessary permissions to clear the ARP cache on the lab machines.

The source address in the Ethernet frame containing the ARP request message sent out by 'my computer' (which is named `BelkinIn_75:b1:52` in the given packet capture file) is `c4:41:1e:75:b1:52`.

### Q13: What is the hexadecimal value of the destination addresses in the Ethernet frame containing the ARP request message sent out by your computer? And what device (if any) corresponds to that address (e.g. client, server, router, switch...)?

The destination address in the Ethernet frame containing the ARP request message is `ff:ff:ff:ff:ff:ff`. This does not correspond to any device, as an address of all 1's is the broadcast address, which is specified to reach all hosts on a local network.

### Q14: What is the hexadecimal value for the two-byte Ethernet Frame type field. What upper layer protocol does this correspond to?

The hexadecimal value for the two-byte Frame type field is `0x0806`, which corresponds to the ARP protocol.

### Q15: How many bytes from the very beginning of the Ethernet frame does the ARP *opcode* field begin?

The ARP opcode (which is `1` for a request) is the 21st and 22nd byte (or byte 20 and 21 with zero-index counting) of the entire frame. Although it doesn't seem efficient to me, the *opcode* field is two bytes long, despite only ever having the value of `1` or `2` (for request and reply, respectively).

### Q16: What is the value of the *opcode* field within the ARP request message sent by your computer?

The *opcode* field within the ARP request message is a `1`, because that is the value that indicates an ARP request.

### Q17: Does the ARP request message contain the IP address of the sender? If the answer is yes, what is that value?

Yes, the request message does contain the IP address of the sender. In the given packet capture, the sender's IP address is `128.119.247.66`.

### Q18: What is the IP address of the device whose corresponding Ethernet address is being requested in the ARP request message sent by your computer?

The IP address of the device whose Ethernet address is being requested is `128.119.247.1`.

### Q19: What is the value of the *opcode* field within the ARP reply message received by your computer?

The *opcode* field within the ARP request message is a `2`, because that is the value that indicates an ARP response.

### Q20: Finally, let’s look at the answer to the ARP request message! What is the Ethernet address corresponding to the IP address that was specified in the ARP request message sent by your computer (see Q18)?

The Ethernet address corresponding to the IP address that was specified in the ARP request message (`128.119.247.1`) is `00:1e:c1:7e:d9:01`.

### Q21: We’ve looked the ARP request message sent by your computer running Wireshark, and the ARP reply message sent in response. But there are other devices in this network that are also sending ARP request messages that you can find in the trace. Why are there no ARP replies in your trace that are sent in response to these other ARP request messages?

We can see other ARP requests because they are broadcast to the network, meaning they reach all hosts. However, ARP responses are addressed specifically to the host that made the request, and thus are *not* broadcast to the network. We can safely assume that the network uses a switch; therefore, the switch would only forward the response message to the interface that corresponded with the requesting host's Ethernet address. So, my computer never sees other responses, because the switch knows exactly where to send those responses.