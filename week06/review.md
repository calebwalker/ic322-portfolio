# Six Week Review Assignment

## [Week 1](https://gitlab.com/calebwalker/ic322-portfolio/-/tree/master/week01?ref_type=heads) review

### 1. Open the feedback I provided for you for Week 1. Read the feedback.

### 2. Open your Portfolio to your Week 1 Learning Objectives. Read your answers.

### 3. Open your Portfolio to your Week 1 Chapter Questions. Review your answers. Do you think you put more effort into these answers than in later weeks? Less? About the same? Why do you think that is?

By the looks of it, I put a little less effort into these answers than later weeks. By no means is the effort below standard - the answers still convey a good enough understanding of the material. I think that I was still trying to experiment with and get used to the markdown format, and wasn't familiar/acclimated to the expectation on the chapter question answers.

### 4. Open your Week 1 Knowledge Check. How many of your answers met standards?

All 3 of my answers met standards.

### 5. Review your Week 1 Lab. How have your markdown skills evolved over the past 6 weeks?

Looking at my first lab, I took heavy style inspiration from Professor Downs' portfolio. I had many different markdown tidbits that split up the text nicely, but it is not normally what I would've done. I usually tend to keep the format simplistic, focusing more on the quality of my answers. However, I am quite comfortable with the simple syntax of markdown, and if I needed to create a more complex markdown page, I could use references to figure out the correct syntax.

### 6. Which classmate's portfolio were you paired with for this assignment?

I was paired with [Forrest](https://gitlab.usna.edu/ForrestYork/midn-york-repository/-/tree/main/week1?ref_type=heads) for this assignment.

### 7. Review your classmate's Week 1 submission. What is the same about their Assignment solution as compared to yours? What is different?

Forrest's submission is about similar to mine, with some small differences. He combined the learning objectives and chapter questions into one markdown file, and put his wireshark lab into a regular file, which kind of made reading the lab difficult.

### 8. Open an issue and leave some feedback for your classmate. Try to mention at least one thing you like, and one thing you think could be improved (and some encouraging words to send them on their way down the path of improvement). 

### 9. Between your Portfolio and your classmate's, is one Portfolio "better"? Or is the answer more complicated than that?

I believe that it's hard to say if one Portfolio is better than another, mostly due to the fact that we were given creative license to make our Portfolio however we wanted. Similar to saying one artwork is better than another, I believe as long as the person learned by creating their Portfolio, then they succeeded and there's no comparison.

### 10. Complete your section's rubric for your Week 1 assignments.

Our rubric defines our grade with 100% completeness, and since my Week 1 is 100% complete, that would mean my Week 1 grade should be an A.

## [Week 2](https://gitlab.com/calebwalker/ic322-portfolio/-/tree/master/week02?ref_type=heads) review

### 1. Open your [Week 2 classmate's Portfolio](https://gitlab.usna.edu/coraskid/ic322-portfolio/-/tree/master/Week2?ref_type=heads). Did they make use of Markdown in any creative ways? Keep their Portfolio open.

While I didn't see any super creative uses, my partner definitely is comfortable with the format, and used simple Markdown in a way that made the pages very readable.

### 2. Open the feedback I provided for Week 2. Read the feedback.

### 3. Open your Week 2 Lab. If you chose one of the programming assignments, review the code. Do you still remember what each line does? Does your "code style" make you happy?

Yes, I remember what each line of my TCP server does, and my code style does make me happy.

### 4. Read you Week 2 Review Questions and Learning Goal answers. How do your Week 2 answers compare to Week 1?

By the looks of it, my Week 2 answers hold up pretty well against my Week 1 answers. I still maintained good insight and was consistent with the markdown style I used.

### 5. Go back to your classmate's Portfolio.

### 6. Which lab did they choose? If it was a programming lab, try to run their program.

### 7. Look at their Review Quesions and Learning Goal answers. Try to learn something new. Include the new thing you leaned when you leave them feedback.

### 8. Open an issue and leave feedback for your classmate. Try to mention at least one thing you like, and one thing you think could be improved. Try to say something that could cheer up someone having a bad day.

### 9. If you were to give your classmate a grade for their Week 2 submission, what would it be? (They won't see your answer. This is an exercise for you.)

I would definitely give my partner's submission an A. Their work was 100% complete, but on top of that, they chose to do the programming lab and every markdown file had nice formatting.

### 10. Look at your Week 2 Knowledge Check. Read your answers and the instructor's feedback.

### 11. Complete your section's rubric for your Week 1 assignments.

Our rubric defines our grade with 100% completeness, and since my Week 2 is 100% complete, that would mean my Week 2 grade should be an A.

## [Week 3](https://gitlab.com/calebwalker/ic322-portfolio/-/tree/master/week03?ref_type=heads) review

### 1. Open your Week 3 Portfolio. Read your Learning Goal answers and your answers to the Review Questions. Without looking at my feedback, can you remember what feedback I gave you? Do you have feedback for yourself?

I don't think there was any changes that you recommended in your feedback - I felt that I was very thorough and explanatory when answering the learning goals and chapter questions.

### 2. Review your Week 3 Knowledge Check. By this point in the class had you developed any strategies that helped you stay on top of the classwork?

I had been behind by a week because I didn't fully understand the Portfolio for the first week, so at this point I had tried to catch up. However, reading the textbook and thinking of how to articulate my answers took a lot of time, and my time after school was limited. I would basically just stay up late or use the weekend to catch up and finish the weeks for the Portfolio.

### 3. Open your [Week 3 partner's Portfolio](https://gitlab.com/PJAsjes/ic322-portfolio/-/tree/main/Week3?ref_type=heads). Which Lab did they do? If it was the DNS lab, visit their website.

I don't see a lab for Week 3, so I will assume they are still working on it.

### 4. Read your classmate's Learning Goal and Review Question answers. What is one thing that they do that you would like to do in future submissions?

One thing I noticed is the hierarchy of questions that my partner implemented with the headers in markdown. I think it helps the reader understand how the questions are arranged, and I will definitely make sure to do that in future submissions.

### 5. Open an issue and leave feedback for your classmate. Try to mention at least one your classmate did in their Portolio that you like, and one practice they could incorporate in the future.

### 6. Some people make a distinction between "learning" and "answering". Do you think there's a difference? How much of the first six weeks did you spend "learning", and how much time did you spend "answering"?

I believe that I spent way more time "answering" than "learning". To be clear, that is just how I felt and is due to the way I learn and retain information. That being said, I felt like reading the textbook was enough for me to completely meet all standards for the first six weeks. The labs helped a little bit in the actual application of the material, but overall the learning objectives and chapter questions felt very forced and repetitive.

### 7. Complete your section's rubric for your Week 3 assignments.

Our rubric defines our grade with 100% completeness, and since my Week 3 is 100% complete, that would mean my Week 3 grade should be an A.

## [Week 4](https://gitlab.com/calebwalker/ic322-portfolio/-/tree/master/week04?ref_type=heads) review

### 1. Read the feedback I provided for you for Week 4. Have there been any patterns so far in the feedback I've left for you?

Because I had a shaky upload schedule for the weeks, I didn't get any feedback on Week 4, so I can't really speak to a clear pattern. However, for the weeks that I did get feedback on, they were generally positive with only a slight modification to an answer or the way I answered things.

### 2. Open your Learning Goal answers. Have you made any mistakes that were addressed in previous weeks' feedback?

I didn't make any mistakes addressed in previous weeks' feedback because the feedback I did receive on previous weeks didn't have any general improvements to include in future weeks.

### 3. Open your [Week 4 partner's Portfolio](https://gitlab.com/mcwieland/ic322-portfolio/-/tree/master/Week4?ref_type=heads). Read their Learning Goal Answers. Did they make any mistakes?

The only mistake I saw was really a slight typo when describing sequence numbers - "Sequence numbers: These are used to number every byte of a segment in order to ensure that they can be reassembled in order when they reach the destination host." This can be misleading, since it can be assumed from that quote that TCP reassembles bytes. Making it more clear that TCP uses sequence numbers to reassemble *segments* rather than individual bytes.

### 4. Open an issue and leave feedback for your classmate.

### 5. Read your Review Question Answers.

### 6. Review your Knowledge Check.

### 7. Review your lab report.

### 8. What was the most important concept we learned during Week 4?

I believe that the explanation of how TCP makes sure packets arrive in order and without an errors was the most important part. Understanding sequence numbers, timers, and the sliding window were very helpful in my opinion.

### 9. Complete your section's rubric for your Week 1 assignments.

Our rubric defines our grade with 100% completeness, and since my Week 4 is 100% complete, that would mean my Week 4 grade should be an A.

## [Week 5](https://gitlab.com/calebwalker/ic322-portfolio/-/tree/master/week05?ref_type=heads) review

### 1. Open your Week 5 assignment submission in your Portfolio.

### 2. Read all your written work. (We did not have a knowledge check for this week).

### 3. Open your Week 5 partner's portfolio. Read their written work.

### 4. What about your solutions do you like better than your partner's?

One thing I would say I like more about my solution is generally the fact that I separated the learning goals, chapter questions, and lab questions into separate markdown files, whereas my partner has everything in one file (not to mention the file is only labeled as 'Week 5 Learning Objectives).

### 5. What about your partner's solutions do you like better than yours?



### 6. Open an issue and leave feedback for your classmate.

### 7. At the beginning of the semester I said I had 3 goals for this class in order of importance: 1. For you to learn Computer Networks, 2. For you to enjoy the class, and 3. For you to "learn how to learn". How successful have I been so far?

As for learning Computer Networks, that goal has been completed many times over. In fact, specifically for me, that goal being hammered home caused the other goals to not be successfully fulfilled. I feel learning with the Portfolio has made me dislike the class a little more than I would have just learning conventionally, as that is how I learn best. Just by reading the textbook and thinking about the learning objectives, I feel genuinely ready to be tested on the subject. The chapter questions and actually writing out my answers to the learning objectives feel obligatory and unnecessary.

### 8. Complete your section's rubric for your Week 1 assignments.

Our rubric defines our grade with 100% completeness, and since my Week 5 is 100% complete, that would mean my Week 5 grade should be an A.

## 6 Week Self-evaluation

### 1. Is there anything that you'd like to focus on this next 6 weeks? Any goals?

I would like to focus on staying ahead and having my weeks' work pushed to the repository in time for feedback. I think now that I have the hang of the class and semester, I can achieve that goal.

### 2. Will you do anything differently in order to achieve those goals?

I will be ahead on the work that I do and be diligent and consistent with reading the textbook and making the week for the portfolio.

### 3. What has been the most successful strategy or practice for you so far in this class?

There isn't really a strategy I have - for this class, it's more about putting the work in. Thus far, classes have been graded mostly based on evaluations (quizzes, tests, exams, etc.), and so you only need to put in as much work as you need to know the material. But this class is more of a time sink because it requires more than just knowing and understanding the material.

### 4. What has been the least successful?

The least successful thing has definitely been getting behind on assignments, because then you don't get feedback and also have to make up more work.

## Grading

### 1. Use your class rubric to propose a 6 week grade for your portfolio.

As my portfolio was 100% complete, and the rubric bases grading only on completion, I propose an A for my portfolio.

### 2. Is there any other information you'd like to include?

Only that while I understand the point of having the class revolve around a portfolio, I personally don't learn most effectively like that. Additionally, I tend to be a perfectionist to my own detriment, so the weeks (I assume) take me longer than the average student because I put a lot of time into the answers.