# MIDN Caleb Walker IC322 Portfolio

I'm maintaining this Portfolio for IC322: Computer Networks. If you're visiting, welcome!

* [Week 1](/week01)
* [Week 2](/week02)
* [Week 3](/week03)
* [Week 4](/week04)
* [Week 5](/week05)
* [Week 6](/week06)
* [Week 7](/week07)
* [Week 8](/week08)
* [Week 9](/week09)
* [Week 10](/week10)
* [Week 11](/week11)
* [Week 12](/week12)
* [Week 13](/week13)
