# Twelve Week Review Assignment

## [Week 7](https://gitlab.com/calebwalker/ic322-portfolio/-/tree/master/week07?ref_type=heads) review

### 1. Collect your Week 7 materials: Instructor feedback, Learning Objective answers, Review Questions and answers, Lab writeup, and Week 7 Knowledge Check.

### 2. Read these materials.

### 3. Did any of your feedback receive a "No answers found; unable to give feedback."? If so, create feedback below for each of the sections that were missing. (Learning Objectives, Review Questions, and/or Lab). Make sure your feedback is thorough and addresses correctness, completeness, and formatting for every item in every section that was missing. If you didn't leave feedback for a partner that week, go ahead and do it now (as well as leaving feedback below).

Yes - I did not have Week 7 complete when feedback was given, and so none of my sections have feedback.

* **Learning Objectives:** I think my Learning Objectives are pretty good - my DHCP could indicate how the steps come together to assign IP addresses, but overall, I feel like I did a good job of explaining everything fully and clearly.
* **Review Questions:** My review questions are really good this week. I spent a lot of time making the routing tables in Markdown, and I'm also confident that I understood longest-prefix addressing well enough to get the IP ranges correct in the later problems. Also, on question P4 specifically, I feel like a lot of people missed the fact that you can schedule the packets however you want. I saw a few peoples' answers, and they all assumed that HOL blocking was an issue (even though the question directly mentions that it doesn't).
* **Lab:** I did two labs this week, and I don't have much to say about them. They are both programming labs, they both work correctly, and the lab writeups follow the guidelines.

### 4. Are your Review Question answers correct?

Yes, they are correct. I gave a lot of thought to these questions, especially the ones involving routing tables and longest-prefix matching.

### 5. Open your Reflection Week [Partner's Portfolio for Week 7](https://gitlab.com/sideoffryes/ic322-portfolio/-/tree/master/week07?ref_type=heads). Read their entries.

### 6. Read their Learning Objectives. Would their explanations make sense to someone who has not taken this course?

Yes - the answers are actually extremely well explained. They are in very simplistic terms, and yet are completely correct. It was Einstein who said "If you can't explain it simply, you don't understand it well enough.", and Henry certainly understands the Learning Objectives.

### 7. Read their Review Question answers. Did they answer the same as you? What are the differences? If there are differences, are one of you right/wrong, or are you both right (or both wrong)?

Since Henry used the other textbook for the Review Questions, some of the questions weren't the same - however, some of them were. For the ones that were the same, most of his answers were the same as mine. The only differences were for the following questions:
* **R21:** Our answers are close, in the fact that we both indicated that routers have more than just one IP address. However, a router does not always just have one WAN interface and one LAN interface, as Henry indicated; rather, it could have multiple interfaces, each going to a separate subnet. There are many examples in the textbook which show such a scenario.
* **P4:** I saw other people get this question wrong too: most people did not scrutinize the question, but I'm a nerd like that and read it carefully. The question specifies two things:
    * For finding the minimal number of time slots, one can assume "any input queue scheduling order you want (i.e., it need not have HOL blocking)?" However, Henry's answer, along with many others', took HOL blocking as a given when determining the minimal number of time slots.
    * For finding the maximum number of slots needed, the question specifies that "a non-empty input queue is never idle". This means that you can have HOL blocking, but you can't just arbitrarily have input queues wait while other input queues process packets.
* **P8:** Our tables aren't exactly the same, but they are close. It could have just been a typo/missing one 0 or 1, so I can't really say who's exactly correct.
* **P9:** Henry did more work than necessary - the question specified that the network only used 8-bit host addresses, but he provided ranges of 32-bit addresses.

### 8. Open an issue and leave some [feedback](https://gitlab.com/sideoffryes/ic322-portfolio/-/issues/18) for your classmate. Try to mention at least one thing you like, and one thing you think could be improved. Make your feedback actionable.

### 9. Complete your section's rubric for your Week 7 assignments.

Our rubric defines our grade with 100% completeness, and since my Week 7 is 100% complete, that would mean my Week 7 grade should be an A.

## [Week 8](https://gitlab.com/calebwalker/ic322-portfolio/-/tree/master/week08?ref_type=heads) review

### 1. Collect your materials: Instructor feedback, Learning Objective answers, Review Questions and answers, Lab writeup, Knowledge Check, and Peer Feedback (left for you by a classmate).

### 2. Read these materials.

### 3. Was the instructor unable to leave feedback for any item or section because it was missing when it was due? If so, create feedback below for each of the sections that were missing. (Learning Objectives, Review Questions, and/or Lab). Make sure your feedback is thorough and addresses correctness, completeness, and formatting for every item in every section that was missing. If you didn't leave feedback for a partner that week, go ahead and do it now (and then also leave feedback below for *this* exercise).

### 4. Are your Review Question answers correct?

Mostly. P16, the question about university IP address blocks, was not working well for me. I ended up just using dig to figure out IP addresses, and even then, the geographical location part did not work.

### 5. Visit the [Portfolio Exemplar page](https://ic322.com/#/references/portfolio-exemplars). Read one or two of the Week 8 entries.

### 6. Could your Learning Objective answers use some work? Do they meet standards?

As my Week 8 portfolio *is* one of the exemplars, I would assume that the Learning Objective answers meet standards.

### 7. Open your Reflection Week [Partner's Portfolio for Week 8](https://gitlab.com/raintree06/ic322-portfolio/-/tree/master/Week08?ref_type=heads).

### 8. Read your partner's Learning Objectives, Review Questions, and Lab writeup.

### 9. Read their Review Question answers. Did they answer in complete sentences? Do their answers make sense to you? Are their answers correct?

Sydney answers her questions in mostly bullet points, but it still remains mostly easy to understand - a lot of the questions have multiple parts, and she addresses each part of the question in a separate bullet point. However, for P15 part a, there are just a lot of bullet points in a row, and it is kind of hard to follow where things are coming from. This is kind of exacerbated by the fact that I don't think her answers are right - it looks like she mistakenly though the subnet mask went backwards compared to the way it actually works.

### 10. Open an issue and leave [feedback](https://gitlab.com/raintree06/ic322-portfolio/-/issues/18).

### 9. Complete your section's rubric for your Week 8 assignments.

Our rubric defines our grade with 100% completeness, and since my Week 8 is 100% complete, that would mean my Week 8 grade should be an A.

## [Week 9](https://gitlab.com/calebwalker/ic322-portfolio/-/tree/master/week09?ref_type=heads) review

### 1. Collect your materials: Instructor feedback, Learning Objective answers, Review Questions and answers, Lab writeup, Knowledge Check, and Peer Feedback (left for you by a classmate).

### 2. Read these materials.

### 3. Same as before, create feedback below for each of the sections that were missing when they were due and leave feedback for you Week 9 partner if you didn't leave feedback when it was assigned.

The only part that wasn't given feedback was my lab, since I was still working on protocol pioneer Chapter 5 (it was pretty difficult). I did finally get it to work, and the lab writeup was done according to the guidelines. The problem with the protocol pioneer labs is that I have to do them on the lab machines in Hopper, and there's not many nights I can dedicate to walking all the way over to work on the chapters.

### 4. Are your Review Question answers correct?

Yes - I also spent a lot of time thinking about and working through these questions. Specifically, when looking at other people's answers for P11, I don't think many people understood poisoned reverse well enough to answer the question. The question involves a very complex graph with respect to working out poisoned reverse, and certain intricacies of the technique were hard to catch in some parts.

### 5. If a question is incorrect (and you suspect or know it's incorrect), can it be complete?

I believe that an incorrect question that the author *knows* or *thinks* is incorrect could be considered incomplete. Willingly giving up on understanding material is in direct conflict with the goal of learning and education, so I would consider it incomplete.

### 6. Visit the [Portfolio Exemplar page](https://ic322.com/#/references/portfolio-exemplars). Do you think any of the Week 9 entries have elements you could incorporate into your future Portfolio entries?

Adley's tables for Djikstra's algorithm incorporate formatting within the cells. I had not considered using code snippets to indicate the end of a row. Also, using the actual infinity symbol along with checkmarks after the final path determination for a given column is a nice touch that I will keep in mind.

### 7. Do your Learning Goals meet standards?

I think my Learning Goals meet standards - they are a bit lacking this week in creative formatting, but the information is detailed and answers each part of each Learning Objective.

### 8. Open your Reflection Week [Partner's Portfolio for Week 9](https://gitlab.usna.edu/coraskid/ic322-portfolio/-/tree/master/Week9).

### 9. Read your partner's Learning Objectives, Review Questions, and Lab writeup.

### 10. Read their Review Question answers. Did they answer in complete sentences? Do their answers make sense to you? Are their answers correct?

Cora's Review Question answers are in complete sentences, and they make a lot of sense, especially with the amount of graphics she includes. As far as I can tell, most of her answers are correct. The only one that seems to be incorrect is P11, but as I mentioned before, the problem is very complicated and has many intricate details that could very well leave me in the wrong and Cora in the right.

### 11. Open an issue and leave [feedback](https://gitlab.usna.edu/coraskid/ic322-portfolio/-/issues/22).

### 12. Complete your section's rubric for your Week 8 assignments.

Our rubric defines our grade with 100% completeness, and since my Week 9 is 100% complete, that would mean my Week 9 grade should be an A.

## [Week 10](https://gitlab.com/calebwalker/ic322-portfolio/-/tree/master/week10?ref_type=heads) review

### 1. Collect your materials: Instructor feedback, Learning Objective answers, Review Questions and answers, Lab writeup, Knowledge Check, and Peer Feedback (left for you by a classmate).

### 2. Read these materials.

### 3. Create feedback below for each of the sections that were missing when they were due (and leave feedback for your Week 10 partner if you failed to do so last week).

### 4. Have there been any patterns so far in the feedback I've left for you?

The only real pattern has been "no notes" on the weeks that I actually have submitted on time. Sometimes there would be occasional notes to expand on or explain more of a Review Question, but otherwise the feedback has been minimal (which is a good thing).

### 5. Are your Review Question answers correct?

They are mostly correct, with my answer to P20 as somewhat of a caveat. I did answer the question, but only to the best of my abilities. I genuinely could not think of a way that BGP would allow such an implementation, but the leading tone of the question combined with the real life implication indicated to me that there probably was a solution with BGP.

### 6. Have you developed a pattern in your Learning Goals?

My pattern for Learning Goals tends to be breaking up the question into parts, and explaining my answers in a very technical and detailed fashion. My Learning Goals tend to be less graphic, and more rigorous and admittedly boring.

### 7. Open your Reflection Week [Partner's Portfolio for Week 10](https://gitlab.usna.edu/coraskid/ic322-portfolio/-/tree/master/Week10?ref_type=heads).

### 8. Read your partner's Learning Objectives, Review Questions, and Lab writeup.

### 9. Read their Review Question answers. Did they answer in complete sentences? Do their answers make sense to you? Are their answers correct?

Cora's answers look good, and they are just as good as Week 9. Her answers give good descriptions of the concepts needed to sufficiently answer the questions, and the answers are complete sentences.

### 10. Open an issue and leave [feedback](https://gitlab.usna.edu/coraskid/ic322-portfolio/-/issues/23).

### 11. Complete your section's rubric for your Week 10 assignments.

Our rubric defines our grade with 100% completeness, and since my Week 10 is 100% complete, that would mean my Week 10 grade should be an A.

## 6 Week Self-evaluation

>Open your Six Week Portfolio Review. Read your answers (if you wrote your answers on paper and turned them in, try to answer the questions from memory).

### 1. What did you say you were going to focus on this past 6 weeks? Were you successful (when you said you were going to "do something differently", did you actually change your behavior)?

Last 6 weeks, I mentioned that I would like to focus on staying ahead and having my work pushed to the repo on time. I realistically did not achieve this goal, as I tended to still complete the weeks a little late - especially during the first week or two.

### 2. What has been the most successful strategy or practice for you so far in this class?

For me, reading the textbook has been the most successful strategy so far. I really learn a lot from it, and after reading the sections of the chapter for the week, I can answer the learning goals easily.

### 3. What has been the least successful?

The least successful habit thus far is still submitting the weeks late. Even though it didn't matter as much this marking period since the feedback wasn't weekly either, it remains an unsuccessful habit that I can hopefully overcome in the next 6 weeks.

### 4. Should you adjust your strategy going forward?

I think spacing out the workload over the week could help. I tend to do the entire week of the Portfolio in one night, which helps with connecting the ideas together, but ultimately has led to procrastination. I think spacing out the textbook reading, Learning Objectives, Review Questions, and Lab(s) would ensure I can reasonably get the week done on time.

### 5. Did you play Protocol Pioneer for any of your labs? If so, do you have any feedback?

I did play Protocol Pioneer for all of the labs that it was provided for. I think overall, it is a fun challenge and a good way to learn about why protocols are necessary and how protocols are defined.

## Grading

### 1. Use your class rubric to propose a 6 week grade for your portfolio (circle one, then provide you supporting argument/data).
**A**     B     C     D     F

Because the work is 100% complete for all weeks in my Portfolio, I think I deserve an A, according to our class rubric. Not only that, but I did 2 labs for Week 7, which should count towards the 'ambition' extra credit.

### 2. Is there any other information you'd like to include?

There's not really any new information I need to include that isn't already in my 6 week review.