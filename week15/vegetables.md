# Questions from *Kurose* Chapter 8

## R20: In the TLS record, there is a field for TLS sequence numbers. True or false?

False, there is no field for a sequence number in the TLS record.

## R21: What is the purpose of the random nonces in the TLS handshake?

These nonces are used to prevent against a *playback attack*. Without any nonces, if an eavesdropper just listened to the messages a client sent during a handshake, and then sent a copy of those messages, the eavesdropper could successfully pose as the client. It wouldn't need to know any of the encryption techniques at all, and thus could sort of open up an old communication with the server.

Nonces allow for the communication to be autheticated as a 'live' communication. A nonce is a random number that a protocol will only ever use once, which can really prove that the communication is not a replay of an old one. In a simplified explanation, the server sends the client a nonce. The client sends back an encrypted nonce, and the server decrypts it to authenticate the client. Then, if the sequence of handshake messages is repeated later, the server will realize that the other party sent an already-used nonce, and know that it is not communicating with a client, but rather an attacker that is playing back handshake messages.

## R22: Suppose a TLS session employs a block cipher with CBC. True or false: the server sends to the client the IV in the clear.

False - if the session employs a block cipher with CBC, then the IV is part of the MS that is shared between the client and server. This MS is derived from the PMS, which the client sends to the server. Since the PMS is encrypted using the server's public key, the IV is *not* sent in the clear.

## R23: Suppose Bob initiates a TCP connection to Trudy who is pretending to be Alice. During the handshake, Trudy sends Bob Alice's certificate. In what step of the TLS handshake algorithm will Bob discover that he is not communicating with Alice?

During step 3, the client verifies the certificate, extracts the server's public key, and encrypts a PMS and sends it to the server. The problem with this step is if Trudy sends Bob Alice's certificate, then Bob will not see any problems with the certificate. Therefore, he will finish step 3, and send Trudy a PMS encrypted with Alice's public key. The problem arises here though - Trudy does not know Alice's private key, and thus will not be able to decrypt the PMS that Bob sent. Upon any further communication, Bob will realize that Trudy wasn't able to get the same MS, since they will be using different keys. Using different keys will mean that messages received will be complete gibberish.

## P9: In this problem, we explore the Diffie-Hellman (DH) public-key encryption algorithm, which allows two entities to agree on a shared key.

#### The DH algorithm makes use of a large prime number *p* and another large number *g* less than *p*. Both *p* and *g* are made public (so that an attacker would know them). In DH, Alice and Bob each independently choose secret keys, *S*<sub>*A*</sub> and *S*<sub>*B*</sub>, respectively. Alice then computes her public key, *T*<sub>*A*</sub>, by raising *g* to *S*<sub>*A*</sub> and then taking mod *p*. Bob similarly computes his own public key *T*<sub>*B*</sub> by raising *g* to *S*<sub>*B*</sub> and then taking mod *p*. Alice and Bob then exchange their public keys over the Internet. Alice then calculates the shared secret key *S* by raising *T*<sub>*B*</sub> to *S*<sub>*A*</sub> and then taking mod *p*. Similarly, Bob calculates the shared key *S'* by raising *T*<sub>*A*</sub> to *S*<sub>*B*</sub> and then taking mod *p*.

#### a. Prove that, in general, Alice and Bob obtain the same symmetric key; that is, prove *S* = *S'*.

The important piece of information in proving this algorithm is a property of modular arithmetic: (*a* mod *n*)<sup>*d*</sup> mod *n* = *a*<sup>*d*</sup> mod *n*.

The algorithm starts by Alice and Bob each computing their public keys using their private keys (denoted *S*<sub>*A*</sub> and *S*<sub>*B*</sub>): *g*<sup>*S*<sub>*A*</sub></sup> mod *p* and *g*<sup>*S*<sub>*B*</sub></sup> mod *p* for Alice and Bob, respectively.

Then, they exchange their public keys, and each compute the shared secret. Alice computes the following: (*g*<sup>*S*<sub>*B*</sub></sup> mod *p*)<sup>*S*<sub>*A*</sub></sup> mod *p*. Bob computes the following: (*g*<sup>*S*<sub>*A*</sub></sup> mod *p*)<sup>*S*<sub>*B*</sub></sup> mod *p*.

With this, we can see that the property of modular arithmetic can be applied, and both computations now become *g*<sup>*S*<sub>*A*</sub>*S*<sub>*B*</sub></sup> mod *p*. This proves that for any given variables, Alice and Bob will obtain the same symmetric key.

#### b. With *p* = 11 and *g* = 2, suppose Alice and Bob choose private keys *S*<sub>*A*</sub> = 5 and *S*<sub>*B*</sub> = 12, respectively. Calculate Alice's and Bob's public keys, *T*<sub>*A*</sub> and *T*<sub>*B*</sub>. Show all work.

* Alice: 2<sup>5</sup> mod 11 = 10.
* Bob: 2<sup>12</sup> mod 11 = 4.

#### c. Following up on part (b), now calculate *S* as the shared symmetric key. Show all work.

* Alice: 4<sup>5</sup> mod 11 = 1.
* Bob: 10<sup>12</sup> mod 11 = 1.

#### d. Provide a timing diagram that shows how Diffie-Hellman can be attacked by a man-in-the-middle. The timing diagram should have three vertical lines, one for Alice, one for Bob, and one for the attacker Trudy.

![Timing Diagram](../images/week15/P9_d_.jpg)

## P14: The OSPF routing protocol uses a MAC rather than digital signatures to provide message integrity. Why do you think a MAC was chosen over digital signatures?

OSPF deals with a network of routers - that is, message integrity is only important in terms of the group. So long as a message is determined to be from a router in the network, it can be authenticated. Thus, a digital signature exceeds the needs of the authentication - there is no need to prove that a message came from a specific router. So, by using a MAC and having all the routers know a shared key for the hash function, OSPF can operate on a MAC because it only provides the guarantee that a message came from an entity that has the matching key.

## P23: Consider the example in Figure 8.28.

![Figure 8.28 Example](../images/week15/P23.png)

#### Suppose Trudy is a woman-in-the-middle, who can insert datagrams into the stream of datagrams going from R1 and R2. As part of a replay attack, Trudy sends a duplicate copy of one of the datagrams sent from R1 to R2. Will R2 decrypt the duplicate datagram and forward it into the branch-office network? If not, describe in detail how R2 detects the duplicate datagram.

R2 will not forward the datagram into the branch-office network, because it will detect the duplicate datagram. IPsec includes within it a field called the `ESP header`, which contains two fields: `SPI` and `Seq #`. For this scenario, the `Seq #` field will be the answer to the question - when Trudy sends the duplicate datagram, that datagram will have the same sequence number as a previous datagram. Thus, when R2 decrypts the duplicate datagram, it will determine that the sequence number does not fall in line with the expected datagram stream. It will discard this datagram before it ever reaches the branch-office network.