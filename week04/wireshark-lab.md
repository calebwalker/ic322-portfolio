# Wireshark TCP Lab

## Introduction

In this lab, I explored TCP segments sent from my computer to gaia.cs.umass.edu with Wireshark, and learned how TCP works by looking at the segments, sequence numbers, ACKs, and window sizes.

## Collaboration

I did not use any additional resources, nor did I get help from anyone on this lab.

## Process

I completed the "TCP" Wireshark lab on the [textbook's website](http://gaia.cs.umass.edu/kurose_ross/wireshark.php) (version 8.1). I didn't deviate from the instructions.

## Questions

### Q1: Wht is the IP address and TCP port number used by the client computer (source) that is transferring the alice.txt file to gaia.cs.umass.edu? To answer this question, it's probably easiest to select an HTTP message and explore the details of the TCP packet used to carry this HTTP message, using the "details of the selected packet header window".

The IP address of the source computer (my computer) is 10.25.153.87, and the source port number is 50481 (port number that my google chrome was using at the time).

### Q2: What is the IP address of gaia.cs.umass.edu? On what port number is it sending and receiving TCP segments for this connection?

The IP address of gaia.cs.umass.edu is 128.119.245.12. The port number it is using for TCP connections is port 80 (which is the port reserved for HTTP traffic).

### Q3: What is the *sequence number* of the TCP SYN segment that is used to initiate the TCP connection between the client computer and gaia.cs.umass.edu? What is it in this TCP segment that identifies the segment as a SYN segment? Will the TCP receiver in this session be able to use Selective Acknowledgments?

The (raw) sequence number of the TCP SYN segment for the TCP connection is 3087000591.

The segment is identified as a SYN segment based on the fact that the 'SYN' flag is set in the flag field.

The receiver should be able to use Selective Acknowledgments in the session, because when looking at the 'Options' part of the Wireshark packet view, there is a 'TCP Option - SACK permitted', which presumably means that Selective Acknowledgments are permitted.

### Q4: What is the *sequence number* of the SYNACK segment sent by gaia.cs.umass.edu to the client computer in reply to the SYN? What is it in the segment that identifies the segment as a SYNACK segment? What is the value of the Acknowledgment field in the SYNACK segment? How did gaia.cs.umass.edu determine that value?

The (raw) sequence number of the SYNACK segment for the TCP connection is 164572875.

The segment is identified as a SYNACK segment based on the fact that both the SYN and ACK flags are set in the flag field.

The (raw) Acknowledgment number in the SYNACK segment is 3087000592.

This value was determined by looking at the sequence number of the SYN segment, and adding 1 to it. Since the ACK number should be the next expected byte, and the SYN segment had no data in it, the next expected byte should be the very next sequence number (hence adding 1).

### Q5: What is the sequence number of the TCP segment containing the header of the HTTP POST command? Note that in order to find the POST message header, you'll need to dig into the packet content field at the bottom of the Wireshark window, *looking for a segment with the ASCII text "POST" within its DATA field*. How many bytes of data are contained in the payload (data) field of this TCP segment? Did all of the data in the transferred file alice.txt fit into this single segment?

The (raw) sequence number of the TCP segment containing the header of the HTTP POST command is 3087000592. This means that the packet containing the header of the HTTP POST command was the one directly after the final 3-way handshake packet.

According to the Wireshark packet view, there are 707 bytes in the TCP payload.

Not all of the data in the transferred file fits into the single packet. The packet could only store 707 bytes, but the file size is 152,136 bytes. This shows that TCP broke the transfer into many smaller transfers of packets with only ~700 bytes.

### Q6: Consider the TCP segment containing the HTTP "POST" as the first segment in the data transfer part of the TCP connection.

#### At what time was the first segment in the data-transfer part of the TCP connection sent?

The first segment was sent at (using the 'Time' column of the packet listing in Wireshark) 0.225092 seconds.

#### At what time was the ACK for this first data-containing segment received?

The ACK for this segment was sent at 0.252500 seconds.

#### What is the RTT for this first data-containing segment?

The RTT of the first segment is {Time when ACK was received} - {Time when packet was sent}, which in this case is 0.252500 - 0.225092 = 0.027408 seconds.

#### What is the RTT value of the second data-carrying TCP segment and its ACK?

The RTT of the second segment is {Time when ACK was received} - {Time when packet was sent}, which in this case is 0.253670 - 0.225164 = 0.028506 seconds.

#### What is the `EstimatedRTT` value after the ACK for the second data-carrying segment is received? Assume that in making this calculation after the received ACK for the second segment, that the initial value of `EstimatedRTT` is equal to the measured RTT for the first segment, and then is computed using the `EstimatedRTT` equation on page 242, and a value of  = 0.125.

The equation for `EstimatedRTT` given on page 242 is: `EstimatedRTT = (1 - ) * (Initial)EstimatedRTT +  * SampleRTT`.

In our case, the `(Initial)EstimatedRTT` is 0.027408 seconds, and the `SampleRTT` is 0.028506 seconds. Using a  of 0.125, we get that the new Estimated RTT is 0.02754525 seconds.

### Q7: What is the length (header plus payload) of each of the first four data-carrying TCP segments?

* First segment: 727 bytes (20 byte header with 707 byte payload)
* Second segment: 1220 bytes (20 byte header with 1200 byte payload)
* Third segment: 1220 bytes (20 byte header with 1200 byte payload)
* Fourth segment: 1220 bytes (20 byte header with 1200 byte payload)

### Q8: What is the minimum amount of available buffer space advertised to the client by gaia.cs.umass.edu among these first four data-carrying TCP segments? Does the lack of receiver buffer space ever throttle the sender for these first four data-carrying segments?

The advertised available buffer space is the same for each of the first four segments: 515 (which is 131840 bytes, since the window size must be multiplied by the 'Window size scaling factor' of 256).

Because this buffer stays the same for the first four packets, it is safe to assume that there is no lack of receiver buffer space that would throttle the sender.

### Q9: Are there any retransmitted segments in the trace file? What did you check for (in the trace) in order to answer this question?

No, there are no retransmitted segments in the trace file. I looked at all TCP segments that had a destination of 128.119.245.12 (IP address of gaia.cs.umass.edu), and there were no sequence numbers that repeated, meaning the sender (my computer) only transmitted each segment once. Also, there were no duplicate ACKs from 128.119.245.12, which means that it received all segments correctly.

### Q10: How much data does the receiver typically acknowledge in an ACK among the first ten data-carrying segments sent from the client to gaia.cs.umass.edu? Can you identify cases where the receiver is ACKing every other received segment among these first ten data-carrying segments?

The receiver ACK'd the first data-carrying segment, and then cumulatively ACK'd the next 5 segments, and then cumulatively ACK'd the next 4 segments.

In this case, there isn't really a place where the receiver ACK'd every other segment; rather, it either individually ACK'd or cumulatively ACK'd 4+ segments at a time.

### Q11: What is the throughput (bytes transferred per unit time) for the TCP connection? Explain how you calculated this value.

Since we already have an RTT for a data-carrying segment, all we need to know is how much data the segment was carrying, and then we can divide the amount of data by the RTT and that will be our throughput. The second data-carrying packet, which contained 1220 bytes, had an RTT of 0.028506 seconds. So, 1220 bytes / 0.028506 seconds = 42798 bytes per second.

### Q12: Use the *Time-Sequence-Graph(Stevens)* plotting tool to view the sequence number versus time plot of segments being sent from the client to the gaia.cs.umass.edu server. Consider the "fleets" of packets sent around *t* = 0.025, *t* = 0.053, *t* = 0.082, *t* = 0.1. Comment on whether this looks as if TCP is in its slow start phase, congestion avoidance phase or some other phase.

During the first couple of "fleets", TCP appears to be in slow start, because the number of packets being sent at a time is increasing exponentially, not to mention that TCP starts in the slow start phase by default. However, towards the 0.1 second mark, the packet amount stops growing along the exponential path - this would indicate that TCP may have switched to the congestion control phase, or perhaps even fast recovery if duplicate ACKs were involved.

### Q13: These "fleets" of segments appear to have some periodicity. What can you say about the period?

The period appears to be around 0.025 seconds between the fleets of segments. This would indicate that the RTT is also about 0.025 seconds, due to the way TCP throttles its connections by only allowing a certain number of segments to be sent at a time. The sender sends a bunch of packets, and then has to wait for ACKs to come back in order to send more segments. The time in between the fleets reflects that waiting time, during which the packet is travelling to the receiver and the corresponding ACK is travelling back to the sender.