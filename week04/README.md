# Week 4: The Transport Layer: TCP and UDP

*MIDN Caleb Walker | Fall 2023*

* [Learning Objectives](learning-objectives.md)
* [Wireshark Lab](wireshark-lab.md)
* [Vegetables](vegetables.md)