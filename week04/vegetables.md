# Questions from *Kurose* Chapter 3

## R5: Why is it that voice and video traffic is often sent over TCP rather than UDP in today's internet?

Although voice and video traffic can be time-sensitive when generated with call or video call over the internet, TCP is still preferred because of the congestion control. Voice and video traffic, regardless of whether it is download or streaming, contains large amounts of data. If there are multiple hosts using streaming or downloading video/audio files, then inevitably the network will be congested. TCP would use congestion control to limit the amount of data being sent over the network to ensure that the network doesn't completely congest itself, but UDP would not, and the network would be very congested, inhibiting the flow of data.

## R7: Suppose a process in Host C has a UDP socket with port number 6789. Suppose both Host A and Host B each send a UDP segment to Host C with destination port number 6789. Will both of these segments be directed to the same socket at Host C? If so, how will the process at Host C know that these two segments originated from two different hosts?

The segments will be directed to the same socket. The process at Host C will not necessarily know that the segment originated from different hosts, since Host C's socket is only defined by the port number of the process and IP address of Host C. However, the process can inspect the incoming segment, which will have the source port number and IP address. A UDP socket is *defined* by only the destination port/IP, but the incoming segments do *include* a source port/IP (the process at Host C must know where to send it's response!).

## R8: Suppose that a Web server runs in Host C on port 80. Suppose this Web server uses persistent connections, and is currently receiving requests from two different Hosts, A and B. Are all of the requests being sent through the same socket at Host C? If they are being passed through different sockets, do both of the sockets have port 80? Discuss and explain.

The first request received from Host A and Host B will end up going through the same socket - the 'welcoming' socket. Then, Host C will create two separate sockets for Host A and Host B, through which data will be sent separately. So no, the requests are not being sent through the same socket, because Host C creates a socket for every connection it currently has open. Also, both of the sockets do have a destination port of 80 - but TCP sockets are not just defined by destination port/IP, they are *also defined by* the source port/IP. So two sockets can have the same destination port number, and still be completely different sockets.

## R9: In our `rdt` protocols, why did we need to introduce sequence numbers?

The problem that arose was the fact that not only could a sent packet be corrupted, but an ACK/NAK could be as well! In the case that a sender receives a corrupted ACK/NAK, it has no idea which one it is, and thus does not know whether or not the receiver got the sent packet. The natural response would be to resend the packet, just to make sure the receiver got the packet. However, if the garbled response was an ACK, then the receiver would be waiting for the *next* packet, not a re-sent one. So the re-sent packet would be mistaken for the next packet, and our protocol would not work. The solution was sequence numbers - each packet would have a sequence number (in the simplest case, a 1 or a 0) that identified where in the sequence of data it went. Thus, the receiver would know if a packet was a re-sent packet or a new packet.

## R10: In our `rdt` protocols, why did we need to introduce timers?

When we introduced a channel that could *lose* packets in addition to corrupting them, we needed a way for the sender to know if a packet was dropped entirely. The logical solution would be to have the sender wait a given amount of time for a packet to be ACK'd. After the amount of time had elapsed, the sender could be pretty sure that the packet was dropped, and re-send the packet. A timer would thus need to be implemented, keeping track of how long it had been since a packet was sent. If the timer reached the given amount of time, then the packet would need to be re-sent and the timer would restart.

## R11: Suppose that the roundtrip delay between sender and receiver is constant and known to the sender. Would a timer still be necessary in protocol `rdt 3.0`, assuming that packets can be lost? Explain.

Yes, a timer would still be necessary. The only difference with the roundtrip delay being known and constant is that the packet loss could be 100% optimized, with the sender waiting the exact amount of time a packet would be sent and ACK'd before re-sending the packet. If 1. There is packet delay and 2. packets can be dropped, then a timer is always necessary, regardless of if the delay is constant and known. A delay means that the sender must wait, and packets being dropped means that the sender does not know if a packet made it to it's destination or if the ACK will make it back.

## R15: Suppose Host A sends two TCP segments back to back to Host B over a TCP connection. The first segment has sequence number 90; the second has sequence number 110.

### a. How much data is in the first segment?

The first segment has 20 bytes - TCP segment numbers correspond to the first byte of the segment. If the first byte of the first packet is 90 and the first byte of the second packet is 110, then 110 - 90 = 20 bytes.

### b. Suppose that the first segment is lost but the second segment arrives at B. In the acknowledgment that Host B sends to Host A, what will be the acknowledgment number?

TCP acknowledgment numbers correspond to the byte that the receiver expects *next*. At the start of the exchange, the receiver would (sort of) be expecting byte 90. If the first packet is lost, then the receiver will receive byte 110, which it knows is out of order. Thus, it will send an acknowledgment number of 90 back to the sender, which tells the sender "I got your packet, but I was expecting byte *90*".

## P3. UDP and TCP use 1s complement for their checksums. Suppose you have the following three 8-bit bytes: 01010011, 01100110, 01110100. What is the 1s complement of the sum of these 8-bit bytes? (Note that although UDP and TCP use 16-bit words in computing the checksum, for this problem you are being asked to consider 8-bit sums.) Show all work. Why is it that UDP takes the 1s complement of the sum; that is, why not just use the sum? With the 1s complement scheme, how does the receiver detect errors? Is it possible that a 1-bit error will go undetected? How about a 2-bit error?

1. 01010011 + 01100110 = 10111001
2. 10111001 + 01110100 = 00101110
3. 1's complement of 00101110 (flip all 1s to 0s and v.v.) -> 11010001.

Using information from [this forum](https://stackoverflow.com/questions/5607978/how-is-a-1s-complement-checksum-useful-for-error-detection), I learned that a 1s complement is taken and added because of the hardware. CPUs are very fast at taking 1s complements and checking if the previous result was zero. However, it is a lot slower at checking each individual bit of a number, so the use of 1s complement is essentially just an optimization.

A 1-bit error will never go undetected, because by making only one change, the sum will be different. However, making two or more changes introduces the chance that the exact right bits were changed to have the result still be correct. For a simple example, one of the 16-bit words had the last bit flipped from a 1 to a 0, and another 16-bit word had the last bit flipped from a 0 to a 1, then the checksum would still be the same, and so the message would be deemed as error-free. 