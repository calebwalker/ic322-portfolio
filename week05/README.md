# Week 5: The Transport Layer: TCP Congestion and Flow Control

*MIDN Caleb Walker | Fall 2023*

* [Learning Objectives](learning-objectives.md)
* [Vegetables](vegetables.md)
* [Feedback for Ayoo's Portfolio](https://gitlab.usna.edu/ayoodada28/ic322-portfolio/-/issues/6)