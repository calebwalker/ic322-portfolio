# Questions from *Kurose* Chapter 3

## R17: Suppose two TCP connections are present over some bottleneck link of rate *R* bps. Both connections have a huge file to send (in the same direction over the bottleneck). The transmissions of the files start at the same time. What transmission rate would TCP like to give to each of the connections?

TCP would like to give both connections *R*/2. TCP strives for fairness of connections, so the bottleneck link should be getting equal amounts of packets from each sender. This should work out due to the congestion control methods that TCP implements, namely Slow Start and Congestion Avoidance.

## R18: True or false? Consider congestion control in TCP. When the timer expires at the sender, the value of `ssthresh` is set to one half of its previous value?

False. The new `ssthresh` value is not based on the previous value at all. Rather, it is based on the value of `cwnd`/2, where `cwnd` is the number of sent-but-unACKed segments that were on the network when the network indicated congestion (either directly or indirectly).

## P27: Host A and B are communicating over a TCP connection, and Host B has already received from A all bytes up through byte 126. Suppose Host A then sends two segments to Host B back-to-back. The first and second segment contain 80 and 40 bytes of data, respectively. In the first segment, the sequence number is 127, the source port number is 302, and the destination port number is 80. Host B sends an acknowledgment whenever it receives a segment from Host A.

### a. In the second segment sent from Host A to B, what are the sequence number, source port number, and destination port number?

* Sequence number: 207
* Source port number: 302
* Destination port number: 80

### b. If the first segment arrives before the second segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number, the source port number, and the destination port number?

* Acknowledgment number: 207
* Source port number: 80
* Destination port number: 302

### c. If the second segment arrives before the first segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number?

* Acknowledgment number: 127

### d. Suppose the two segments sent by A arrive in order at B. The first acknowledgment is lost and the second acknowledgment arrives after the first timeout interval. Draw a timing diagram, showing these segments and all other segments and acknowledgments sent. (Assume there is no additional packet loss). For each segment in your figure, provide the sequence number and the number of bytes of data; for each acknowledgment that you add, provide the acknowledgment number.

![Drawn P27(d) Answer](../images/week05/P27_d_.jpg)

## P33: In Section 3.5.3, we discussed TCP's estimation of RTT. Why do you think TCP avoids measuring the `SampleRTT` for retransmitted segments?

The problem with measuring the `SampleRTT` for retransmitted segments is that the ACK for the segment is ambiguous - it could be for the retransmitted segment, or it could be for the original segment. Thus, the RTT of the segment is ambiguous, and cannot be determined. So retransmitted packets are ignored when measuring `SampleRTT`.

## P36: In Section 3.5.4, we saw that TCP waits until it has received three duplicate ACKs before performing a fast retransmit. Why do you think the TCP designers chose not to perform a fast retransmit after the first duplicate ACK for a segment is received?

TCP was designed in such a way as to cut down on unnecessary retransmissions. By waiting for 3 duplicate ACKs, the sender can know with much more certainty that the segment was lost. It is highly unlikely that the segment will be so delayed that it ends up being 3+ segments out of order. However, it is much more likely that the segment will take slightly longer than the following segment, resulting in only 1 or 2 segments out of order. In this case, the sender would receive 1 or 2 duplicate ACKs before receiving the cumulative ACK. If the sender didn't wait for 3 duplicate ACKs, then it would retransmit the original packet unecessarily.

## P40: Consider Figure 3.61. Assuming TCP Reno is the protocol experiencing the behavior shown above, answer the following questions. In all cases, you should provide a short discussion justifying your answer.

![Figure 3.61](../images/week05/figure-3_61.jpg)

### a. Identify the intervals of time when TCP slow start is operating.

From Transmission round 1-6 and 23+, TCP is in slow start. This is because the Congestion window size is increasing exponentially, which only occurs during the slow start phase.

### b. Identify the intervals of time when TCP congestion avoidance is operating.

From Transmission round 6-16 and 17-22, TCP is in congestion avoidance. This is because the Congestion window size is increasing linearly, which only occurs during the congestion avoidance phase.

### c. After the 16th transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?

Segment loss after the 16th transmission round is detected by a triple duplicate ACK. This is because the Congestion window size is only cut in half, and not reset back to 0. If a timeout occurs, the window size is reset back to 0, but upon arrival of 3 duplicate ACKs, the sender only halves the window size. This is because duplicate ACKs indicate a single lost segment, not necessarily network congestion, so the sender doesn't need to throttle its data rate as much.

### d. After the 22nd transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?

Segment loss after the 22nd transmission round is detected by a timeout. This is because the Congestion window size is reset back to 0, and not cut in half. The timeout indicates that the network is congested, so the sender resets the window size to 1 segment.