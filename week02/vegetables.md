# Questions from *Kurose* Chapter 2

## R5: What information is used by a process running on one host to identify a process running on another host?

The information needed would be the IP address, to identify the actual machine that the process is on, as well as the socket/port number, to identify the specific process on the machine.

## R8: List the four broad classes of services that a transport protocol can provide. For each of the service classes, indicate if either UDP or TCP (or both) provides such a service.

*Reliable Data Transfer: The protocol guarantees that packets will make it to the destination, with no corruptions, and in order. This is supported by TCP, but not supported by UDP.
*Throughput: The protocol can guarantee a requested bitrate (within reason), meaning that the hosts will always be able to send at least *r* bits/second between each other. This is supported by TCP, and not supported by UDP (although this usually ends up meaning that UDP just transmits as fast as it possibly wants/needs).
*Timing: The protocol guarantees a fast delivery of packets/data. This is supported by UDP (guarantees the packets will get there fast, but only if they get there at all), and not supported by TCP.
*Security: The protocol provides security for the data being transferred, so that if the information is observed during transit, it cannot be deciphered. This is not supported by either TCP or UDP, but an enhancement for TCP called Transport Layer Security (TLS) adds encryption and other security services to the protocol.

## R11: Why do HTTP, SMTP, and IMAP run on top of TCP rather than UDP?

These protocols need the reliable transfer aspect of TCP. They rely on the assumption that all the packets will get to their destination, and be able to be reconstructed successfully. If not, the webpage will not load correctly, or the e-mail will be all screwed up. The guarantee of correct data is critical to these services, as opposed to video or audio streaming, which would only incur a small glitch if some of the packets didn't make it. Also, it is not imperative that the information be sent *right away*, because even if a webpage loads in 500ms rather than 200ms, to a human it's pretty much the same difference.

## R12: Consider an e-commerce site that wants to keep a purchase record for each of its customers. Describe how this can be done with cookies.

The site could have a system where a user needs a cookie to log in and use the site, which would first make sure every user must be associated with some unique account. Then, the site keeps a database of users, and for each user, stores a purchase record. Then, whenever a user purchases something from the website, the database is updated to include new purchase history data for the user. This works because every time the user accesses the site, the HTTP request message includes the user's cookie 'id', so the site knows exactly who the user is, and exactly which database entry to update when they purchase something.

## R13: Describe how Web caching can reduce the delay in receiving a requested object. Will Web caching reduce the delay for all objects requested by a user or for only some of the objects? Why?

Web caching allows for large and mostly static files to be stored in geographically sensible areas, to reduce the delay that would be required to retrieve the same files from the main web server (which is usually much further away). When a client requests any of the files, they come from the cache instead of the main server, which means that the data doesn't have to travel as far, and cuts down on the delays as a result.

Since caches are mainly used for large files, and are either maintained specifically for one service/website or have a charge attached to storing a given server's objects, this means that not all websites utilize/have web caching. So web caching does not reduce delay for all objects requested by a user, because if the client requests a webpage from a server/site that does not use web caching, then those objects won't come from web caching.

## R14: Telnet into a Web server and send a multiline request message. Include in the request message the 'If-modified-since:' header line to force a response message with the '304 Not Modified' status code.

![telnet command and HTTP response (304 Not Modified status code)](../images/week02/telnet-example.png)

## R26: In Section 2.7, the UDP server described needed only one socket, whereas the TCP server needed two sockets. Why? If the TCP server were to support *n* simultaneous connections, each from a different client host, how many sockets would the TCP server need?

A TCP socket connection requires more than just a single connection, because it has a 'welcome' socket that accepts all incoming connection requests. Then, once a client gets to the welcome socket, the process creates a new socket on the server side solely for the client, and the welcome socket goes back to listening for new connection requests. Thus, TCP requires at least two sockets for a connection, and more generally, *n*+1 sockets for *n* simultaneous connections, because it needs a socket for each connection and a welcome socket.