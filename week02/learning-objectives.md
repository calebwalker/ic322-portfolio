# Week 2 Learning Objectives

## I can explain the HTTP message format, including the common fields.

There are two types of messages that HTTP specifies the format for: request messages and response messages.

The request message starts with a request line, followed by header lines. The request line is made up of the method field (common values include GET and POST), the URL field, and the Version field. The header lines can contain fields that give information about the request, such as the host, connection specifications, and preferred language. There is also an entity body section of a request message, which may or may not have any data, depending on the method field value in the request line (GET vs. POST - explained in the next learning objective).

The response message starts instead with a status line, but then also follows with header lines. Also, a response method contains an entity body, and this is always filled with the information that was requested. The status field is made up of the protocol version field, status code field, and a corresponding status message field. The header lines have fields that give information about the connection specifications, date of the response message, and size/type of information in the entity body.

## I can explain the difference between HTTP GET and POST requests and why you would use each.

GET requests are a type of HTTP request that don't have any information in the entity body. If a client is simply requesting a webpage, or really any regular data, a GET request is the standard, because there isn't really any need for the client to provide additional information to the server in order to get the requested object(s). If there is any extra data that needs to be provided (if the user was submitting a form of some sort, per se), then the GET method includes the information in the URL.

POST requests are almost the same as GET requests, except they do have information in the entity body. POST requests should be used if there is any information that either cannot or should not be included in the URL. For example, if one of the form entries is a password field, a user's password should not be included in the URL for security reasons. Also, if the client needs to, for whatever reason, include any information other than text (such as an image or other file), that cannot be included in a URL (a URL has a fairly small character limit).

## I can explain what CONDITIONAL GET requests are, what they look like, and what problem they solve.

A CONDITIONAL GET request is a type of request, only used by caches, that eliminates the need for an entire object to be sent over the internet if the cache that requests the object already has the latest version of the object stored.

If a cache does not have a given object, it will send a normal GET request to the web server for the object, and the server will send a response with the object in the entity body. This response also includes a date that signifies the last time this certain object was modified. Then, if a browser tries to access the given web server through the given cache, the cache first sends a CONDITIONAL GET request to the web server for the object. It is essentially a GET request, except it has a field called 'If-modified-since:'. It will make the value of this field the date that was given when it first requested the object from the web server. Then, the web server will either respond with a 304 (Not Modified) response code, or an updated version of the object, based on the 'If-modified-since' field. This cuts down on unnecessary internet traffic, since an empty 304 HTTP response message is much less data than an entire object.

## I can explain what an HTTP response code is, how they are used, and I can list common codes and their meanings.

An HTTP response code is a status code that indicates the result of the request. It basically lets the client know what happened with its request. Some examples are:

* 200 (OK): The request was a success, and the information is in the response.
* 301 (Moved Permanently): Requested object is at a new location/destination; will normally give the new URL, and the client's browser often redirects automatically.
* 400 (Bad Request): A generic response code that indicates there was some problem understanding the request.
* 404 (Not Found): The requested object does not exist.
* 505 (HTTP Version Not Supported): Self-explanatory; the requested HTTP protocol is not supported by the server.

## I can explain what cookies are used for and how they are implemented.

Cookies are a tool used by Web designers to, despite the HTTP protocol design being stateless, keep track of user identity and allow the user's identity to affect the contents or availability of the webpage. While this may seem dangerous or overreaching, without cookies, we would have to log in to a website every time we clicked a link within the website (very impractical).

As for how cookies work, it is essentially an addition in the HTTP header lines that keeps track of user identity with a unique ID. When visiting a website for the first time, the website includes a 'Set-cookie' header line with its response message. The value that this field has in the response message is the client's unique ID. It should provide this ID every consequent time it requests objects from the given website/server.

When the client receives this ID, it stores the website and corresponding cookie ID in a running file that keeps track of the client's cookie IDs for each frequently visited website. Also, on the other end, the server adds an entry in its database of users, so that it can keep track and pull up information of individual users when they access the website.

Now, every time the client accesses the website again, the HTTP request message also includes a cookie field with the user's unique ID, and when the server receives the request message, it takes the ID in the cookie field, finds the entry in its database, and implements the given information into the webpage as it sees fit.

## I can explain the significance of HTTP pipelining.

HTTP pipelining is significant because it allows for a faster transfer of objects from a web server to a client/browser. Normally, a client would first request the base HTML for the webpage, which would require a TCP connection. Then, after the client gets the response message with the HTML, the server closes the TCP connection, only to open new ones for each additional object the client needs. What HTTP pipelining does is allow the TCP connection to remain open, so that there isn't as much time taken up by executing the TCP handshake for each object, but rather sending all the objects over a single TCP connection.

## I can explain the significance of CDNs, and how they work.

If a web server has just one location that holds all of its large files, then all users globally must access that specific server. This can cause multiple problems, namely problems with having too many connections at once, problems with low bandwidth over long distances, and problems with redundancy (if that one server goes down, the information isn't accessible). A CDN is a way to mitigate these problems.

CDNs are essentially highly specialized web caches that store large files (often videos) in a geographically focused fashion. With CDNs, a server just has to send one copy of any large files, and the CDN will keep those copies stored for future access. So if a user requests one of these files, the idea is that a CDN exists nearby that can provide the files, and the data can go just a short distance from the CDN to the client rather than all the way to the main server. This significantly reduces internet traffic, because the data is always much closer, and so there are rarely duplicates being sent over the same connections/links. A CDN provides the data at a lower level of ISP, and thus also cuts costs down as well.