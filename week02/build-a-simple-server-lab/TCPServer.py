from socket import *
serverPort = 12000
serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind(('', serverPort))
serverSocket.listen(1)
print('The server is now ready to receive requests')
while True:
    connectionSocket, addr = serverSocket.accept()
    request = connectionSocket.recv(1024).decode()
    response = "HTTP/1.1 200 OK\r\n"

    # Take the entire HTTP request message and parse the second word (which is the filename)
    # Then take out the first character (it is always a '/')
    filename = (request.split()[1])[1:]

    # Take out file extension
    extension = filename.split(".")[1]

    try:

        # Check if file is an image
        if extension == "png" or extension == "jpg":

            # Add Content-type header field to tell the browser that the data is in an image format
            response += "Content-type: image/"
            if(extension == "jpg"):
                extension = "jpeg"
            response += extension
            response += "\r\n\r\n"

            # Open the requested file ('rb' to read file in binary, since it is an image)
            f = open(filename, "rb")

            # Send the HTTP response message header
            connectionSocket.send(response.encode())

            # Send the HTTP response message entity body (image file contents)
            connectionSocket.send(f.read())

        # If file is not an image, it is a regular text file (assumed)
        else:

            # No header lines needed
            response += "\r\n"

            # Open the requested file
            f = open(filename)

            # Send the HTTP response message header
            connectionSocket.send(response.encode())

            # Send the HTTP response message entity body (file contents)
            connectionSocket.send(f.read().encode())
            
    # If the requested file doesn't exist, send 404 status code and send the 404 HTML page
    except IOError:

        # New response to indicate an error
        response = "HTTP/1.1 404 Not Found\r\n\r\n"

        # Open the 404 HTML file
        f = open("404.html")

        # Send the HTTP response message header
        connectionSocket.send(response.encode())

        # Send the HTTP response message entity body (file contents)
        connectionSocket.send(f.read().encode())
    
    # Close connection
    connectionSocket.close()