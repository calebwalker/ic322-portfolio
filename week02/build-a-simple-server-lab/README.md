# Lab: Build a Simple Server

## Introduction

This lab was about creating a simple server that could respond to multiple different types of HTTP requests. I learned how to create a skeleton server, modify and utilize the HTTP protocol, and handle the connection between the application and transport layers.

## Collaboration/Citations

I used [Soleil's Lab](https://gitlab.com/soleilxie1/ic322-portfolio/-/tree/master/week2/lab-build-a-server?ref_type=heads) for example code of how to read a file as binary and modify the 'Content-type' header field. I also used her code to understand the connection between the `cr` and `lf`, and `\r` and `\n`.

## Process

This lab was done in accordance with the instructions outlined in the [Build a Server Lab](https://courses.cs.usna.edu/IC322/#/assignments/week02/lab-build-a-server). I somewhat added an additional component by adding a 404.html page that is sent if the server is unable to read the requested file.

## Questions

### Q1: Why are we focusing on the TCP server in this lab rather than the UDP server?

Because in almost all cases, HTTP utilizes the TCP protocol, as well as most other application protocols. It has much more structure, and is connection-oriented, meaning information is meant to be passed through secure channels rather than thrown out in the case of UDP. We don't really have any need to send time-sensitive information, so the advantages of UDP are negligible. Also, really care about making sure all of the information arrives (because if not, the webpage/object will be all messed up), so TCP is a lot better.

### Q2: Look carefully at Figure 2.9 (General format of an HTTP response message). Notice that there's a blank line between the header section and the body. And notice that the blank line is two characters: `cr` and `lf`. What are these characters and how do we represent them in our Python response string?

The `cr` character represents 'Carriage Return', denoted by `\r` in the Python response string, and the `lf` character represents 'Line Feed', denoted by `\n` in the Python response string. They are characters that are typically invoked together when the 'Enter' key is pressed, hence the abnormal representation in a string.

### Q3: When a client requests the `/index.html` file, where on your computer will your server look for that file?

My computer would look in the same directory that the server script is from. The computer would understand the request to be a relative path, and would consequently start from that directory.