# Week 2: The Application Layer: HTTP

*MIDN Caleb Walker | Fall 2023*

* [Learning Objectives](learning-objectives.md)
* [Build a Simple Server Lab](build-a-simple-server-lab)
* [Vegetables](vegetables.md)