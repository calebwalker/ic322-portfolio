# Questions from *Kurose* Chapter 5

## R11: How does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?

The BGP NEXT-HOP attribute is the address of the router interface that begins the AS-PATH. If an AS receives an AS-PATH, the NEXT-HOP attribute will be the router interface that sends the eBGP message. BGP uses the NEXT-HOP attribute to compute a path (along with other attributes). With hot-potato routing, the router would determine the least-cost path to every NEXT-HOP address for a given address. It will have received one or more paths to an address, each with a different NEXT-HOP. The hot-potato routing will then choose the least-cost path as the path to get to the destination. In this sense, the routers are trying to pass messages to other autonomous systems using the least amount of resources.

The BGP AS-PATH attribute contains a sequence of all autonomous systems along a path to a given destination. BGP uses this attribute in a couple of different way, sometimes depending on the actual type of BGP routing. The length of AS-PATH can give insight into the amount of hops required to reach a destination, which could factor into the decision of which path to choose. Another way BGP uses the AS-PATH attribute is preventing looping advertisements. If a router sees its own AS in the AS-PATH, then it can throw that path out, because the path is redundant.

## R13: True or False: When a BGP router receives an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all of its neighbors. Explain.

False. BGP routers do not add their own identity to received paths when sending the new path to its neighbors within the same AS. The only time that things are added to a path are when a gateway router advertises a path to another AS - and in that case, the entire AS is added to the path, not the specific router. Also, as mentioned in the previous question, if a router notices its own AS in the path, it throws the path out and does not send it to any of its neighbors.

## R19: Name four different types of ICMP messages.

* **ICMP Type 3, Code 0:** Destination Network Unreachable - used to let the sending host know that the datagram was unable to make it to the given network.
* **ICMP Type 3, Code 6:** Destination Network Unknown - used to let the sending host know that the given network wasn't found.
* **ICMP Type 8, Code 0:** Echo Request - used to let the destination host know that the sender would like an ICMP Type 0 Code 0 response message.
* **ICMP Type 11, Code 0:** TTL Expired - used to let the sending host know that the datagram expired according to it's TTL field.

## R20: What two types of ICMP messages are received at the sending host executing the *Traceroute* program?

Traceroute programs will receive ICMP Type 11 Code 0 (TTL Expired) messages, which let the program know sequential routers along the path. It also receives ICMP Type 3 Code 3 (Destination Port Unreachable) messages when the program's datagram finally made it to the destination host. Since traceroute chose an unlikely UDP port number, the host sends the ICMP message, and that is how traceroute knows to stop sending datagrams and compile the results.

## P14: Consider the network shown below. Suppose that AS3 and AS2 are running OSPF for their intra-AS routing protocol. Suppose AS1 and AS4 are running RIP for their intra-AS routing protocol. Suppose eBGP and iBGP are used for the inter-AS routing protocol. Initially suppose that there is *no* physical link between AS2 and AS4.

![AS Network](../images/week10/P14.png)

### a. Router 3c learns about prefix *x* from which routing protocol: OSPF, RIP, eBGP, or iBGP?

Router 3c learns about prefix *x* from eBGP.

### b. Router 3a learns about *x* from which routing protocol?

Router 3a learns about prefix *x* from iBGP.

### b. Router 1c learns about *x* from which routing protocol?

Router 1c learns about prefix *x* from eBGP.

### b. Router 1d learns about *x* from which routing protocol?

Router 1d learns about prefix *x* from iBGP.

## P15: Referring to the previous problem, once router 1d learns about *x* it will put an entry (*x*, *I*) in its forwarding table.

### a. Will *I* be equal to *I*<sub>1</sub> or *I*<sub>2</sub> for this entry? Explain why in one sentence.

*I* will be equal to *I*<sub>1</sub> because in almost all cases, 1d will determine the least-cost path to 1c to be through 1a directly.

### b. Now suppose that there is a physical link between AS2 and AS4, shown by the dotted line. Suppose router 1d learns that *x* is accessible via AS2 as well as via AS3. Will *I* be set to *I*<sub>1</sub> or *I*<sub>2</sub>? Explain why in one sentence.

Now, *I* will be equal to *I*<sub>2</sub> because the shortest path should theoretically be the 4 links through AS2, not the 8 links through AS3. However, this would depend on certain policies and costs determined by the BGP protocol being used.

### c. Now suppose there is another AS, called AS5, which lies on the path between AS2 and AS4 (not shown in diagram). Suppose router 1d learns that *x* is accessible via AS2 AS5 AS4 as well as via AS3 AS4. Will *I* be set to *I*1 or *I*2? Explain why in one sentence.

In this case, *I* will probably be set to *I*<sub>1</sub> because BGP usually chooses the shortest AS-PATH, and thus routing through AS2 and AS5 would be deemed more costly than just routing through AS1.

## P19: In Figure 5.13, suppose that there is another stub network V that is a customer of ISP A. Suppose that B and C have a peering relationship, and A is a customer of both B and C. Suppose that A would like to have the traffic destined to W to come from B only, and the traffic destined to V from either B or C. How should A advertise its routes to B and C? What AS routes does C receive?

![Figure 5.13](../images/week10/P19.png)

A should advertise the path (A -> W) only to B, and the path (A -> V) to B and C. This way, C does not know that A can get to to W, which will mean that A only gets traffic bound for W from B.

C will get the following paths:
* (A -> V)
* (B -> A -> V)
* (B -> A -> W)

## P20: Suppose ASs X and Z are not directly connected but instead are connected by AS Y. Further suppose that X has a peering agreement with Y, and that Y has a peering agreement with Z. Finally, suppose that Z wants to transit all of Y's traffic but does not want to transit X's traffic. Does BGP allow Z to implement this policy?

Based on my current knowledge of BGP from the information in the textbook, I cannot think of any actions Z could take to ensure that it fully accepts Y's traffic but does not accept X's traffic. However, I assume that such a policy could be implemented using BGP, and that it involves more complicated aspects of BGP.