# Week 10: The Network Layer: More Control Plane

*MIDN Caleb Walker | Fall 2023*

* [Learning Objectives](learning-objectives.md)
* [Protocol Pioneer](protocol-pioneer.md)
* [Feedback for Nicholas' Portfolio](https://gitlab.usna.edu/ndzayfman/ic322/-/issues/15)
* [Vegetables](vegetables.md)