# Lab: Protocol Pioneer

## Introduction

This lab involved playing LCDR Downs' beta-version game and testing its functionality. The game is intended to teach others about network protocols without explicitly referring to them.

## Collaboration/Citations

The game is located [here](https://gitlab.com/jldowns-usna/protocol-pioneer/-/tree/main?ref_type=heads). I did not use any additional resources, nor did I get help from anyone on this lab.

## Process

I followed the lab instructions, and completed Chapter 1 and 2 of the game. This was all in accordance with the instructions outlined in the [Protocol Pioneer Lab](https://courses.cs.usna.edu/IC322/#/assignments/week07/protocol-pioneer/).

## Questions

### Q1: How did you solve the Chapters? Please copy and paste your winning strategy(s), and also explain it in English.

For the sixth chapter, my solution was just completely cheesy. I was able to come up with this strategy after reading the part of the instructions that mentioned a drone could generate a key for itself. With this, I realized that the rate could be spoofed by just generating hundreds of keys for each drone every tick. Then, I would just discard the keys for other drones, and immediately call `operate_radio(k)` whenever a key was generated for the battlebot that was generating the key. This allowed me to get essentially an infinite rate. The only reason this worked is because a drone could generate an infinite amount of keys per tick, and a drone could generate a key for itself.

```python
for i in range(99999999999...):
    key = self.generate_key()
    if key[2] == self.id:
        self.operate_radio(key[0])
```

### Q2: Include a section on Beta Testing Notes.

This is obviously not the intended solution, but hopefully it provides feedback. To eliminate the cheesy solution, perhaps you could limit the number of keys a bot could generate per tick, or just only generate keys for other battlebots. Other than that, this chapter is a good follow-up to the previous chapter. However, I think it would be good to have some sort of end-state; maybe the chapter ends when the rate reaches a certain value, or when the user has successfully operated a certain number of radios. It feels out of place to just be trying to optimize the routing (even though that is a very realistic problem) without having the feeling of success when the program finally works.
