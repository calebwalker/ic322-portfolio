class Address:
    def __init__(self, mask):
        ddn = mask.split("/")[0].split(".")
        self.bits = []
        for x in range(4):
            self.bits.append(int(ddn[x]))
        self.maskLength = int(mask.split("/")[1])

    def matchLength(self, addr):
        count = 0
        flag = False
        for x in range(4):
            for y in range(7, -1, -1):
                if((self.bits[x] >> y) ^ (addr.bits[x] >> y) == 0 and count < self.maskLength):
                    count += 1
                else:
                    flag = True
                    break
            if(flag):
                break
        return count if count == self.maskLength else 0


inp = input("Enter an address: ")
ddn = []
for x in range(4):
    substr = "0b" + inp[x*8:(x+1)*8]
    ddn.append(str(int(substr, 2)))
inpAddress = Address(ddn[0]+"."+ddn[1]+"."+ddn[2]+"."+ddn[3]+"/0")
addresses = ["1.0.0.0/8", "1.100.56.0/24", "1.0.100.0/24", "1.99.0.0/20", "8.0.0.0/27", "8.8.8.0/24", "126.2.3.0/20", "8.8.8.16/30", "237.1.1.0/30", "237.2.0.0/16", "99.31.4.0/22", "99.0.0.0/8", "101.5.0.0/16", "223.0.0.0/8", "101.5.7.32/30", "99.0.0.0/20", "101.0.0.0/8", "101.42.3.0/24", "223.4.0.0/16"]
interfaces = ["Fa 0/0", "Fa 2/0", "Fa 0/1", "Fa 2/1", "Fa 0/2", "Fa 2/2", "Fa 0/3", "Fa 2/3", "Fa 0/4", "Fa 2/4", "Fa 0/5", "Fa 2/5", "Fa 0/6", "Fa 2/6", "Fa 0/7", "Fa 2/7", "Gi 1/0", "Gi 3/0", "Gi 1/1"]
longestMatch = 0
maxIndex = -1
currIndex = 0
for x in addresses:
    localMatchLength = Address(x).matchLength(inpAddress)
    if(localMatchLength > longestMatch):
        longestMatch = localMatchLength
        maxIndex = currIndex
    currIndex += 1
if(maxIndex != -1):
    print(interfaces[maxIndex])
else:
    print("Gi 3/1")