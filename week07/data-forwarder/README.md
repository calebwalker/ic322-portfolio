# Lab: Router Simulator

## Introduction

This lab involved creating a simulated router in python to read in a IP address (given fully in binary) and output a corresponding link that the hypothetical datagram should be forwarded to. The lab helped me conceptualize how longest prefix matching worked by building a simulated router that does just that.

## Collaboration/Citations

I did not use any additional resources, nor did I get help from anyone on this lab.

## Process

This lab was done in accordance with the instructions outlined in the [Router Simulator Lab](https://courses.cs.usna.edu/IC322/#/assignments/week07/lab-router-simulator/).

## Example test cases

![Simulation Test Runs](example.PNG)