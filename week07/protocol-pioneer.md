# Lab: Protocol Pioneer

## Introduction

This lab involved playing LCDR Downs' beta-version game and testing its functionality. The game is intended to teach others about network protocols without explicitly referring to them.

## Collaboration/Citations

The game is located [here](https://gitlab.com/jldowns-usna/protocol-pioneer/-/tree/main?ref_type=heads). I did not use any additional resources, nor did I get help from anyone on this lab.

## Process

I followed the lab instructions, and completed Chapter 1 and 2 of the game. This was all in accordance with the instructions outlined in the [Protocol Pioneer Lab](https://courses.cs.usna.edu/IC322/#/assignments/week07/protocol-pioneer/).

## Questions

### Q1: How did you solve the Chapters? Please copy and paste your winning strategy(s), and also explain it in English.

For the first chapter, the solution was pretty simple. I added the `self.send_message()` method call in the following code block:

```python
while(self.message_queue):
    m = self.message_queue.pop()
    self.send_message("Message Received", "W")
```

This essentially sends a message to the West (towards the Mothership) with "Message Received" as the text whenever there is an incoming message that is processed.

---

The next chapter was also fairly easy to solve, especially with the added information/hint about using the `self.state["received"]` variable. I added the following code chunk to the `while` loop that checked the message queue:

```python
state = self.state["received"][m.interface]
state[0] += 1
state[1] += int(m.text)
if(state[0] == 3):
    self.send_message(str(state[1]), m.interface)
```

This piece of code essentially creates a `state` key that points to the correct interface in the `self.state["received"]` map. Then, it increments left number to count the number of messages received on that interface, and also adds the received value to the right number to keep a running total. When a given interface has received 3 messages, it sends the running total back to the same interface.

### Q2: Include a section on Beta Testing Notes.

It was somewhat difficult to understand what was going on initially - it took some time before the instructions and the code part made sense. For the first chapter, maybe including more comment about what the `while` loop does could help the user know where code should go and when/how it executes. The description for sending messages is good, and the given example helps.

Other than that, I think the first two chapters were pretty good, and introduce the user to the general rundown of future chapters.