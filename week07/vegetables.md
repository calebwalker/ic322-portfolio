# Questions from *Kurose* Chapter 4

## R11: Describe how packet loss can occur at input ports. Describe how packet loss at input ports can be eliminated (without using infinite buffers).

Packet loss can occur at input ports in two scenarios. First, if the switching fabric is slower than the link bandwidth, then inevitably the packets will start queuing up at the input port until the buffer runs out of room. The other scenario is if packets at the head of the input port queue are temporarily blocked due to other input ports passing packets to the same output port. Both of these scenarios can be fixed by making sure the switching fabric is at least *N* times faster than the fastest link bandwidth, *N* being the number of input ports on the router.

## R12: Describe how packet loss can occur at output ports. Can this loss be prevented by increasing the switch fabric speed?

Packet loss occurs at output ports due to the switching fabric giving the output port more packets than the output link can handle. Because output port packet loss is due to the switching fabric speed being higher than the output link speed, increasing the switching fabric speed even more will not prevent packet loss.

## R13: What is HOL blocking? Does it occur in input ports or output ports?

Head of Line blocking occurs at input ports, and involves multiple input ports attempting to send packets to the same output port. Unless using a sophisticated switching fabric, this will result in only one input port sending a packet to the output port at a time - all the other input ports must wait to send their packet. However, the inefficiency is noticeable when the blocked packets have other packets behind them. These packets, if destined for a different port, are wasting unnecessary time. Essentially, if they were at the 'head of the line', they would be sent simultaneously with the other packet. However, it must wait, which causes long delays.

## R16: What is an essential difference between RR and WFQ packet scheduling? Is there a case where RR and WFQ will behave exactly the same?

RR scheduling places packets into groups, and cycles through the groups sending a packet from each group before moving on. WFQ is similar, except it places *weights* on the groups, and (mathematically) assuring that the groups get a throughput proportional to the weight. If WFQ places an equal weight on all groups, then it will function exactly the same as RR.

## R18: What field in the IP header can be used to ensure that a packet is forwarded through no more than *N* routers?

The TTL field ensures that a packet is forwarded through no more than *N* routers. When the packet is created, the TTL field is set to this *N*. At each subsequent 'hop', the router decreases the TTL field of the packet by 1. Then, if a router receives a packet with a TTL of 0, it drops the packet.

## R21: Do routers have IP addresses? If so, how many?

Routers do have IP addresses - typically more than one. Since IP addresses are associated with *interfaces* rather than machines, a router will have as many IP addresses as it has interfaces (one interface facing towards the internet, and one or more interfaces facing towards each subnet it connects to).

## P4: Consider the switch shown below. Suppose that all datagrams have the same fixed length, that the switch operates in a slotted, synchronous manner, and that in one time slot a datagram can be transferred from an input port to an output port. The switch fabric is a crossbar so that at most one datagram can be transferred to a given output port in a time slot, but different output ports can receive datagrams from different input ports in a single time slot. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want (i.e., it need not have HOL blocking)? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle?

![P4 Switch Diagram](../images/week07/p4.png)

Minimal time slots: 2
* The top input port sends packet `X`, the middle input port sends packet `Y`, and the bottom input sends packet `Z`.
* The middle input port sends packet `X`, and the bottom input port sends packet `Y`.

Maximum time slots: 3
* The top input port sends packet `X`, the middle input port sends packet `Y`. The bottom input port cannot send a packet, because in this case we are including HOL blocking, and so packet `Y` must wait in the bottom input port buffer.
* The middle input port sends packet `X`, and the bottom input port sends packet `Y`.
* The bottom input port sends packet `Z`.

## P5: Suppose that the WFQ scheduling policy is applied to a buffer that supports three classes, and suppose the weights are 0.5, 0.25, and 0.25 for the three classes.

### a. Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights (For RR scheduling, a natural sequence is 123123123...).

Given that class '1' is the 0.5 weighted class while '2' and '3' are the 0.25 weighted classes, a sequence might look something like:
`1 2 1 3 1 2 1 3 1 2 1 3 ...`

### b. Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?

Again assuming that class '1' is the 0.5 weighted class while '2' and '3' are the 0.25 weighted classes, and that class '3' has no packets, a sequence might look something like:
`1 1 2 1 1 2 1 1 2 ...`
(Of note, this is not to say that the packet scheduling policy doesn't check class '3' - it just shows which packets are actually sent).

## P8: Consider a datagram network using 32-bit host addresses. Suppose a router has four links, numbered 0 through 3, and packets are to be forwarded to the link interfaces as follows:

|      Destination Address Range      | Link Interface |
| :---------------------------------: | :------------: |
| 11100000 00000000 00000000 00000000 |                |
|               through               |       0        |
| 11100000 00111111 11111111 11111111 |                |
|                                     |                |
| 11100000 01000000 00000000 00000000 |                |
|               through               |       1        |
| 11100000 01000000 11111111 11111111 |                |
|                                     |                |
| 11100000 01000001 00000000 00000000 |                |
|               through               |       2        |
| 11100001 01111111 11111111 11111111 |                |
|                                     |                |
|              otherwise              |       3        |

### a. Provide a forwarding table that has five entries, uses longest prefix matching, and forwards packets to the correct link interfaces.

|       Prefix      | Link Interface |
| :---------------: | :------------: |
|    11100000 00    |       0        |
| 11100000 01000000 |       1        |
|     11100000      |       2        |
|    11100001 0     |       2        |
|     otherwise     |       3        |

### b. Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses:
* `11001000 10010001 01010001 01010101`
* `11100001 01000000 11000011 00111100`
* `11100001 10000000 00010001 01110111`

For the first datagram, the longest match is none of the prefixes, since all prefixes start with `111` but the first datagram is `110`. This defaults to the 'otherwise' case, and the datagram is correctly forwarded to link interface 3.

The second datagram has a longest match of **11100001 0**1000000 11000011 00111100. This longest match corresponds with link interface 2, and the datagram is forwarded accordingly.

For the third datagram, the longest match is none of the prefixes, since no prefixes start with `11100001 1`. This defaults to the 'otherwise' case, and the datagram is correctly forwarded to link interface 3.

## P9: Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table:

| Prefix | Link Interface |
| :----: | :------------: |
|   00   |       0        |
|   010  |       1        |
|   011  |       2        |
|   10   |       2        |
|   11   |       3        |

## P9 (Continued): For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range.

| Destination Address Range | Link Interface | Addresses |
| :-----------------------: | :------------: | :-------: |
|         00000000          |                |           |
|          through          |       0        |    63     |
|         00111111          |                |           |
|                           |                |           |
|         01000000          |                |           |
|          through          |       1        |    31     |
|         01011111          |                |           |
|                           |                |           |
|         01100000          |                |           |
|          through          |       2        |    95     |
|         10111111          |                |           |
|                           |                |           |
|         11000000          |                |           |
|          through          |       3        |    67     |
|         11111111          |                |           |

## P11: Consider a router that interconnects three subnets: Subnet 1, Subnet 2, and Subnet 3. Suppose all of the interfaces in each of these three subnets are required to have the prefix 223.1.17.0/24. Also suppose that Subnet 1 is required to support at least 60 interfaces, Subnet 2 is required to support at least 90 interfaces, and Subnet 3 is required to support at least 12 interfaces. Provide three network addresses (of the form a.b.c.d/x) that satisfy these constraints.

* **Subnet 1:** 223.1.17.128/26
* **Subnet 2:** 223.1.17.0/25
* **Subnet 2:** 223.1.17.192/26