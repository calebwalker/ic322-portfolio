# Week 7: The Network Layer: Data Plane

*MIDN Caleb Walker | Fall 2023*

* [Learning Objectives](learning-objectives.md)
* [Router Simulator Lab](data-forwarder)
* [Protocol Pioneer](protocol-pioneer.md)
* [Feedback for David's Portfolio](https://gitlab.com/ic3223/ic322-portfolio/-/issues/17)
* [Vegetables](vegetables.md)