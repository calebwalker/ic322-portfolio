# Week 7 Learning Objectives

## I can explain what a subnet is, how a subnet mask is used, and how longest prefix matching is used to route datagrams to their intended subnets.

A subnet is a designated range of IP addresses that correspond to a grouping of connected interfaces. An *interface* is the boundary between a host and the physical network connection, so the subnet is a grouping of such boundaries. Subnets are defined in IP by the first section of the address. IP addresses are represented as *dotted-decimal notation*, meaning the 4 bytes are represented as numbers and separated by a period, such as 192.45.3.80.

A subnet mask takes this notation and adds another piece of information to the end: 192.45.3.80/*n*, where *n* is the number of bits that define the subnet. So if our address is 192.45.3.80/8, then our subnet is defined as all IP addresses that start with 192. If it was 192.45.3.80/24, then the subnet would be defined as all IP addresses that start with 192.45.3. Importantly, the subnet mask doesn't need to be multiples of 8 - 192.45.3.80/13 is a valid subnet as well.

Longest prefix matching is used in conjunction with the properties of subnets and subnet masks to route datagrams appropriately. A larger subnet mask has a smaller range of IP addresses, and also a longer matching IP address to the real intended host. So when a router inspects a datagram and uses longest prefix matching, the router will send the datagram to the subnet with the smallest range of IP addresses, which whittles down the search for the host significantly.

## I can step through the DHCP protocol and show how it is used to assign IP addresses.

DHCP assigns new hosts that connect to a network a IP address. Importantly, it does so *dynamically*, without any human input, so a network can add new hosts automatically. The protocol involves a couple of messages that are exchanged between a DHCP client (a new host trying to connect to the network) and a DHCP server, not dissimilar to a TCP handshake.

* **Server Discovery:** This is the first step of DHCP, and involves the client broadcasting a message to the entire network saying *"I'm here, can anyone help me get an IP address?"*. The message, sent as a UDP segment to port 67, is encapsulated in an IP datagram with a destination IP of 255.255.255.255 and a source IP of 0.0.0.0. These special addresses are reserved specifically for DHCP, and when a DHCP server receives such a datagram, it knows that a host is trying to get an IP address on the network.

* **Server Offer(s):** This step involves the server responding to a discovery message by broadcasting its own IP address in another datagram: the destination IP is also 255.255.255.255, but the source IP is the actual IP of the DHCP server. The client will receive one (or more) datagrams that are broadcast with a destination of 255.255.255.255. For each of these datagrams, the client will glean the address of the DHCP server and the offered IP address.

* **DHCP Request:** This step simply involves the client responding to one of the offers (by echoing the specific parameters given by the server). This lets the server know it has been chosen by the client to provide an IP address.

* **DHCP ACK:** This step is just a confirmation from the server, acknowledging the request and finalizing the process.