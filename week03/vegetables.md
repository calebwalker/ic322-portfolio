# Questions from *Kurose* Chapter 2

## R16: Suppose Alice, with a Web-based e-mail account (such as Hotmail or Gmail), sends a message to Bob, who accesses his mail from his mail server using IMAP. Discuss how the message gets from Alice's host to Bob's host. Be sure to list the series of application-layer protocols that are used to move the message between the two hosts.

Alice composes some e-mail on her device, with the recipient being Bob, and hits 'send'. Since her e-mail account is Web-based, it will most likely use HTTP to transfer the e-mail to her mail server. The e-mail is outbound, so it ends up in the message queue, and once it reaches the front of the queue, Alice's mail server opens a TCP connection to Bob's mail server. Then, the e-mail is transferred to Bob's mail server using SMTP, and it arrives in Bob's specific inbox on the mail server. Then, whenever Bob uses his e-mail application, it will use the IMAP protocol to access the contents of his inbox, which will now have Alice's message.

## R18: What is the HOL blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?

HOL blocking is the issue that arises when a webpage's objects are retrieved over a single connection, and in order of appearance on the webpage. For example, if there is a large video file that appears before a bunch of smaller image files on a webpage, when a client loads that webpage the large video file will be requested first, and it will take a while to transfer that file. This means the smaller image files, even though they could have already been transferred, must wait on the large video file.

HTTP/1.1 got around this issue by opening up multiple parallel TCP connections to transfer multiple objects at once, but this can be impractical. HTTP/2 attempts to solve this issue by breaking up all objects on a webpage into smaller frames, and then interleaving the file transfers to transfer all of the files at the same time, but only giving them a portion of the total bandwidth. This way, small files can be transferred in a few frames, and can be loaded before the large files.

## R24: CDNs typically adopt one of two different server placement philosophies. Name and briefly describe them.

* *Enter Deep*: This philosophy entails placing CDNs at low levels of internet access. While this increases maintenance requirements, it allows for faster perceived connections and fewer links to access the content. There are many more CDN clusters in access ISPs in order to be physically close to users.
* *Bring Home*: This philosophy entails placing CDNs at strategic locations, mainly IXPs. This creates a very low-maintenance design, since there are much fewer clusters, but will usually include more links between the users and the CDN.

## P16: How does SMTP mark the end of a message body? How about HTTP? Can HTTP use the same method as SMTP to mark the end of a message body? Explain.

SMTP uses a single line with just a period to mark the end of a message body. This is technically `CRLF.CRLF`, with the `CRLF` representing the 'enter' key being pressed. HTTP marks the end of a message body with nothing - the message length is specified in the header line, and that is used to mark the end of the message. Thus, HTTP cannot use the same method to mark the end of a message body, since they employ two entirely different methods.

## P18 (a & b only):

### a. What is a *whois* database?

a. A *whois* database is a collection of hostnames, and their corresponding owner, the registration service that they were registered with, and the date of creation & expiration.

### b. Use various whois databases on the Internet to obtain the names of two DNS servers. Indicate which whois databases you used.

b. For `walmart.com`, the following DNS server was provided (from `whois.com`): `a1-185.akam.net`. For `nytimes.com`, the following DNS server was provided (from `who.is`): `dns1.p06.nsone.net`.

## P20: Suppose you can access the caches in the local DNS servers of your department. Can you propose a way to roughly determine the Web servers (outside your department) that are most popular among the users in your department? Explain.

Since local DNS servers cache website addresses that are visited for around two days, one would have to either compare or keep track of the cache over a period of time. The websites that stay in the cache are necessarily frequently visited - they are never removed, so they are visited frequently enough for the two-day timer to reset before running out.

## P21: Suppose that your department has a local DNS server for all computers in the department. You are an ordinary user (i.e., not a network/system administrator). Can you determine if an external Web site was likely accessed from a computer in your department a couple of seconds ago? Explain.

The local DNS server's cache stores websites that were accessed recently. So all we would need to do it figure out a way to know if a given website's address is in the cache, and that would mean the website was accessed recently. One way to do this would be to compare the time to load a website based on the hostname vs. the actual IP address. If the times are almost the same, then that means the resolution of the hostname took almost no time, meaning the local DNS cache likely had the hostname and corresponding address.