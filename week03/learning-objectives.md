# Week 3 Learning Objectives

## I can explain how the DNS system uses local, authoritative, TLD, and root servers to translate an IP address into a hostname.

The DNS system performs a number of functions for hosts on the internet, namely resolving human-readable hostnames into router-readable IP addresses. This is done through a hierarchical system of servers that coordinate and refer to each other to resolve parts of the hostname until the final IP address is returned.

The process starts with the client requesting DNS service from a root server (for example, to resolve the hostname `www.maps.google.com`). The root servers are the start of the DNS system, and they contain the addresses of all TLD servers. When a request comes in, a root DNS server will look at the top-level domain (`.com`, `.net`, `.edu`, etc) and return the appropriate IP address(es) for the corresponding TLD server (in this example, the `.com` TLD server).

The process continues with TLD (Top-level domain) servers. Once the client receives the IP address for the correct TLD server, it sends another request to that server. This time, the TLD server has IP addresses for authoritative servers with the given Top-level domain (all `.com` servers for this example). Once the TLD server finds the correct authoritative server (`www.google.com`), the TLD server returns the IP address of the authoritative server.

The final step of the process is the authoritative server. The client, similar to the previous step, receives the authoritative server IP address and sends a request for the final IP address of the resource it is requesting. The authoritative server (in the example, `www.google.com`) is specifically for a large company, or it hosts multiple smaller websites. It has all of the possible domains/hostnames and respective IP addresses. The authoritative server returns the final IP address that the client needs (for example, whatever the IP address for `www.maps.google.com` is), and the client uses that address to contact the server.

A local DNS server is not within the hierarchy of the DNS system, but still serves an important role in the DNS process. These servers belong to low-level ISPs, and when a client connects to the ISP, it provides IP addresses for any local DNS servers. The local DNS server essentially abstracts the DNS requesting process away from the actual host by handling all of the iterative requests through the hierarchy of the DNS system. With this system, a client will send a request to the local DNS server, that server will make the requests to root, TLD, and authoritative DNS servers, and then the local DNS server will return the final IP address to the client. This way, the client simplifies the process on its end: only having to send one request, and getting a final response in one step.

## I can explain the role of each DNS record type.

* `Type=A` records: These records are the simple hostname/IP address records. They just provide hostname resolution to an IP address, with the `Name` field being the hostname and `Value` being the IP address.
* `Type=NS` records: These records provide an authoritative DNS server for the given domain. The `Name` field is the domain (`amazon.com`), and the `Value` field is the hostname of the authoritative DNS server that has IP addresses for the given domain.
* `Type=CNAME` records: CNAME records deal with resolving canonical hostnames. The `Name` field is the alias hostname, and `Value` will be the canonical hostname.
* `Type=MX` records: MX records deal with resolving canonical hostnames for mail servers. The `Name` field is the alias mail server hostname, and the `Value` field will be the canonical mail server hostname.

## I can explain the role of the SMTP, IMAP, and POP protocols in the email system.

SMTP is a very old and very simple yet effective protocol used for the transfer of electronic mail over the Internet. It defines how the mail system works, and what information should be included in electronic mail.

Since it is an old protocol, some of the requirements and standards are archaic in nature. The body, along with the headers, of all emails must be in 7-bit ASCII. This is not conducive to an effective method for attachments such as images, videos, and other files (they must be encoded before transmission and decoded after transmission). However, it is very simple (it is literally in the name: SIMPLE mail transfer protocol) and is thus still exclusively used for any e-mail.

SMTP starts with the sending user composing an e-mail, and then once it is fully created, the user agent sends the SMTP message to their mail server. A mail server has an outgoing queue of messages, and eventually will send the SMTP message to the recipient's mail server. Mail servers also have individual inboxes for all users, and incoming messages go to the respective user's inbox. Then, the recipient can retrieve the message whenever (using HTTP or IMAP, which is explained below).

Since SMTP does not pertain to retrieving e-mail, there had to be other protocols that filled that role. One protocol is HTTP, which has already been covered, but is only used for web-based e-mail or an equal smartphone app. If the user agent is not using one of these e-mail services, then another protocol is used: IMAP (Internet Mail Access Protocol).

IMAP is a more streamlined protocol that is used for accessing a mail server's inbox and being able to retrieve messages, create and delete messages, and move messages into different folders/organize the inbox.

## I know what each of the following tools are used for: `nslookup`, `dig`, and `whois`.

* `nslookup`: This tool is used to send a DNS query directly to a DNS server. It will send the request, and display the response in a human-readable format.
* `dig`: This tool is used to obtain information about DNS servers. It is essentially a more sophisticated and newer version of `nslookup` - it can run with options to allow a user to see all types of records, hostnames, IP addresses, and any other configuration of information that is gleaned from the given DNS server.
* `whois`: This tool is just used to query registration information on a given hostname. The command returns which registrar the hostname was registered with, when it was registered, and when it expires.