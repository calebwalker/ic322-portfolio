# Week 3: The Application Layer: DNS and Other Protocols

*MIDN Caleb Walker | Fall 2023*

* [Learning Objectives](learning-objectives.md)
* [Wireshark Lab](wireshark-lab)
* [Vegetables](vegetables.md)