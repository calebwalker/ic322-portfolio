# Wireshark DNS Lab

## Introduction

In this lab, I explored DNS query messages and responses with Wireshark, and learned the different types of queries and how they support HTTP requests.

## Collaboration

I did not use any additional resources, nor did I get help from anyone on this lab.

## Process

I completed the "DNS" Wireshark lab on the [textbook's website](http://gaia.cs.umass.edu/kurose_ross/wireshark.php) (version 8.1). I didn't deviate from the instructions.

## Questions

### Q1: Run `nslookup` to obtain the IP address of the web server for the Indian Institute of Technology in Bombay, India: www.iitb.ac.in. What is the IP address of www.iitb.ac.in?

 The IP address of www.iitb.ac.in is 103.21.124.10.

### Q2: What is the IP address of the DNS server that provided the answer to your `nslookup` command in question 1 above?

The IP address of the DNS server is 10.1.74.10.

### Q3: Did the answer to your `nslookup` command in question 1 above come from an authoritative or non-authoritative server?

It came from a non-authoritative server, which was specified in the response from `nslookup`.

### Q4: Use the `nslookup` command to determine the name of the authoritative name server for the iitb.ac.in domain. What is that name? (If there are more than one authoritative server, what is the name of the first authoritative server returned by `nslookup`)? If you had to find the IP address of that authoritative name server, how would you do so?

The first authoritative name server that was returned was 'dns2.iitb.ac.in'. You could find the IP address of that authoritative name server by using a `nslookup dns2.iitb.ac.in`, which would request a type=A record from DNS, and would return the IP address.

### For Q5-Q11, the wireshark capture file is [here](dns-wireshark-trace-1.pcapng).

### Q5: Locate the first DNS query message resolving the name gaia.cs.umass.edu. What is the packet number in the trace for the DNS query message? Is this query message sent over UDP or TCP?

For my capture, the first DNS query message to resolve gaia.cs.umass.edu is packet 215. This packet was sent over UDP.

### Q6: Now locate the corresponding DNS response to the initial DNS query. What is the packet number in the trace for the DNS response message? Is this response message received via UDP or TCP?

For my capture, the DNS response message that resolved gaia.cs.umass.edu is packet 217. This packet was received over UDP.

### Q7: What is the destination port for the DNS query message? What is the source port of the DNS response message?

The destination port of the DNS query message is port 53, and the source port of the DNS response message is port 53.

### Q8: To what IP address is the DNS query message sent?

The DNS query message was sent to 10.1.74.10.

### Q9: Examine the DNS query message. How many "questions" does this DNS message contain? How many "answers" does it contain?

It only contains one "question", and doesn't contain any "answers", since the DNS query is only trying to resolve a hostname.

### Q10: Compare the DNS response message to the initial query message. How many "questions" does this DNS message contain? How many "answers" does it contain?

It contains one "question" and one "answer", presumably because it retained the original question and provided the answer as well.

### Q11: The web page for the base file http://gaia.cs.umass.edu/kurose_ross/ references the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E_2.jpg, which, like the base webpage, is on gaia.cs.umass.edu. What is the packet number in the trace for the initial HTTP GET request for the base file http://gaia.cs.umass.edu/kurose_ross/? What is the packet number in the trace of the DNS query made to resolve gaia.cs.umass.edu so that this initial HTTP request can be sent to the gaia.cs.umass.edu IP address? What is the packet number in the trace of the received DNS response? What is the packet number in the trace for the HTTP GET request for the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E_2.jpg? What is the packet number in the DNS query made to resolve gaia.cs.umass.edu so that this second HTTP request can be sent to the gaia.cs.umass.edu IP address? Discuss how DNS caching affects the answer to this last question.

For the base webpage HTTP GET request, the packet number is 316. However, the response was a "301: Moved Permanently", and the second request for the new link also received the same response. Finally, the browser actually requested "http://gaia.cs.umass.edu/kurose_ross/index.php", which was packet 440. For the DNS query to resolve gaia.cs.umass.edu, the packet number was 215. The received DNS response was packet 217. For the HTTP GET request for the image object, the packet was 652. There was no DNS query to resolve gaia.cs.umass.edu because the browser had already made that DNS query, and my computer stored the corresponding IP address in the DNS cache. Thus, upon reaching the HTTP GET request for the image object, the browser could just pull the IP address from the cache, and didn't need to make a query.

### For Q12-Q15, the wireshark capture file is [here](dns-wireshark-trace-2.pcapng).

To note: I believe the USNA servers handle DNS queries in a unique way - this causes multiple internal DNS queries to be made before the expected one. For my provided capture file, the DNS protocol packets 5-14 are all internal queries. The real query is packet 15, and the response is packet 16.

### Q12: What is the destination port for the DNS query message? What is the source port of the DNS response message?

The destination port of the DNS query message is port 53, and the source port of the DNS response message is port 53.

### Q13: To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?

The IP address the DNS query message is sent to is 10.1.74.10. which is the IP address of my default local DNS server.

### Q14: Examine the DNS query message. What "Type" of DNS query is it? Does the query message contain any "answers"?

The query is a type A DNS query, and the query message does not contain any "answers", since a 'query' is a question.

### Q15: Examine the DNS response message to the query message. How many "questions" does this DNS response message contain? How many "answers"?

There is one "question" and one "answer", presumably indicating the original query and the answer the the query.

### For Q16-Q18, the wireshark capture file is [here](dns-wireshark-trace-3.pcapng).

Similar to the previous capture, there are a couple of internal DNS queries made before the expected one. The real DNS query is packet 13, and the response is packet 14.

### Q16: To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?

The IP address the DNS query message is sent to is 10.1.74.10. which is the IP address of my default local DNS server.

### Q17: Examine the DNS query message. How many questions does the query have? Does the query message contain any "answers"?

The query message has one question, and does not contain any "answers".

### Q18: Examine the DNS response message. How many answers does the response have? What information is contained in the answers? How many additional resource records are returned? What additional information is included in these additional resource records?

The response has 3 answers, which contain the 3 authoritative DNS servers for umass.edu. My response had no additional resource records returned, but comparing this to the provided capture file, there would be 3 of them, and they include the IP address of each authoritative server. This makes sense, because when I issued the `nslookup` command, the IP addresses of the servers were not shown.

![Command Prompt - type=NS nslookup](../../images/week03/cmd-screenshot.PNG)